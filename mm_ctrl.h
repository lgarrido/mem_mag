#ifndef MM_INC_CTRLFILE
#define MM_INC_CTRLFILE

#include <stdint.h>

#include "node_info.h"

/* Debug Macros */
//#define MM_CTRL_DEBUG

//#define MM_REQ_MCAP_DEBUG
//#define MM_RCV_MCAP_DEBUG_ZERO
//#define MM_RCV_MCAP_DEBUG_ONE
#define MM_RELINQ_MCAP_DEBUG
#define MM_RELACK_MCAP_DEBUG
#define MM_RECLAIM_MCAP_DEBUG
//#define MM_RECLACK_MCAP_DEBUG
//#define MM_STATS_RCV_DEBUG
//#define MM_CMD_RCV_DEBUG
//#define MM_CMD_RCV_SNDSTATS_DEBUG
//#define MM_PLAIN_CMD_DEBUG


//#define MM_INACT_CONSTRAINT

// Reserve the amount of pages estimated that Xen uses
// According to very basic measurements, this value is really around 55~60MB.
// To be save, we separate 96MB. Then we translate this value into pages.
#define XEN_MEM_MB 	96
#define DOM0_MEM_MB	2048

#define XEN_PAGES 	XEN_MEM_MB*1024/4
#define DOM0_PAGES	DOM0_MEM_MB*1024/4

#define LOCAL_MIN_PAGES 65536 
#define LOCAL_MAX_PAGES 458752

#define PAGE_THRESHOLD 50

#define PG_BATCH_SIZE 128

#define INIT_FRACTION_RESOLUTION 10000

/* Memory management commands. Communicate with the HV */
#define MM_VOID                 0xB0000
#define MM_TEST                 0xB0001
#define MM_RSVPAG               0xB0002
#define MM_RELPAG               0xB0003

#define MM_SETUP_TFAL           0xB0005		// Setup the local free address list.
#define MM_SETUP_LFAL           0xB0006		// Setup the local free address list.
#define MM_SETUP_RFAL           0xB0007		// Setup the local free address list.

#define MM_SETUP_HNINF          0xB0008
#define MM_SEND_STATS           0xB0009

#define MM_TFAL_TO_LFAL         0xB000A		// Move from the total list to the local list.
#define MM_LFAL_TO_TFAL         0xB000D
#define MM_TFAL_TO_RFAL         0xB000E
#define MM_RFAL_TO_TFAL         0xB000F
#define MM_LORRFAL_TO_TFAL      0xB0023
#define MM_TFAL_TO_LORRFAL      0xB0025

/* Commands that can be sent to remote mm, hypervisor
	MM_REQ_MCAP: when sent to the hypervisor of the local node
		     it will retrieve pages from the total list (and will be put in 
		     the HASH_GRP_RMT for system resiliency purposes, this cannnot 
		     be used by the local node). When sent to a remote mm, it 
		     will be to request memory from it.

	MM_RCV_MCAP: when sent to the hypervisor of the local node
		     it will insert memory pages received from a remote node into
		     the free_remote_addrs list. When sent to a remote mm, 
		     it will be to notify the remote node that pages are being sent to it.
		     (remote MM only) Sends a list of pages as a response to a previous
		     MM_REQ_CAP from a remote node.

	MM_RECLAIM_MCAP: when sent to the hypervisor of the local node
		     it will move pages from the HASH_GRP_RMT group back into the total 
		     list. When sent to a remote mm, it will be to tell it to stop using
		     all the pages given to it by the node that sent the command. Some
		     flush operations will be necessary by the receiving node of the command.

	MM_RECLACK_MCAP: (remote MM only) When sent to a remote mm, it is sent as a 
			response to a previous MM_REC_MCAP comand. The MM_RECACK_MCAP 
			will send the list of pages that could be returned by 
			the receiving node of the MM_REC_MCAP command. 

	MM_RELINQ_MCAP: when sent to the hypervisor of the local node
		     it will just erase all the pages in the total list that belong to the
		     node that it wants to stop borrowing memory from. 
		     (remote MM only) When sent to a remote mm, it will be to notify that the
		     remote pages given to the sending node are not being used any more
		     by this node. The pages in the HASH_GRP_RMT of the remote node are moved
		     into the total list, and the node that sends this. The sending node
		     will have to send a list of pages that it wants to stop using. This is
		     necessary because some of the pages might already be in used, and the
		     remote node will not be able to know in any other way.

	MM_RELACK_MCAP: (remote MM only) When sent to a remote mm, it is sent as a 
			response to a previous MM_REL_MCAP comand. The MM_RELACK_MCAP 
			will send the list of pages that could be returned by 
			the receiving node of the MM_REC_MCAP command.

	MM_TEST_SEND: (remote MM only) just send a test command. This is to make sure socket
		      is working, and that the data is received properly using the data
		      structures and the functions written for processing of data
		      reception and tranmission.

	MM_TEST_RESP: (remote MM only) upon receiving a MM_TEST_SEND, the node will respond
		      with a MM_TEST_RESP command, and this response will include some fake
		      information about pages.


	// For single master-multiple slave mode
	MM_FN2M_REQ_MCAP: a node tells the master that it needs memory (FN2M: from node to master)
	MM_FM2N_REQ_MCAP: master chooses a node, and sends a request to get capacity from it 
			  (FM2N: from master to node), so that the receiving node can send it to
			  the requesting node.
	MM_FN2N_REQ_MCAP: the node that received the request from the master now sends the memory
			  pages back to the requesting node

	MM_FN2M_SENDSTATS: send stats from node to master node. Master node does not send stats to itself.
	MM_FM2N_REQSTATS: the master might explicitly request stats from a node if it does not receive them.
*/
#define MM_REQ_MCAP		0xB000B		// request memory capacity
#define MM_RCV_MCAP		0xB000C		
#define MM_RELINQ_MCAP		0xB0010
#define MM_RELACK_MCAP		0xB0011
#define MM_RECLAIM_MCAP		0xB0013
#define MM_RECLACK_MCAP 	0xB0012

#define MM_DISPERS_MCAP		0xB0024

#define MM_SND_STATS		0xB0014
#define MM_REQ_STATS		0xB0015

// For single master-multiple node
#define MM_FN2M_REQ_MCAP	0xB0018
#define MM_FM2N_REQ_MCAP	0xB0019
#define MM_FN2N_REQ_MCAP	0xB0020

#define MM_FN2M_SENDSTATS	0xB0021
#define MM_FM2N_REQSTATS	0xB0022


// For external DRL agent in python
#define MM_EXTERNAL_DRL         0xB0023 

#define MM_TEST_SEND	0xB0016
#define MM_TEST_RESP	0xB0017 
/* End of Memory management commands */

/* List groups */
#define LIST_GRP_TOTAL             0
#define LIST_GRP_LOCAL_LOCALADDR   1
#define LIST_GRP_LOCAL_RMTADDR     2
#define LIST_GRP_TOTAL_RMTADDR     5
#define LIST_GRP_NONE_AWAY     	   6

#define HASH_GRP_RMT		   3
#define RMT_REGION		   4


/* Success and error return values. Grouped by function */
#define S_MM_RELATED 		0
#define SUCC_SET_HNINF 		0
#define SUCC_REQ_MCAP 		0
#define SUCC_RCV_MCAP 		0
#define SUCC_RELINQ_MCAP 	0
#define SUCC_RELACK_MCAP 	0
#define SUCC_RECLAIM_MCAP 	0
#define SUCC_RECLACK_MCAP 	0

#define ERR_FN2M_REQMCAP_NO_MASTER		-1
#define ERR_FN2M_REQMCAP_MASTER_ME		-2

#define ERR_CMDRCV_SNDSTAT_MASTER		-3
#define ERR_CMDRCV_REQSTAT_MASTER		-4

#define ERR_CANT_SEND_TO_SELF			-5

#define SUCC_MM_PLAIN_CMD_CANT_SEND		 1
#define ERR_MM_PLAIN_CMD_CANT_SEND		-6

#define SUCC_MM_PGLST_SEND			 1
#define ERR_MM_PGLST_CMD_CANT_SEND		-7



//#define MM_INTERRUPTS_STATS_ENABLED		0
#define MM_INTERRUPTS_STATS_ENABLED		1

// Latency for remote accesses. Assuming all nodes have the same distance.
#define REMOTE_ACCESS_LATENCY                   50000 - 1
//10000 - 1

// Inactivity monitoring
#define INACTIVITY_COUNT                        MM_SAMPLING_TIME-1
#define PGSRET_NOACTIVITY                       250

// Command to user process

typedef uint32_t mm_command;

//
struct cntg_blk_mm {
    uint64_t base;
    uint64_t length;
};

// Data structure used to communicate address arrays across
// instances of the memory manager

// Add the assertions, make sure is less than a page.
// GCC compile time assert.
struct mm_hyp_cmd
{
    uint32_t cmd;
    uint64_t total_list_min;

    union {
	// To reserve pages for the domain	
	struct {
            uint32_t cmd;
	    uint32_t dom_id;

	    uint64_t mm_target;
	    uint64_t dn_target;
	    uint64_t dn_target_prev;
	    uint64_t mm_dn_slk;
	    uint8_t breached;
	    uint32_t reclaim_priority;
	    uint32_t retrieve_fraction;  

	    uint64_t pages_freed;
	    uint64_t freed_threshold;

            uint64_t balloon_target;
	} pg_mng[MAX_DOMAINS];

	// To add new addresses to a group of either global or remote. 
	// Commands that invoke this: MM_SETUP_TFAL, MM_SETUP_LFAL, MM_SETUP_RFAL
	struct {
	    uint8_t src_group;	// total -> local/remote/given, given->total
	    uint8_t dst_group;
	    uint64_t len;
	    uint32_t region_id;
	    
	    struct cntg_blk_mm addr_blks[PG_BATCH_SIZE];
	} pglst_mng;		// used to add new addresses to the lists
    } m; 
};

struct mmcmd_prime {
    uint32_t cmd;
    uint32_t src_region_id;
    uint32_t dest_region_id;
};

struct mmcmd_pgreq {
    uint64_t pages_to_request;
};

struct mmcmd_pg_give_rel {
    uint64_t pages;
    uint64_t len;	    
    struct cntg_blk_mm addr_blks[PG_BATCH_SIZE];
};

int send_test_command();
int resp_test_command(struct tmop_cnt *opcnt);

int send_free_addr_list( uint32_t cmd,
			 uint8_t src_group, uint8_t dst_group,
			 struct cntg_blk_mm *addr_contg_blks,
			 uint64_t len );

int32_t is_address_local( uint64_t address, struct node_info *ninf, uint32_t lnid);
int32_t address_region( uint64_t address, struct node_info *ninf, uint32_t lnid);

int move_addresses(uint32_t cmd, uint8_t src_group, uint8_t dst_group, uint64_t len);

int mm_hyp_cmd_clear(struct mm_hyp_cmd *rep);

int set_hyp_ninf(struct node_info *ninf, uint32_t this_node_id);
int set_total_free_list(struct node_info *ninf, uint32_t nid );
int set_local_free_list(struct node_info *ninf, uint32_t nid );

int move_total_to_local(uint64_t amount);
int move_local_to_total(uint64_t amount);
int move_total_to_remote(uint64_t amount);
int move_remote_to_total(uint64_t amount);
int move_localOrRemote_to_total(uint64_t amount);

int32_t mm_req_mcap(struct mm_hyp_cmd *rep, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                      uint64_t * entries, uint32_t this_region_id, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf,
                      uint64_t amount);
int32_t mm_rcv_mcap(struct mm_hyp_cmd *rep);

int32_t mm_relinq_mcap(struct mm_hyp_cmd *rep, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                            uint64_t * entries, uint32_t this_region_id, uint64_t amount, 
                            uint32_t lnid, struct node_info *ninf, uint32_t node_no );
int32_t mm_relack_mcap(struct mm_hyp_cmd *rep);

int32_t mm_reclaim_mcap(struct mm_hyp_cmd *rep, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts,
                         uint64_t * entries, uint32_t this_region_id, uint32_t node_no, 
                         uint32_t this_node_id, struct node_info *ninf, uint64_t amount, uint32_t region_id);
int32_t mm_reclack_mcap(struct mm_hyp_cmd *rep); int32_t mm_pagealloc_cmd(struct mm_hyp_cmd *mmcmd, uint32_t cmd, 
				uint32_t lnid, struct node_info *ninf, uint32_t node_no);

/* Receive stats coming from the TKM */
int32_t mm_fn2n_test_cmd( uint32_t src_region_id, uint32_t src_node_id, struct node_info *ninf, uint32_t node_no);
int32_t mm_fn2n_testresp_cmd( uint32_t src_region, uint32_t dst_region, uint32_t trn_id,
				uint32_t lnid, struct node_info *ninf, uint32_t node_no);
int32_t mm_plain_cmd(uint32_t cmd, uint32_t dst_region, uint64_t len, 
			uint32_t lnid, struct node_info *ninf, uint32_t node_no);
int32_t mm_pglst_cmd(struct mm_hyp_cmd *mmcmd, uint32_t cmd, 
			uint32_t lnid, struct node_info *ninf, uint32_t node_no);


int32_t mm_stats_cmd (struct tmop_cnt* opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t cmd, uint32_t src_region, 
			uint32_t dst_region, uint32_t lnid, struct node_info *ninf, uint32_t node_no);

int32_t mm_cmd_rcv( struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                      uint64_t * entries, uint32_t this_region_id, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf);

int32_t stats_rcv(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                     uint64_t * entries, uint32_t this_region_id, uint8_t inter, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf );


/* Manage commands coming from other memory managers */
int mm_rmtcmd(struct tmop_cnt *opcnt);

#endif
