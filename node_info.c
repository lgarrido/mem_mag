#include <stdio.h>		// for FILE
#include <stdlib.h>		// malloc()
#include <string.h>		// str* related function
#include <arpa/inet.h> 
#include <sys/types.h>
#include <time.h>
#include <float.h>
#include <limits.h>

#include "node_info.h"

//#define CFG_DEBUG

static uint8_t nstat_file_opened=0;
static struct domstat_file_status nstat_file_opened_dom[MAX_DOMAINS];
struct llnode prevgrph[MAX_RMTNODES];

/**/					// ++ platero, start
int parnoconf(char * config_file_name, struct node_info **ninf2, uint32_t * number_of_nodes) {
    int rc=0, newlen, i=0, j, k, params=0;
    uint32_t node_no;
    size_t buf_size = (size_t)BUF_SIZE;
    char *buffer;
    char *temp_buf0, *temp_buf1, *temp_buf2, *temp_buf3;
    struct node_info *ninf;
    FILE *cfg_fd;

    cfg_fd = fopen(config_file_name, "r");
    if (cfg_fd == NULL)
    {
        printf("Configuration file could not be opened\n");
        rc = E_CFG_OPEN;
	goto parnoconf_exit;
    }

    // Allocate buffer to read each line of the corresponding configuration file
    buffer = (char*)malloc(buf_size * sizeof(char));
    temp_buf2 = (char *)malloc(sizeof(char)*buf_size );
    temp_buf3 = (char *)malloc(sizeof(char)*buf_size );

    while ( (newlen = getline(&buffer, &buf_size, cfg_fd)) > 0 ) {
	if (strstr(buffer, "nodes") > 0 ) {

	    temp_buf0 = strchr(buffer, '=');
	    temp_buf0++;
	    temp_buf1 = strchr(buffer, ',');
 
	    memset(temp_buf2, 0, buf_size); 
	    strncpy(temp_buf2, temp_buf0, temp_buf1 - temp_buf0); 
	    node_no = (unsigned int)atoi(temp_buf2);
	    #ifdef CFG_DEBUG
	    printf("node_no=%i\n", node_no);
	    #endif
	
	    //printf("before malloc node_no=%i, ninf=%lu\n", node_no, (uint64_t)ninf);
	    *ninf2 = (struct node_info *)malloc(sizeof(struct node_info)*node_no);
	    ninf = *ninf2;
	    //printf("after malloc node_no=%i, ninf=%lu\n", node_no, (uint64_t)ninf);
	    *number_of_nodes = node_no;

	    temp_buf0 = strchr(temp_buf1, '=');
	    temp_buf0++;
	    temp_buf1 = strchr(temp_buf1, '\0');

	    memset(temp_buf2, 0, buf_size);
	    strncpy(temp_buf2, temp_buf0, temp_buf1 - temp_buf0);
	    params = (unsigned int)atoi(temp_buf2);

	    #ifdef CFG_DEBUG
	    printf("params=%i\n", params);
	    printf("node numbers=%i,das params=%i\n", node_no, params);
	    #endif

	    for (j=0; j < node_no; j++) {
		// Initialize the buffer to zero.
		memset(buffer, '0', buf_size);
		// Read a new line from the configuration file
		newlen = getline(&buffer, &buf_size, cfg_fd);
		temp_buf0 = buffer;
		for (k=0; k < params; k++) {
		    temp_buf1 = strstr(temp_buf0, "=");
		
		    memset(temp_buf2, 0, buf_size);
		    strncpy(temp_buf2, temp_buf0, temp_buf1 - temp_buf0);
		    #ifdef CFG_DEBUG
		    printf("temp_buf2=%s\n", temp_buf2);
		    #endif

		    temp_buf0 = temp_buf1;
		    temp_buf0++;
		    temp_buf1 = strchr(temp_buf0, ',');
		    if (temp_buf1 == NULL)
			temp_buf1 = strchr(temp_buf0, '\0');

		    memset(temp_buf3, 0, buf_size);
		    strncpy(temp_buf3, temp_buf0, temp_buf1 - temp_buf0);
		    #ifdef CFG_DEBUG
		    printf("temp_buf3=%s\n", temp_buf3);
		    #endif

		    if (strstr(temp_buf2, "address") != NULL) {
			// Get IP address of the node that read
			//#ifdef CFG_DEBUG
			printf("address in temp_buf3=%s\n", temp_buf3);
			//#endif

			// Add stuff here to make translation from hostname to IP.
			//inet_aton(temp_buf2, (struct in_addr*)&ninf[j].naddr.sin_addr.s_addr);
			inet_aton(temp_buf3, (struct in_addr*)&ninf[j].naddr.sin_addr.s_addr);

		    } else if (strstr(temp_buf2, "port") != NULL) {	
			#ifdef CFG_DEBUG	
			printf("port in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].portno = htons(atoi(temp_buf3));
	
		    } else if (strstr(temp_buf2, "self") != NULL) {
			#ifdef CFG_DEBUG
			printf("self in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].self = atoi(temp_buf3);
		    } else if (strstr(temp_buf2, "master") != NULL) {
			#ifdef CFG_DEBUG
			printf("addr_strt in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].master = strtol(temp_buf0, NULL, 16);
		    } else if (strstr(temp_buf2, "addr_strt") != NULL) {
			#ifdef CFG_DEBUG
			printf("addr_strt in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].addr_strt = strtol(temp_buf0, NULL, 16);
		    } else if (strstr(temp_buf2, "num_pages") != NULL) {
			#ifdef CFG_DEBUG
			printf("num_pages in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].num_pages = atoi(temp_buf3);
		    } else if (strstr(temp_buf2, "local_init_pages") != NULL) {
			#ifdef CFG_DEBUG
			printf("local_init_pages in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].local_init_pages = atoi(temp_buf3);
		    } else if (strstr(temp_buf2, "region") != NULL) {
			#ifdef CFG_DEBUG
			printf("region in temp_buf3=%s\n", temp_buf3);
			#endif
			ninf[j].region = atoi(temp_buf3);
			rc = CFG_READ_SUCC;
		    }

		    temp_buf0 = temp_buf1;
		    temp_buf0++;
		    temp_buf0++;
		}

		ninf[j].node_id = j;
		ninf[j].rem_socket = -1;
		ninf[j].connected = -1;
		ninf[j].sd = -1;
		ninf[j].bffst = 0;
		ninf[j].cmd_type = 0;
		ninf[j].rdbuf = NULL;

	    }
	}
	i++;
    }
    free(buffer);
    free(temp_buf2);
    free(temp_buf3);
    fclose(cfg_fd);

    printf("/*****    Information of all nodes    *****/\n");
    for (j = 0; j < node_no; j++) {
	temp_buf3 = (char *)malloc( sizeof(char)*CHART_FILENAME_SIZE);
	inet_ntop( AF_INET, &ninf[j].naddr.sin_addr, temp_buf3, CHART_FILENAME_SIZE);
	printf("\t%i. ninf[%i].node_id=%i, ninf[%i].portno=%i, .self=%i, .master=%i, .addr_strt=%lx, .region=%i, .address=%s, .local_init.pages=%lu\n", 
			j, j, ninf[j].node_id, j, ntohs(ninf[j].portno), ninf[j].self,
				ninf[j].master, ninf[j].addr_strt,  ninf[j].region, temp_buf3, ninf[j].local_init_pages );

	free(temp_buf3);
    }
    printf("\n\n");

parnoconf_exit:
    switch (rc) {
 	case CFG_READ_SUCC:
	    printf("CFG_READ_SUCC: Configuration file parsed successfully\n");
	break;
	case E_CFG_OPEN:
	    printf("E_CFG_OPEN: Problem opening configuration file\n");
	break;
    }

    return rc;
}

int display_chart (struct pernodechart * nodecharts ) {
    int i = 0, j = 0, k=0, rc=0;
    struct value_entry *temp_time;
    struct nodechart_entry *temp_ns;
    struct domchart_entry *temp_ds;

    printf("\n\n/***** Elements in the list  *****/\n");
    printf("\tnodecharts->region_id=%i, nodecharts->sys_mem=%lu, nodecharts->domcnt=%i, nodecharts->entry_count=%lu\n", 
		nodecharts->region_id, nodecharts->sys_mem, nodecharts->domcnt, nodecharts->entry_count);
    
    temp_time = nodecharts->time.list_root;
    temp_ns = nodecharts->nodestats.list_root;
    for (i = 0; i < nodecharts->nodestats.size; i++) {

	printf("\t%i. temp_time->v=%lu, temp_ns->total_puts=%lu\n", 
			i, temp_time->v, temp_ns->total_puts);

	if (temp_ns->next) {
	    temp_ns = temp_ns->next;
	} else if (!temp_ns->next && i < (nodecharts->nodestats.size -1 ) ) {
	    printf("func %s, error: list->size=%lu, temp_ns->next=%lu, i=%i\n", 
			__func__, nodecharts->nodestats.size, (long unsigned int)temp_ns->next, i);
	    rc = -1;
	    break;
	}

	if (temp_time->next) {
	    temp_time = temp_time->next;
	} else if (!temp_time->next && i < (nodecharts->nodestats.size -1 ) ) {
	    printf("func %s, error: nodecharts->nodestats->size=%lu, temp_time->next=%lu, i=%i\n", 
			__func__, nodecharts->nodestats.size, (long unsigned int)temp_time->next, i);
	    rc = -1;
	    break;
	}

	if (i == 99) {
	    printf("temp_ns=%lu\n", (long unsigned int)temp_ns);
	}
    }

    for (i=0; i < nodecharts->domcnt; i++) {
	printf("\t%i. nodecharts->domcharts[%i], .region_id=%i, .dom_id=%i, .nod_mem=%lu\n", 
			i,i,  nodecharts->domcharts[i].region_id, 
			nodecharts->domcharts[i].dom_id, 
			nodecharts->domcharts[i].nod_mem);

	temp_ds = nodecharts->domcharts[i].domstats.list_root;
	for (j=0; j < nodecharts->domcharts[i].domstats.size; j++) {
	    printf("\t\t%i. temp_ds->puts=%lu\n", j, temp_ds->puts);

	    if (temp_ds->next) {
		temp_ds = temp_ds->next;
	    } else if (!temp_ds->next && j < (nodecharts->domcharts[i].domstats.size -1 ) ) {
		printf("func %s, error: nodecharts->domcharts[i].domstats.size=%lu, temp_ds->next=%lu, j=%i\n", 
			__func__, nodecharts->domcharts[i].domstats.size, (long unsigned int)temp_ds->next, j);
		rc = -1;
		break;
	    }
	}

    }

    if ( !(temp_time == nodecharts->time.list_last) ) {
	printf("func %s, error: not last elem, nodecharts->time.size=%lu, .list_last=%lu, temp_time=%lu, ->next=%lu, ->prev=%lu\n", 
			__func__, nodecharts->time.size, 
			(long unsigned int)nodecharts->time.list_last,
			(long unsigned int)temp_time, 
			(long unsigned int)temp_time->next,
			(long unsigned int)temp_time->prev);
    }

    rc = 1;

    return rc;
}

int32_t write_stats_to_file(struct pernodechart *nodecharts) {
    uint32_t i=0, j=0;
    int32_t nsfod = -1, zeroInd=-1;
    struct value_entry *tce;
    struct nodechart_entry *nce;
    struct domchart_entry *dce;
    char *cfn, *cfn_suffix;
    FILE *cfd = NULL;
    //int fd = 0;
    time_t t;
    struct tm tmin;

    //printf("func %s, note: in write_stats_to_file\n", __func__);

    cfn = (char*)malloc(CHART_FILENAME_SIZE*sizeof(char));
    cfn_suffix = (char *)malloc(CHART_FILENAME_SIZE*sizeof(char));
    for (i=0; i < CHART_FILENAME_SIZE; i++) {
	cfn[i] = 0;
	cfn_suffix[i] = 0;
    }
    strcpy(cfn, MM_NSTAT_FILE);

    //printf("func %s, note: cfn = strcat\n", __func__);
    sprintf(cfn_suffix, "%i", nodecharts->region_id);
    cfn = strcat(cfn, cfn_suffix);

    strcpy(cfn_suffix, MM_STATFILE_EXTENSION);
    cfn = strcat(cfn, cfn_suffix);
    if (nstat_file_opened) {
        j=0;
	//printf("func %s, note: region, cfd = fopen(cfn, a)\n", __func__);
        while (cfd == NULL /*&& j < 10*/) {
	    cfd = fopen(cfn, "a");
            usleep(1000);
            j = j + 1;
        }
        j=0;
        
    } else {
	nstat_file_opened = 1;
	//printf("func %s, note: region, cfd = fopen(cfn, w)\n", __func__);
        j = 0;
        while (cfd == NULL /*&& j < 10*/) {
	    cfd = fopen(cfn, "w");
            j = j + 1; 
            usleep(1000);
        }
        j=0;

	t = time(NULL);
	tmin = *localtime(&t);
	fprintf(cfd, "date: %d/%d/%d-%d:%d:%d\n", 
			(tmin.tm_year +1900), 
			tmin.tm_mon + 1, 
			tmin.tm_mday, 
			tmin.tm_hour, 
			tmin.tm_min, 
			tmin.tm_sec);

	fprintf(cfd, "%s, %s, %s, %s,  %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s\n", 
		"region_id", "sys_mem", "domcnt", "timestamp", 
		"total_accesses","total_list", "local_list", "remote_list", "given_list", "used_list", "total_puts", 
		"total_putssucc", "total_putrateavg","total_putrateinst", "total_gets", "total_getssucc", "total_getrateavg", "total_getrateinst", 
		"total_flush", "total_flushsucc","total_flushrateavg","total_flushrateinst","local_puts","local_putrateavg","local_putrateinst",
		"local_gets", "local_getrateavg","local_getrateinst","local_flush","local_flushrateavg","local_flushrateinst","remote_puts",
		"remote_putrateavg", "remote_putrateinst","remote_gets","remote_getrateavg","remote_getrateinst","remote_flush","remote_flushrateavg",
		"remote_flushrateinst", "total_tmem", "local_tmem", "remote_tmem");
    }
    
    if (cfd == NULL) {
	#ifdef MM_WRITESTATS_DEBUG
	printf("func %s, error: cfd=%lu, cannot open file\n", __func__, (uint64_t)cfd);
	#endif
	return -2;
    }

    tce = nodecharts->time.list_root;
    nce = nodecharts->nodestats.list_root;
    //fprintf(cfd, "%s\n\n", "Start batch");

    for (i=0;i < WINDOW_SIZE; i++) { 
	//printf("%i,%lu,%i,%lu,%lu\n", nodecharts->region_id, nodecharts->sys_mem, nodecharts->domcnt, tce->v, nce->total_puts);
	fprintf(cfd, "%i,%lu,%i,%lu, %lu,%lu,%lu,%lu,%lu,%lu,%lu, %lu,%.6f,%.6f,%lu,%lu,%.6f,%.6f, %lu,%lu,%.6f,%.6f,%lu,%.6f,%.6f, %lu,%.6f,%.6f,%lu,%.6f,%.6f,%lu, %.6f,%.6f,%lu,%.6f,%.6f,%lu,%.6f, %.6f,%lu,%lu,%lu\n", 
			nodecharts->region_id, nodecharts->sys_mem, nodecharts->domcnt, tce->v,
 
			nce->total_accesses,
			nce->total_list, 
			nce->local_list,
			nce->remote_list,
			nce->given_list,
			nce->used_list,
			nce->total_puts,

			nce->total_putssucc,
			nce->total_putrateavg,
			nce->total_putrateinst,
			nce->total_gets,
			nce->total_getssucc,
			nce->total_getrateavg,
			nce->total_getrateinst,
			
			nce->total_flush, 
			nce->total_flushsucc,
			nce->total_flushrateavg,
			nce->total_flushrateinst,
			nce->local_puts,
			nce->local_putrateavg,
			nce->local_putrateinst,
			
			nce->local_gets,
			nce->local_getrateavg,
			nce->local_getrateinst,
			nce->local_flush,
			nce->local_flushrateavg,
			nce->local_flushrateinst,
			nce->remote_puts,
			
			nce->remote_putrateavg,
			nce->remote_putrateinst,
			nce->remote_gets,
			nce->remote_getrateavg,
			nce->remote_getrateinst,
			nce->remote_flush,
			nce->remote_flushrateavg,
			
			nce->remote_flushrateinst,
			nce->total_tmem,
			nce->local_tmem, 
                        nce->remote_tmem);

	if (tce->next) {
	    tce = tce->next;
	} else if (!tce->next && i < (WINDOW_SIZE -1)) {
	    #ifdef MM_WRITESTATS_DEBUG
	    printf("func %s, error: tce->next=%lu, not valid. Exiting.\n", __func__, (uint64_t)tce->next);
	    #endif
	    return -1;
	}

	if (nce->next) {
	    nce = nce->next;
	} else if (!nce->next && i < (WINDOW_SIZE-1) ) {
	    #ifdef MM_WRITESTATS_DEBUG
	    printf("func %s, error: nce->next=%lu, not valid. Exiting\n", __func__, (uint64_t)nce->next);
	    #endif
	    return -1;
	}
    }
    //fprintf(cfd, "%s\n\n", "First batch");
    //printf("about to close cfd\n");
    fflush(cfd);       // flushing before closing. Make sure all buffered data is written.
    fclose(cfd);
    
    for (i=0; i < nodecharts->domcnt /* MAX_DOMAINS*/; i++) {
	for (j=0; j < CHART_FILENAME_SIZE; j++) {
	    cfn[j] = 0;
	}
	strcpy(cfn, MM_NSTAT_FILE);
	sprintf(cfn_suffix, "%idom%i%s", nodecharts->domcharts[i].region_id, nodecharts->domcharts[i].dom_id, MM_STATFILE_EXTENSION);

	for (j=0; j < nodecharts->domcnt; j++) {
	    if ( (nstat_file_opened_dom[j].region_id == nodecharts->domcharts[i].region_id) && 
			(nstat_file_opened_dom[j].dom_id == nodecharts->domcharts[i].dom_id) ) {
		nsfod = j;
		break;
	    }

	    if ( (nstat_file_opened_dom[j].dom_id == 0) && 
		 (nstat_file_opened_dom[j].region_id == 0) && zeroInd == -1 ) {
		zeroInd = j;
	    }
	}

	if (nsfod == -1 && zeroInd != -1) {
	    nstat_file_opened_dom[zeroInd].region_id = nodecharts->domcharts[i].region_id;
	    nstat_file_opened_dom[zeroInd].dom_id = nodecharts->domcharts[i].dom_id;
	    nstat_file_opened_dom[zeroInd].stat_file_opened = 0;
	    nsfod = zeroInd;
	} 

	cfn = strcat(cfn, cfn_suffix);
	if (nstat_file_opened_dom[nsfod].stat_file_opened) {
	    //printf("func %s, domain note: cfd = fopen(cfn, a)\n", __func__);
            j=0;
            //while (cfd == NULL /*&& j < 10*/) {
	        cfd = fopen(cfn, "a");
                usleep(1000);
                //j = j + 1;
            //}
            j=0;
	} else {
	    nstat_file_opened_dom[nsfod].stat_file_opened = 1;
	    #ifdef MM_WRITESTATS_DEBUG
	    printf("func %s, domain note: cfd = fopen(cfn, w), dom_id=%i\n", 
			__func__, nstat_file_opened_dom[zeroInd].dom_id);
	    #endif
            j=0;
            //while (cfd == NULL /*&& j < 10*/) {
	        cfd = fopen(cfn, "w");
                usleep(1000);
                //j = j + 1;
            //}
            j=0;

	    t = time(NULL);
	    tmin = *localtime(&t);
	    fprintf(cfd, "date: %d/%d/%d-%d:%d:%d\n", 
			(tmin.tm_year +1900), 
			tmin.tm_mon + 1, 
			tmin.tm_mday, 
			tmin.tm_hour, 
			tmin.tm_min, 
			tmin.tm_sec);
	    fprintf(cfd, "%s, %s, %s, %s, %s,  %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s\n", 
		"region_id", "dom_id", "nod_mem", "timestamp", "nod_tmem",
		"total_accesses","puts","putssucc", "putrateavg", "putrateinst", "putfailrateinst", "gets", 
		"getssucc", "getrateavg","getrateinst", "getfailrateinst", "flush", "flushsucc", "flushrateavg", 
		"flushrateinst", "mm_target", "mm_slack","dn_target","breached", "balloon_target", 
                /*new additions*/ "local_accesses", "local_puts", "local_putssucc", "local_putrateavg", 
                "local_putratinst", "local_putfailrateinst",
                "local_gets", "local_getssucc", "local_getrateavg", "local_getrateinst", "local_getfailrateinst",
                "local_flush", "local_flushsucc", "local_flushrateavg", "local_flushrateinst", "local_flushfailrateinst",
                "remote_accesses", "rem_puts", "rem_putssucc", "rem_putrateavg", "rem_putratinst", "rem_putfailrateinst",
                "rem_gets", "rem_getssucc", "rem_getrateavg", "rem_getrateinst", "rem_getfailrateinst",
                "rem_flush", "rem_flushsucc", "rem_flushrateavg", "rem_flushrateinst", "rem_flushfailrateinst"  );

	    #ifdef MM_WRITESTATS_DEBUG
	    printf("func %s, note: header written to cfd\n", __func__);
	    #endif
	}

	if (cfd == NULL) {
	    #ifdef MM_WRITESTATS_DEBUG
	    printf("func %s, error: cfd=%lu, cannot open file\n", 
			__func__, (uint64_t)cfd);
	    #endif
	    return -2;
 	}

	tce = nodecharts->time.list_root;
	dce = nodecharts->domcharts[i].domstats.list_root;
	//fprintf(differentiating line, column info);
	for (j=0; j < WINDOW_SIZE; j++) {
	    fprintf(cfd, "%i,%i,%lu,%lu,%lu, %lu,%lu,%lu,%.6f,%.6f,%.6f,%lu, %lu,%.6f,%.6f,%.6f,%lu,%lu,%.6f, %.6f,%lu,%lu,%lu,%i,%lu, %lu,%lu,%lu,%.6f,%.6f,%.6f, %lu,%lu,%.6f,%.6f,%.6f, %lu,%lu,%.6f,%.6f,%.6f, %lu,%lu,%lu,%.6f,%.6f,%.6f, %lu,%lu,%.6f,%.6f,%.6f, %lu,%lu,%.6f,%.6f,%.6f\n", 
			 nodecharts->domcharts[i].region_id, nodecharts->domcharts[i].dom_id, /*nodecharts->domcharts[i].nod_mem*/ dce->nod_mem, tce->v,
			 dce->nod_tmem,

			 dce->total_accesses, 
			 dce->puts,
			 dce->putssucc,
			 dce->putrateavg,
			 dce->putrateinst,
			 dce->putfailrateinst,
			 dce->gets, 

			 dce->getssucc,
			 dce->getrateavg, 
			 dce->getrateinst,
			 dce->getfailrateinst,
			 dce->flush,
			 dce->flushsucc,
			 dce->flushrateavg,

			 dce->flushrateinst,
			 dce->mm_target,
			 dce->mm_slack,
			 dce->dn_target,
			 dce->breached,
                         dce->balloon_target,

                         dce->total_local_access,
                         dce->local_puts,
                         dce->local_putssucc,
                         dce->local_putrateavg,
                         dce->local_putrateinst,
                         dce->local_putfailrateinst,

                         dce->local_gets,
                         dce->local_getssucc,
                         dce->local_getrateavg,
                         dce->local_getrateinst,
                         dce->local_getfailrateinst,

                         dce->local_flush,
                         dce->local_flushsucc,
                         dce->local_flushrateavg,
                         dce->local_flushrateinst,
                         dce->local_flushfailrateinst,

                         dce->total_rem_access,
                         dce->rem_puts,
                         dce->rem_putssucc,
                         dce->rem_putrateavg,
                         dce->rem_putrateinst,
                         dce->rem_putfailrateinst,

                         dce->rem_gets,
                         dce->rem_getssucc,
                         dce->rem_getrateavg,
                         dce->rem_getrateinst,
                         dce->rem_getfailrateinst,

                         dce->rem_flush,
                         dce->rem_flushsucc,
                         dce->rem_flushrateavg,
                         dce->rem_flushrateinst,
                         dce->rem_flushfailrateinst );

	    if (tce->next) {
		tce = tce->next;
	    } else if (!tce->next && j < (WINDOW_SIZE -1) ) {
		#ifdef MM_WRITESTATS_DEBUG
		printf("func %s, error: tce->next=%lu, not valid. Exiting.\n", __func__, (uint64_t)tce->next);
		#endif
		return -1;
	    }

	    if (dce->next) {
		dce = dce->next;
	    } else if (!dce->next && j < (WINDOW_SIZE -1)) {
		#ifdef MM_WRITESTATS_DEBUG
		printf("func %s, error: dce->next=%lu, not valid. Exiting\n", __func__, (uint64_t)dce->next);
		#endif
		return -1;
	    }

	}
        fflush(cfd);       // flushing before closing. Make sure all buffered data is written.
	fclose(cfd);
    }

    free(cfn);
    free(cfn_suffix);
    return 1;
}

int32_t setzero_domstats(struct domchart_entry *entry, uint64_t start_ts) {


    entry->total_accesses = 0;
    entry->nod_tmem = 0;
    entry->nod_mem = 0;

    entry->puts = 0;
    entry->putssucc = 0;
    entry->putrateavg = 0;

    entry->putrateinst = 0; //entry->prev->putrateinst;
    entry->putfailrateinst = 0; //entry->prev->putfailrateinst;

    entry->getrateinst = 0; //entry->prev->getrateinst;
    entry->getfailrateinst = 0; //entry->prev->getfailrateinst;

    entry->flushrateinst = 0;

    entry->gets = 0; //opcnt->dominfo[i].nod_gets_tot;
    entry->getssucc = 0; //opcnt->dominfo[i].nod_gets_succ;
    entry->getrateavg = 0; //((float)opcnt->dominfo[i].nod_gets_tot)/ts_avgdiff;

    entry->flush = 0; //opcnt->dominfo[i].nod_flush_tot;
    entry->flushsucc = 0; //opcnt->dominfo[i].nod_flush_succ;
    entry->flushrateavg = 0; //((float)opcnt->dominfo[i].nod_flush_tot)/ts_avgdiff;

    entry->mm_target = 0; //opcnt->dominfo[i].mm_target;
    entry->mm_slack = 0; // opcnt->dominfo[i].mm_slack;
    entry->dn_target = 0; //opcnt->dominfo[i].dn_target;
    entry->breached =  0; //opcnt->dominfo[i].breached;
    entry->balloon_target = 0; //opcnt->dominfo[i].balloon_target;

    entry->total_local_access = 0; //opcnt->dominfo[i].total_local_access;
    entry->local_puts = 0; //opcnt->dominfo[i].nod_puts_local_tot;
    entry->local_putssucc = 0; //opcnt->dominfo[i].nod_puts_local_succ;
    entry->local_putrateavg = 0; //((float)opcnt->dominfo[i].nod_puts_local_tot)/ts_avgdiff;

 
    entry->local_putrateinst = 0; //((float)opcnt->dominfo[i].nod_puts_local_tot - (float)prev_local_puts)/ts_instdiff;
    entry->local_putfailrateinst = 0; //(((float)opcnt->dominfo[i].nod_puts_local_tot - (float)opcnt->dominfo[i].nod_puts_local_succ) -
                                        //(prev_local_puts - prev_local_puts_succ) )/ts_instdiff;

    entry->local_getrateinst = 0; //((float)opcnt->dominfo[i].nod_gets_local_tot - (float)prev_local_gets)/ts_instdiff;
    entry->local_getfailrateinst = 0; //(((float)opcnt->dominfo[i].nod_gets_local_tot - (float)opcnt->dominfo[i].nod_gets_local_succ) -
                                        //(prev_local_gets - prev_local_gets_succ) )/ts_instdiff;

    entry->local_flushrateinst = 0; //((float)opcnt->dominfo[i].nod_flush_local_tot - (float)prev_local_flush)/ts_instdiff;

        // Remote fields
    entry->rem_putrateinst = 0; //((float)opcnt->dominfo[i].nod_puts_rem_tot - (float)prev_rem_puts)/ts_instdiff;
    entry->rem_putfailrateinst = 0; //(((float)opcnt->dominfo[i].nod_puts_rem_tot - (float)opcnt->dominfo[i].nod_puts_rem_succ) -
                                        //(prev_rem_puts - prev_rem_puts_succ) )/ts_instdiff;

    entry->rem_getrateinst = 0; //((float)opcnt->dominfo[i].nod_gets_rem_tot - (float)prev_rem_gets)/ts_instdiff;
    entry->rem_getfailrateinst = 0; //(((float)opcnt->dominfo[i].nod_gets_rem_tot - (float)opcnt->dominfo[i].nod_gets_rem_succ) -
                                        //(prev_rem_gets - prev_rem_gets_succ) )/ts_instdiff;

    entry->rem_flushrateinst = 0; //((float)opcnt->dominfo[i].nod_flush_rem_tot - (float)prev_rem_flush)/ts_instdiff;


    entry->local_gets = 0; //opcnt->dominfo[i].nod_gets_local_tot;
    entry->local_getssucc = 0; //opcnt->dominfo[i].nod_gets_local_succ;
    entry->local_getrateavg = 0; //((float)opcnt->dominfo[i].nod_gets_local_tot)/ts_avgdiff;

    entry->local_flush = 0; //opcnt->dominfo[i].nod_flush_local_tot;
    entry->local_flushsucc = 0; //opcnt->dominfo[i].nod_flush_local_succ;
    entry->local_flushrateavg = 0; //((float)opcnt->dominfo[i].nod_flush_local_tot)/ts_avgdiff;


    entry->total_rem_access = 0; //opcnt->dominfo[i].total_rem_access;
    entry->rem_puts = 0; //opcnt->dominfo[i].nod_puts_rem_tot;
    entry->rem_putssucc = 0; //opcnt->dominfo[i].nod_puts_rem_succ;
    entry->rem_putrateavg = 0; //((float)opcnt->dominfo[i].nod_puts_rem_tot)/ts_avgdiff;

    entry->rem_gets = 0; //opcnt->dominfo[i].nod_gets_rem_tot;
    entry->rem_getssucc = 0; //opcnt->dominfo[i].nod_gets_rem_succ;
    entry->rem_getrateavg = 0; //((float)opcnt->dominfo[i].nod_gets_rem_tot)/ts_avgdiff;

    entry->rem_flush = 0; //opcnt->dominfo[i].nod_flush_rem_tot;
    entry->rem_flushsucc = 0; //opcnt->dominfo[i].nod_flush_rem_succ;
    entry->rem_flushrateavg = 0; //((float)opcnt->dominfo[i].nod_flush_rem_tot)/ts_avgdiff;


    return 1;
}


int32_t copy_domstats(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t i, struct domchart_entry *entry, 
			uint64_t curr_ts, uint64_t prev_ts, uint64_t start_ts, 
			uint64_t prev_puts, uint64_t prev_gets, uint64_t prev_flush,
			uint64_t prev_puts_succ, uint64_t prev_gets_succ, 
                        /* New additions */ uint64_t prev_local_puts, uint64_t prev_local_gets, uint64_t prev_local_flush,
                        uint64_t prev_local_puts_succ, uint64_t prev_local_gets_succ,
                        uint64_t prev_rem_puts, uint64_t prev_rem_gets, uint64_t prev_rem_flush,
                        uint64_t prev_rem_puts_succ, uint64_t prev_rem_gets_succ ) {
    float ts_avgdiff, ts_instdiff;  

    if (curr_ts > start_ts) {
	ts_avgdiff = ((float)curr_ts - (float)start_ts)/MM_XEN_TIMESCALE;
    } else {
	ts_avgdiff = (float)curr_ts/MM_XEN_TIMESCALE;
	#ifdef COPY_DOMSTATS_DEBUG
	printf("func %s, error: curr_ts=%lu, start_ts=%lu, start_ts is larger, returning -1\n", 
			__func__, curr_ts, start_ts);
	#endif
	//return -1;
    }  

    if (curr_ts > prev_ts) {
	ts_instdiff = ((float)curr_ts - (float)prev_ts)/MM_XEN_TIMESCALE;
    } else  {
	ts_instdiff = curr_ts/MM_XEN_TIMESCALE;
	#ifdef COPY_DOMSTATS_DEBUG
	printf("func %s, error: curr_ts=%lu, prev_ts=%lu, prev_ts is larger, returning -1\n", 
			__func__, curr_ts, prev_ts);
	#endif
	return -1;
    }

    
    entry->total_accesses = opcnt->dominfo[i].total_access;
    entry->nod_tmem = opcnt->dominfo[i].nod_tmem;
    entry->nod_mem = opcnt->dominfo[i].nod_mem;

    entry->puts = opcnt->dominfo[i].nod_puts_tot;
    entry->putssucc = opcnt->dominfo[i].nod_puts_succ;
    entry->putrateavg = ((float)opcnt->dominfo[i].nod_puts_tot)/ts_avgdiff;

    if (ts_instdiff == 0) {
	#ifdef COPY_NODESTATS_DEBUG
	printf("func %s, error: ts_instdiff=%2.3f, it is zero\n", 
			__func__, ts_instdiff);
	#endif

	entry->putrateinst = entry->prev->putrateinst;
	entry->putfailrateinst = entry->prev->putfailrateinst;

	entry->getrateinst = entry->prev->getrateinst;
	entry->getfailrateinst = entry->prev->getfailrateinst;

	entry->flushrateinst = entry->prev->flushrateinst;
    } else {
	entry->putrateinst = ((float)opcnt->dominfo[i].nod_puts_tot - (float)prev_puts)/ts_instdiff;
   	entry->putfailrateinst = (((float)opcnt->dominfo[i].nod_puts_tot - (float)opcnt->dominfo[i].nod_puts_succ) - 
					(prev_puts - prev_puts_succ) )/ts_instdiff;

	entry->getrateinst = ((float)opcnt->dominfo[i].nod_gets_tot - (float)prev_gets)/ts_instdiff;
	entry->getfailrateinst = (((float)opcnt->dominfo[i].nod_gets_tot - (float)opcnt->dominfo[i].nod_gets_succ) - 
					(prev_gets - prev_gets_succ) )/ts_instdiff;

	entry->flushrateinst = ((float)opcnt->dominfo[i].nod_flush_tot - (float)prev_flush)/ts_instdiff;
    }

    entry->gets = opcnt->dominfo[i].nod_gets_tot;
    entry->getssucc = opcnt->dominfo[i].nod_gets_succ;
    entry->getrateavg = ((float)opcnt->dominfo[i].nod_gets_tot)/ts_avgdiff;

    entry->flush = opcnt->dominfo[i].nod_flush_tot;
    entry->flushsucc = opcnt->dominfo[i].nod_flush_succ;
    entry->flushrateavg = ((float)opcnt->dominfo[i].nod_flush_tot)/ts_avgdiff;

    entry->mm_target = opcnt->dominfo[i].mm_target;
    entry->mm_slack = opcnt->dominfo[i].mm_slack;
    entry->dn_target = opcnt->dominfo[i].dn_target;
    entry->breached =  opcnt->dominfo[i].breached;
    entry->balloon_target = opcnt->dominfo[i].balloon_target;

    entry->total_local_access = opcnt->dominfo[i].total_local_access;
    entry->local_puts = opcnt->dominfo[i].nod_puts_local_tot;    
    entry->local_putssucc = opcnt->dominfo[i].nod_puts_local_succ;    
    entry->local_putrateavg = ((float)opcnt->dominfo[i].nod_puts_local_tot)/ts_avgdiff; 

    if (ts_instdiff == 0) {
	#ifdef COPY_NODESTATS_DEBUG
	printf("func %s, error: ts_instdiff=%2.3f, it is zero\n", 
			__func__, ts_instdiff);
	#endif

	entry->local_putrateinst = entry->prev->local_putrateinst;
	entry->local_putfailrateinst = entry->prev->local_putfailrateinst;

	entry->local_getrateinst = entry->prev->local_getrateinst;
	entry->local_getfailrateinst = entry->prev->local_getfailrateinst;

	entry->local_flushrateinst = entry->prev->local_flushrateinst;

        // Remote fields
        entry->rem_putrateinst = entry->prev->rem_putrateinst;
        entry->rem_putfailrateinst = entry->prev->rem_putfailrateinst;

        entry->rem_getrateinst = entry->prev->rem_getrateinst;
        entry->rem_getfailrateinst = entry->prev->rem_getfailrateinst;
  
        entry->rem_flushrateinst = entry->prev->rem_flushrateinst;
    } else {
	entry->local_putrateinst = ((float)opcnt->dominfo[i].nod_puts_local_tot - (float)prev_local_puts)/ts_instdiff;
   	entry->local_putfailrateinst = (((float)opcnt->dominfo[i].nod_puts_local_tot - (float)opcnt->dominfo[i].nod_puts_local_succ) - 
					(prev_local_puts - prev_local_puts_succ) )/ts_instdiff;

	entry->local_getrateinst = ((float)opcnt->dominfo[i].nod_gets_local_tot - (float)prev_local_gets)/ts_instdiff;
	entry->local_getfailrateinst = (((float)opcnt->dominfo[i].nod_gets_local_tot - (float)opcnt->dominfo[i].nod_gets_local_succ) - 
					(prev_local_gets - prev_local_gets_succ) )/ts_instdiff;

	entry->local_flushrateinst = ((float)opcnt->dominfo[i].nod_flush_local_tot - (float)prev_local_flush)/ts_instdiff;

        // Remote fields
        entry->rem_putrateinst = ((float)opcnt->dominfo[i].nod_puts_rem_tot - (float)prev_rem_puts)/ts_instdiff;
        entry->rem_putfailrateinst = (((float)opcnt->dominfo[i].nod_puts_rem_tot - (float)opcnt->dominfo[i].nod_puts_rem_succ) -
                                        (prev_rem_puts - prev_rem_puts_succ) )/ts_instdiff;

        entry->rem_getrateinst = ((float)opcnt->dominfo[i].nod_gets_rem_tot - (float)prev_rem_gets)/ts_instdiff;
        entry->rem_getfailrateinst = (((float)opcnt->dominfo[i].nod_gets_rem_tot - (float)opcnt->dominfo[i].nod_gets_rem_succ) -
                                        (prev_rem_gets - prev_rem_gets_succ) )/ts_instdiff;

        entry->rem_flushrateinst = ((float)opcnt->dominfo[i].nod_flush_rem_tot - (float)prev_rem_flush)/ts_instdiff;
    }

    entry->local_gets = opcnt->dominfo[i].nod_gets_local_tot;
    entry->local_getssucc = opcnt->dominfo[i].nod_gets_local_succ;
    entry->local_getrateavg = ((float)opcnt->dominfo[i].nod_gets_local_tot)/ts_avgdiff;

    entry->local_flush = opcnt->dominfo[i].nod_flush_local_tot;
    entry->local_flushsucc = opcnt->dominfo[i].nod_flush_local_succ;
    entry->local_flushrateavg = ((float)opcnt->dominfo[i].nod_flush_local_tot)/ts_avgdiff;


    entry->total_rem_access = opcnt->dominfo[i].total_rem_access;
    entry->rem_puts = opcnt->dominfo[i].nod_puts_rem_tot;    
    entry->rem_putssucc = opcnt->dominfo[i].nod_puts_rem_succ;    
    entry->rem_putrateavg = ((float)opcnt->dominfo[i].nod_puts_rem_tot)/ts_avgdiff; 

    entry->rem_gets = opcnt->dominfo[i].nod_gets_rem_tot;
    entry->rem_getssucc = opcnt->dominfo[i].nod_gets_rem_succ;
    entry->rem_getrateavg = ((float)opcnt->dominfo[i].nod_gets_rem_tot)/ts_avgdiff;

    entry->rem_flush = opcnt->dominfo[i].nod_flush_rem_tot;
    entry->rem_flushsucc = opcnt->dominfo[i].nod_flush_rem_succ;
    entry->rem_flushrateavg = ((float)opcnt->dominfo[i].nod_flush_rem_tot)/ts_avgdiff;

    #ifdef COPY_DOMSTATS_DEBUG
    printf("func %s, note: ts_avgdiff=%2.3f, ts_instdiff=%2.3f, entry->nod_tmem=%lu, entry->mm_target=%lu\n", 
			__func__, ts_avgdiff, ts_instdiff, entry->nod_tmem, entry->mm_target);
    #endif

    return 1;
}

int32_t copy_nodestats(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, 
                        uint32_t this_node_id,struct node_info *ninf, struct nodechart_entry *entry, 
			uint64_t curr_ts, uint64_t prev_ts, uint64_t start_ts, 
			uint64_t prev_total_puts, uint64_t prev_total_gets, uint64_t prev_total_flush,
			uint64_t prev_local_puts, uint64_t prev_local_gets, uint64_t prev_local_flush,
			uint64_t prev_remote_puts, uint64_t prev_remote_gets, uint64_t prev_remote_flush) {
    float ts_avgdiff, ts_instdiff;

    if (curr_ts > start_ts) {
	ts_avgdiff = ((float)curr_ts - (float)start_ts)/MM_XEN_TIMESCALE;
    } else {
	ts_avgdiff = ((float)curr_ts)/MM_XEN_TIMESCALE;
	#ifdef COPY_NODESTATS_DEBUG
	printf("func %s, error: returning with -1\n", __func__);
	#endif
	//return -1;
    }

    if (curr_ts > prev_ts) {
	ts_instdiff = ((float)curr_ts - (float)prev_ts)/MM_XEN_TIMESCALE;
    } else {
	ts_instdiff = (float)curr_ts/MM_XEN_TIMESCALE;
	#ifdef COPY_NODESTATS_DEBUG
	printf("func %s, error: curr_ts=%lu, prev_ts=%lu, prev_ts is larger, returning -1\n", 
			__func__, curr_ts, prev_ts);
	#endif
	//return -1;
    }

    #ifdef COPY_NODESTATS_DEBUG
    printf("func %s, note: ts_avgdiff=%.3f, ts_instdiff=%.3f, opcnt->total_puts=%lu, prev_total_puts=%lu\n", 
			__func__, ts_avgdiff, ts_instdiff, opcnt->total_puts, prev_total_puts);
    #endif

    entry->total_accesses = opcnt->total_accesses;

    entry->total_list = opcnt->total_list;
    entry->local_list = opcnt->local_list;
    entry->remote_list = opcnt->remote_list;
    entry->given_list = opcnt->given_list;
    entry->used_list = opcnt->used_list;

    entry->total_puts = opcnt->total_puts;
    entry->total_putssucc = opcnt->total_putssucc;
    entry->total_putrateavg = ((float)opcnt->total_puts)/ts_avgdiff;

    if (ts_instdiff == 0) {
	#ifdef COPY_NODESTATS_DEBUG
	printf("func %s, error: ts_instdiff=%2.3f, it is zero\n", 
			__func__, ts_instdiff);
	#endif
	entry->total_putrateinst = entry->prev->total_putrateinst;
	entry->total_flushrateinst = entry->prev->total_flushrateinst;
	entry->total_getrateinst = entry->prev->total_getrateinst;

	entry->local_putrateinst = entry->prev->local_putrateinst;
	entry->local_getrateinst = entry->prev->local_getrateinst;
	entry->local_flushrateinst = entry->prev->local_flushrateinst;

	entry->remote_putrateinst = entry->prev->remote_putrateinst;
	entry->remote_getrateinst = entry->prev->remote_getrateinst;
	entry->remote_flushrateinst = entry->prev->remote_flushrateinst;
    } else {
	entry->total_putrateinst = ((float)opcnt->total_puts - (float)prev_total_puts)/ts_instdiff;
	entry->total_flushrateinst = ((float)opcnt->total_flush - (float)prev_total_flush)/ts_instdiff;
	entry->total_getrateinst = ((float)opcnt->total_gets - (float)prev_total_gets)/ts_instdiff;

	entry->local_putrateinst = ((float)opcnt->local_puts - (float)prev_local_puts)/ts_instdiff;
	entry->local_getrateinst = ((float)opcnt->local_gets - (float)prev_local_gets)/ts_instdiff;
	entry->local_flushrateinst = ((float)opcnt->local_flush - (float)prev_local_flush)/ts_instdiff;

	entry->remote_putrateinst = ((float)opcnt->rem_puts - (float)prev_remote_puts)/ts_instdiff;
	entry->remote_getrateinst = ((float)opcnt->rem_gets - (float)prev_remote_gets)/ts_instdiff;
	entry->remote_flushrateinst = ((float)opcnt->rem_flush - (float)prev_remote_flush)/ts_instdiff;
    }

    entry->total_gets = opcnt->total_gets;
    entry->total_getssucc = opcnt->total_getssucc;
    entry->total_getrateavg = ((float)opcnt->total_gets)/ts_avgdiff;

    entry->total_flush = opcnt->total_flush;
    entry->total_flushsucc = opcnt->total_flushsucc;
    entry->total_flushrateavg = ((float)opcnt->total_flush)/ts_avgdiff;



    entry->local_puts = opcnt->local_puts;
    entry->local_putrateavg = ((float)opcnt->local_puts)/ts_avgdiff;

    entry->local_gets = opcnt->local_gets;
    entry->local_getrateavg = ((float)opcnt->local_gets)/ts_avgdiff;

    entry->local_flush = opcnt->local_flush;
    entry->local_flushrateavg = ((float)opcnt->local_flush)/ts_avgdiff;

    entry->remote_puts = opcnt->rem_puts;
    entry->remote_putrateavg = ((float)opcnt->rem_puts)/ts_avgdiff;

    entry->remote_gets = opcnt->rem_gets;
    entry->remote_getrateavg = ((float)opcnt->rem_gets)/ts_avgdiff;
	
    entry->remote_flush = opcnt->rem_flush;
    entry->remote_flushrateavg = ((float)opcnt->rem_flush)/ts_avgdiff;

    entry->total_tmem = ninf[this_node_id].num_pages + rmopcnt->remote_pages_taken - rmopcnt->remote_pages_given;
    entry->local_tmem = ninf[this_node_id].num_pages;
    entry->remote_tmem = rmopcnt->remote_pages_taken;

    //printf("func %s, note: opcnt->total_tmem=%lu, ninf[this_node_id].num_pages=%lu, rmopcnt->remote_pages_given=%lu, rmopcnt->remote_pages_taken=%lu\n", 
     //        __func__, opcnt->total_tmem, ninf[this_node_id].num_pages, rmopcnt->remote_pages_given, rmopcnt->remote_pages_taken);    

    return 1;
}

int32_t dom_register(struct tmop_cnt *opcnt, struct pernodechart * nodecharts) {
    int32_t rc=0, i=0, j=0, k=0, zeroInd=-1;
    uint8_t reg=0, present=0;
    #ifdef MM_NODINF_DOM_REG_DEBUG
    uint8_t iter=0;
    #endif

    #ifdef MM_NODINF_DOM_REG_DEBUG
    printf("func %s, note: in %s\n", __func__, __func__);
    #endif


    for (i = 0; i < opcnt->domcnt; i++) {
	reg = 0;
	zeroInd = -1;

	#ifdef MM_NODINF_DOM_REG_DEBUG
	printf("func %s, note: opcnt->dominfo[%i].region_id=%i, opcnt->dominfo[%i].dom_id=%i\n", 
			__func__, i, opcnt->dominfo[i].region_id, 
				  i, opcnt->dominfo[i].dom_id);
	#endif

	for (j=0; j < MAX_DOMAINS; j++) {

	    #ifdef MM_NODINF_DOM_REG_DEBUG
	    //if (iter == 0) {
		printf("func %s, note: nodecharts->domcharts[%i].region_id=%i, nodecharts->domcharts[%i].dom_id=%i\n", 
				__func__, i, nodecharts->domcharts[i].region_id, 
					i, nodecharts->domcharts[i].dom_id );
		iter++;
	    //}
	    #endif

	    if ( (nodecharts->domcharts[j].region_id == opcnt->dominfo[i].region_id) && 
		 (nodecharts->domcharts[j].dom_id == opcnt->dominfo[i].dom_id) ) {
		reg = 1;
	    }

 	    if ( (nodecharts->domcharts[j].dom_id == 0) && 
		 (nodecharts->domcharts[j].region_id == 0) && zeroInd == -1 ) {
		zeroInd = j;
	    }
	}

dom_register_reg_dom0:
	if (reg == 0 && zeroInd >= 0) {
	    // use a free slot, and put it there
	    #ifdef MM_NODINF_DOM_REG_DEBUG
	    printf("func %s, note: free slot, reg=%i, zeroInd=%i\n", 
			__func__, reg, zeroInd);
	    #endif
	    reg = 1;
	    nodecharts->domcharts[zeroInd].region_id = opcnt->dominfo[i].region_id;
	    nodecharts->domcharts[zeroInd].dom_id = opcnt->dominfo[i].dom_id;
	    nodecharts->domcharts[zeroInd].nod_mem = opcnt->dominfo[i].nod_mem;
	    nodecharts->domcnt++;
	} else if (reg == 0 && zeroInd < 0 ) {
	    // Look which one is present in nodecharts that is not present in opcnt
	    #ifdef MM_NODINF_DOM_REG_DEBUG
	    printf("func %s, note: look which one is present in nodecharts, reg=%i, zeroInd=%i\n", 
			__func__, reg, zeroInd);
	    #endif
	    for (j=0; j < nodecharts->domcnt; j++) {
		present = 0;
		for (k=0; k < opcnt->domcnt; k++) {
		    if ( (nodecharts->domcharts[j].region_id == opcnt->dominfo[k].region_id) &&
			 (nodecharts->domcharts[j].dom_id == opcnt->dominfo[k].dom_id) ) {
			present = 1;
		    }
		}

		if (present == 0) {
		    zeroInd = j;
		    goto dom_register_reg_dom0; 
		}

	    }
	}
    }

    if (reg == 1)
	rc = SUCC_DOM_REG;
    else
	rc = ERR_DOM_REG;


    return rc;
}

int32_t insert_element_to_entrylist(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, 
                                        struct pernodechart *nodecharts, uint32_t this_region_id, 
                                        uint32_t node_no, uint32_t this_node_id, struct node_info *ninf ) {
    int32_t rc, rmopcnt_id=-1;
    uint32_t i=0, j=0;
    uint64_t diffts=0;

    if (nodecharts->time.list_last) {
	if (nodecharts->time.list_last->v > opcnt->curr_ts) {
	    printf("func %s, note: prev timestamp larger than new one. Stale data, do not store\n", __func__);
	    return -1;
        }

	diffts = opcnt->curr_ts - nodecharts->time.list_last->v;
	if (diffts < MM_SAMPLING_TIME) {
	    printf("func %s, note: diffts=%lu, less than %lu\n", __func__, diffts, (unsigned long)(MM_SAMPLING_TIME));
	}
    }

    if (ninf[this_node_id].master == 1) {
        for (i = 0; i < node_no; i++) {
            if (this_region_id == rmopcnt[i].region_id) {
                rmopcnt_id = i;
            }
        }
    } else {
        rmopcnt_id = 0;
    }
    

    if ( rmopcnt_id == -1) {
        printf("func %s, error: MASSIVE, rmopcnt_id=-1, it's gonna crash\n", __func__ );
        exit(0);
    }

    if (nodecharts->time.size == WINDOW_SIZE && nodecharts->time.list_root) {
	struct value_entry *tve;
	struct nodechart_entry *nce;

	tve = nodecharts->time.list_root;
	nodecharts->time.list_root = nodecharts->time.list_root->next;
	nodecharts->time.list_root->prev = NULL;
	nodecharts->time.list_last->next = tve;
	tve->prev = nodecharts->time.list_last;
	tve->next = NULL;
	tve->v = opcnt->curr_ts;
	nodecharts->time.list_last = tve;

	nce = nodecharts->nodestats.list_root;
	nodecharts->nodestats.list_root = nodecharts->nodestats.list_root->next;
	nodecharts->nodestats.list_root->prev = NULL;
	nodecharts->nodestats.list_last->next = nce;
	nce->prev = nodecharts->nodestats.list_last;
	nce->next = NULL;

	copy_nodestats(opcnt, &rmopcnt[rmopcnt_id], this_node_id, ninf, nce, 
				opcnt->curr_ts, nodecharts->time.list_last->prev->v, nodecharts->start_ts, 
				nodecharts->nodestats.list_last->total_puts, 
				nodecharts->nodestats.list_last->total_gets, 
				nodecharts->nodestats.list_last->total_flush, 

				nodecharts->nodestats.list_last->local_puts, 
				nodecharts->nodestats.list_last->local_gets,
				nodecharts->nodestats.list_last->local_flush,

				nodecharts->nodestats.list_last->remote_puts,
				nodecharts->nodestats.list_last->remote_gets,
				nodecharts->nodestats.list_last->remote_flush );
	nodecharts->nodestats.list_last = nce;

        for (i=0; i < /*nodecharts->domcnt*/ MAX_DOMAINS; i++) {
            for (j=0; j < opcnt->domcnt; j++) {
                if (i < opcnt->domcnt) {
		    if (nodecharts->domcharts[i].region_id == opcnt->dominfo[j].region_id) {
		        if (nodecharts->domcharts[i].dom_id == opcnt->dominfo[j].dom_id) {
			    struct domchart_entry *dce;

			    dce = nodecharts->domcharts[i].domstats.list_root;
			    nodecharts->domcharts[i].domstats.list_root = nodecharts->domcharts[i].domstats.list_root->next;
			    nodecharts->domcharts[i].domstats.list_root->prev = NULL;
			    nodecharts->domcharts[i].domstats.list_last->next = dce;
			    dce->prev = nodecharts->domcharts[i].domstats.list_last;
			    dce->next = NULL;
			    copy_domstats(opcnt, &rmopcnt[rmopcnt_id], j, dce, 
					opcnt->curr_ts, nodecharts->time.list_last->prev->v, nodecharts->start_ts, 
					nodecharts->domcharts[i].domstats.list_last->puts,
					nodecharts->domcharts[i].domstats.list_last->gets,
					nodecharts->domcharts[i].domstats.list_last->flush,
					nodecharts->domcharts[i].domstats.list_last->putssucc,
					nodecharts->domcharts[i].domstats.list_last->getssucc,
                                        /*new addons*/ nodecharts->domcharts[i].domstats.list_last->local_puts, 
                                        nodecharts->domcharts[i].domstats.list_last->local_gets, 
                                        nodecharts->domcharts[i].domstats.list_last->local_flush,
                                        nodecharts->domcharts[i].domstats.list_last->local_putssucc,
                                        nodecharts->domcharts[i].domstats.list_last->local_getssucc, 
                                        nodecharts->domcharts[i].domstats.list_last->rem_puts,
                                        nodecharts->domcharts[i].domstats.list_last->rem_gets,
                                        nodecharts->domcharts[i].domstats.list_last->rem_flush,
                                        nodecharts->domcharts[i].domstats.list_last->rem_putssucc,
                                        nodecharts->domcharts[i].domstats.list_last->rem_getssucc );
			    nodecharts->domcharts[i].domstats.list_last = dce;

		        }
		    }
                } else {
                    struct domchart_entry *dce;

                    dce = nodecharts->domcharts[i].domstats.list_root;
                    nodecharts->domcharts[i].domstats.list_root = nodecharts->domcharts[i].domstats.list_root->next;
                    nodecharts->domcharts[i].domstats.list_root->prev = NULL;
                    nodecharts->domcharts[i].domstats.list_last->next = dce;
                    dce->prev = nodecharts->domcharts[i].domstats.list_last;
                    dce->next = NULL;

                    setzero_domstats(dce, nodecharts->start_ts);
                    nodecharts->domcharts[i].domstats.list_last = dce;
                }
	    }
	}	

	rc = 1;
    } else if (nodecharts->time.size > 0 && nodecharts->time.list_root ) {
	nodecharts->time.list_last->next = (struct value_entry *)malloc(sizeof(struct value_entry) );
	nodecharts->time.list_last->next->v = opcnt->curr_ts;
	nodecharts->time.list_last->next->prev = nodecharts->time.list_last;
	nodecharts->time.list_last = nodecharts->time.list_last->next;
	nodecharts->time.list_last->next = NULL;
	nodecharts->time.size++;

	nodecharts->nodestats.list_last->next = (struct nodechart_entry *)malloc(sizeof(struct nodechart_entry) );
	nodecharts->nodestats.list_last->next->prev = nodecharts->nodestats.list_last;
	copy_nodestats(opcnt, &rmopcnt[rmopcnt_id], this_node_id, ninf, nodecharts->nodestats.list_last->next, 
				opcnt->curr_ts, nodecharts->time.list_last->prev->v, nodecharts->start_ts, 
				nodecharts->nodestats.list_last->total_puts, 
				nodecharts->nodestats.list_last->total_gets, 
				nodecharts->nodestats.list_last->total_flush, 

				nodecharts->nodestats.list_last->local_puts, 
				nodecharts->nodestats.list_last->local_gets,
				nodecharts->nodestats.list_last->local_flush,

				nodecharts->nodestats.list_last->remote_puts,
				nodecharts->nodestats.list_last->remote_gets,
				nodecharts->nodestats.list_last->remote_flush);
	nodecharts->nodestats.list_last = nodecharts->nodestats.list_last->next;
	nodecharts->nodestats.list_last->next = NULL;
	nodecharts->nodestats.size++;

        for (i=0; i < /*nodecharts->domcnt*/ MAX_DOMAINS; i++) {
            for (j=0; j < opcnt->domcnt; j++) {
                if (i < opcnt->domcnt) {
		    if (nodecharts->domcharts[i].region_id == opcnt->dominfo[j].region_id) {
		        if (nodecharts->domcharts[i].dom_id == opcnt->dominfo[j].dom_id) {
			    //printf("nodecharts->domcharts[%i].dom_id=%i, opcnt->dominfo[%i].dom_id=%i\n", 
				    //i, nodecharts->domcharts[i].dom_id,
				    //j, opcnt->dominfo[j].dom_id);
			    nodecharts->domcharts[i].domstats.list_last->next = (struct domchart_entry *) malloc(sizeof(struct domchart_entry) );
			    nodecharts->domcharts[i].domstats.list_last->next->prev = nodecharts->domcharts[i].domstats.list_last;
			    copy_domstats(opcnt, &rmopcnt[rmopcnt_id], j, nodecharts->domcharts[i].domstats.list_last->next, 
					    opcnt->curr_ts, nodecharts->time.list_last->prev->v, nodecharts->start_ts,
					    nodecharts->domcharts[i].domstats.list_last->puts, 
					    nodecharts->domcharts[i].domstats.list_last->gets,
					    nodecharts->domcharts[i].domstats.list_last->flush,
					    nodecharts->domcharts[i].domstats.list_last->putssucc,
					    nodecharts->domcharts[i].domstats.list_last->getssucc,
                                            /*new addons*/ nodecharts->domcharts[i].domstats.list_last->local_puts,
                                            nodecharts->domcharts[i].domstats.list_last->local_gets, 
                                            nodecharts->domcharts[i].domstats.list_last->local_flush,
                                            nodecharts->domcharts[i].domstats.list_last->local_putssucc,
                                            nodecharts->domcharts[i].domstats.list_last->local_getssucc, 
                                            nodecharts->domcharts[i].domstats.list_last->rem_puts, 
                                            nodecharts->domcharts[i].domstats.list_last->rem_gets, 
                                            nodecharts->domcharts[i].domstats.list_last->rem_flush,
                                            nodecharts->domcharts[i].domstats.list_last->rem_putssucc,
                                            nodecharts->domcharts[i].domstats.list_last->rem_getssucc );
			    nodecharts->domcharts[i].domstats.list_last = nodecharts->domcharts[i].domstats.list_last->next;
			    nodecharts->domcharts[i].domstats.list_last->next = NULL;

			    nodecharts->domcharts[i].domstats.size++;
		        }   
		    }    
                } else {
                    nodecharts->domcharts[i].domstats.list_last->next = (struct domchart_entry *) malloc(sizeof(struct domchart_entry) );
                    nodecharts->domcharts[i].domstats.list_last->next->prev = nodecharts->domcharts[i].domstats.list_last;
                    setzero_domstats(nodecharts->domcharts[i].domstats.list_last->next, nodecharts->start_ts);
                   
                    nodecharts->domcharts[i].domstats.list_last = nodecharts->domcharts[i].domstats.list_last->next;
                    nodecharts->domcharts[i].domstats.list_last->next = NULL;

                    nodecharts->domcharts[i].domstats.size++;
                }  
	    }
	}
	
	rc = 1;	
    } else if (nodecharts->time.size == 0 && !nodecharts->time.list_root && !nodecharts->time.list_last) {

	printf("\tfunc %s, note: root for chart structs\n", __func__);

	if (opcnt->region_id != this_region_id) {
	    printf("func %s, error: MASSIVE, this_region_id=%i, opcnt->region-id=%i, not equal\n", 
			__func__, this_region_id, opcnt->region_id);
	    return ERR_REGION_ID_INCONSISTENCY;
	}
	nodecharts->region_id = opcnt->region_id;
	nodecharts->sys_mem = opcnt->sys_mem;

	nodecharts->time.list_root = (struct value_entry *)malloc(sizeof(struct value_entry) );
	nodecharts->time.list_root->v = opcnt->curr_ts;
	nodecharts->start_ts = opcnt->curr_ts;		// starting timestamp
	nodecharts->time.list_last = nodecharts->time.list_root;	
	nodecharts->time.list_root->prev = NULL;
	nodecharts->time.list_root->next = NULL;

	printf("\tfunc %s, note: root 2\n", __func__);
	nodecharts->nodestats.list_root = (struct nodechart_entry *)malloc(sizeof(struct nodechart_entry) );
	rc = copy_nodestats(opcnt, &rmopcnt[rmopcnt_id], this_node_id, ninf, nodecharts->nodestats.list_root, nodecharts->start_ts, 0, 0, 
					0, 0, 0,
					0, 0, 0,
					0, 0, 0);
	nodecharts->nodestats.list_last = nodecharts->nodestats.list_root;	
	nodecharts->nodestats.list_root->prev = NULL;
	nodecharts->nodestats.list_root->next = NULL;
	printf("\tfunc %s, note: root 3\n", __func__);
	for (i=0; i < /*nodecharts->domcnt*/ MAX_DOMAINS; i++) {
	    for (j=0; j < opcnt->domcnt; j++) {
                if ( i < opcnt->domcnt ) {
		    if (nodecharts->domcharts[i].region_id == opcnt->dominfo[j].region_id) {
		        if (nodecharts->domcharts[i].dom_id == opcnt->dominfo[j].dom_id) {
			    printf("\tfunc %s, note: root, filling dom\n", __func__);

			    nodecharts->domcharts[i].domstats.list_root = (struct domchart_entry *) malloc(sizeof(struct domchart_entry) );
			    copy_domstats(opcnt, &rmopcnt[rmopcnt_id], j, nodecharts->domcharts[i].domstats.list_root, nodecharts->start_ts, 0, 0, 
					0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0 );
			    nodecharts->domcharts[i].domstats.list_last =  nodecharts->domcharts[i].domstats.list_root;
			    nodecharts->domcharts[i].domstats.list_root->prev = NULL;
			    nodecharts->domcharts[i].domstats.list_root->next = NULL;

			    nodecharts->domcharts[i].domstats.size++;
		        }
		    }
                } else {
                    nodecharts->domcharts[i].domstats.list_root = (struct domchart_entry *) malloc(sizeof(struct domchart_entry) );
                    setzero_domstats(nodecharts->domcharts[i].domstats.list_root, nodecharts->start_ts);
                    nodecharts->domcharts[i].domstats.list_last =  nodecharts->domcharts[i].domstats.list_root;

                    nodecharts->domcharts[i].domstats.list_root->prev = NULL;
                    nodecharts->domcharts[i].domstats.list_root->next = NULL;

                    nodecharts->domcharts[i].domstats.size++; 
                }
	    }
	}

	nodecharts->time.size++;
	nodecharts->nodestats.size++;
	rc = 1;
    } else {
	printf("\tfunc %s, error: nodecharts->time.size=%lu, nodecharts->time.list_root=%lu, nodecharts->time.list_last=%lu, big problems\n", 
			__func__, nodecharts->time.size, 
			(long unsigned int)nodecharts->time.list_root, 
			(long unsigned int)nodecharts->time.list_last);
	rc = ERR_INSERT_ENTRYLIST;
    }

    if (rc>0) {
	nodecharts->entry_count++;
    }

    #ifdef MM_STATS_FILE_PRINT
    if (nodecharts->entry_count == WINDOW_SIZE) {
	//printf("func %s, note: nodecharts->entry_count=%lu\n", __func__, nodecharts->entry_count);
	nodecharts->entry_count=0;
	rc = write_stats_to_file(nodecharts);
    }
    #endif

    return rc;
}


int32_t node_updt(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart * nodecharts, 
                      uint32_t this_region_id, uint32_t node_no, int32_t this_node_id, struct node_info *ninf) {
    int32_t rc=0, i=0, j=0, k=0, zeroInd=-1;
    uint8_t reg=0, present=0;

    if (opcnt->domcnt > MAX_DOMAINS) {
	#ifdef MM_NODINF_DOMUPDT_DEBUG
	printf("func %s, error: opcnt->domcnt=%i, MAX_DOMAINS=%i\n", 
			__func__, opcnt->domcnt, MAX_DOMAINS);
	#endif
	return 0;
    }

    // opcnt is the reference. The golden patterns
    // 1. Check the status of the domain registrations
    rc = dom_register( opcnt, nodecharts);

    #ifdef MM_NODINF_DOMUPDT_DEBUG
    for (i=0; i < nodecharts->domcnt; i++) {
	printf("func %s, note: nodecharts->domcharts[%i].region_id=%i, nodecharts->domcharts[%i].dom_id=%i\n", 
			__func__, i, nodecharts->domcharts[i].region_id, 
			i, nodecharts->domcharts[i].dom_id);
    }
    #endif

    if (rc == ERR_DOM_REG) {
	printf("func %s, error: cannot register domain\n", __func__);
	return ERR_NODE_UPDT;
    }

    // 2. Put that in the corresponding list.
    rc = insert_element_to_entrylist(opcnt, rmopcnt, nodecharts, this_region_id, node_no, this_node_id, ninf);
    
    return SUCC_NODE_UPDT;
}


uint32_t get_this_node_id(struct node_info *ninf, uint32_t node_no) {
    int i =0;
    uint32_t node_id;

    //printf("getting node_id\n");
    for (i=0; i < node_no; i++) {
	//printf("i=%i\n", i);
	if (ninf[i].self == 1) {
	    node_id = ninf[i].node_id;
	}
    }
    //printf("got node_id\n");

    return node_id;
}

uint32_t get_this_region_id(struct node_info *ninf, uint32_t node_no) {
    int i =0;
    uint32_t region_id;

    for (i=0; i < node_no; i++) {
	if (ninf[i].self == 1) {
	    region_id = ninf[i].region;
	}
    }

    return region_id;
}

uint64_t get_this_tmempages(struct node_info *ninf, uint32_t node_no) {
    int i =0;
    uint64_t num_pages;

    for (i=0; i < node_no; i++) {
	if (ninf[i].self == 1) {
	    num_pages = ninf[i].num_pages;
	}
    }

    return num_pages;
}

int32_t rmtaggr_cnt_clear(struct rmtaggr_cnt *rmopcnt) {
    uint32_t i = 0;

    rmopcnt->region_id = 0;
   
    // MM_REQ_MCAP stats 
    rmopcnt->req_mcap_in = 0;
    rmopcnt->req_mcap_inpagreq = 0;
    rmopcnt->rcv_mcap_out = 0;
    rmopcnt->rcv_mcap_outpagsent = 0;

    rmopcnt->req_mcap_out = 0;
    rmopcnt->req_mcap_outpagreq = 0;
    rmopcnt->rcv_mcap_in = 0;
    rmopcnt->rcv_mcap_inpagrcvd = 0;

    // MM_RECLAIM/RECLACK_STATS
    rmopcnt->reclaim_mcap_in = 0;
    rmopcnt->reclaim_mcap_inpagret = 0;
    rmopcnt->reclack_mcap_out = 0;
    rmopcnt->reclack_mcap_outpagret = 0;

    rmopcnt->reclaim_mcap_out = 0;
    rmopcnt->reclaim_mcap_outpagreq = 0;
    rmopcnt->reclack_mcap_in = 0;
    rmopcnt->reclack_mcap_inpagret = 0;

    // MM_RELACK_MCAP stats, (relinquish)
    rmopcnt->relack_mcap_in = 0;
    rmopcnt->relack_mcap_inpagret = 0;
    rmopcnt->relack_mcap_out = 0;
    rmopcnt->relack_mcap_outpagret = 0;

    rmopcnt->remote_pages_taken = 0;
    rmopcnt->remote_pages_given = 0;
    rmopcnt->remote_pages_dispin = 0;
    rmopcnt->remote_pages_dispout = 0;
 
    for (i = 0; i < MAX_RMTNODES; i++) {
        rmopcnt->nodinfo[i].region_id = 0;

        //
        rmopcnt->nodinfo[i].req_mcap_in = 0;
        rmopcnt->nodinfo[i].req_mcap_inpagreq = 0;
        rmopcnt->nodinfo[i].rcv_mcap_out = 0;
        rmopcnt->nodinfo[i].rcv_mcap_outpagsent = 0;

        rmopcnt->nodinfo[i].req_mcap_out = 0;
        rmopcnt->nodinfo[i].req_mcap_outpagreq = 0;
        rmopcnt->nodinfo[i].rcv_mcap_in = 0;
        rmopcnt->nodinfo[i].rcv_mcap_inpagrcvd = 0;


        //
        rmopcnt->nodinfo[i].reclaim_mcap_in = 0;
        rmopcnt->nodinfo[i].reclaim_mcap_inpagret = 0;
        rmopcnt->nodinfo[i].reclack_mcap_out = 0;
        rmopcnt->nodinfo[i].reclack_mcap_outpagret = 0;

        rmopcnt->nodinfo[i].reclaim_mcap_out = 0;
        rmopcnt->nodinfo[i].reclaim_mcap_outpagreq = 0;
        rmopcnt->nodinfo[i].reclack_mcap_in = 0;
        rmopcnt->nodinfo[i].reclack_mcap_inpagret = 0;

        // 
        rmopcnt->nodinfo[i].relack_mcap_in = 0;
        rmopcnt->nodinfo[i].relack_mcap_inpagret = 0;
        rmopcnt->nodinfo[i].relack_mcap_out = 0;
        rmopcnt->nodinfo[i].relack_mcap_outpagret = 0;

        
        //
        rmopcnt->nodinfo[i].remote_pages_taken = 0;
        rmopcnt->nodinfo[i].remote_pages_given = 0;
        rmopcnt->nodinfo[i].remote_pages_dispin = 0;
        rmopcnt->nodinfo[i].remote_pages_dispout = 0;


        // 
        rmopcnt->nodinfo[i].reallatency2node = 0;
        rmopcnt->nodinfo[i].estilatency2node = 0;
    }

    return SUCC_RMTAGGRCNT_CLEAR;
}

int32_t tmop_cnt_clear(struct tmop_cnt * opcnt) { 
    int32_t i=0;

    opcnt->curr_ts=0;
    opcnt->prev_ts=0;
    opcnt->free_pages=0;
    opcnt->tmem_freeable_pages=0;
    opcnt->domcnt=0;
    opcnt->min_request_pages=0;
    opcnt->region_id = 0; //this_region_id;
    opcnt->sys_mem = 0;	
    opcnt->tmempages = 0;

    opcnt->total_accesses=0;

    opcnt->total_list = 0; //(ninf[this_node_id].num_pages > ninf[this_node_id].local_init_pages) ? (ninf[this_node_id].num_pages - ninf[this_node_id].local_init_pages) : 0;
    opcnt->local_list = 0; //ninf[this_node_id].local_init_pages;
    opcnt->remote_list=0;
    opcnt->given_list=0;
    opcnt->used_list=0;

    opcnt->total_puts=0;
    opcnt->total_putssucc=0;
    opcnt->total_gets=0;
    opcnt->total_getssucc=0;
    opcnt->total_flush=0;
    opcnt->total_flushsucc=0;

    opcnt->local_puts=0;
    opcnt->local_gets=0;
    opcnt->local_flush=0;
    opcnt->local_accesses=0;

    opcnt->rem_puts=0;
    opcnt->rem_gets=0;
    opcnt->rem_flush=0;

    opcnt->total_tmem=0;
    opcnt->local_tmem=0;
    opcnt->remote_tmem=0;

    for (i=0; i < MAX_DOMAINS; i++) {
	opcnt->dominfo[i].region_id=0;
	opcnt->dominfo[i].dom_id=-1;
 	opcnt->dominfo[i].total_access=0;
	opcnt->dominfo[i].imm_alloc=0;

	opcnt->dominfo[i].mm_target=0;
	opcnt->dominfo[i].mm_slack=0;
 	opcnt->dominfo[i].dn_target=0;
	opcnt->dominfo[i].breached=0;

	opcnt->dominfo[i].nod_mem=0;
	opcnt->dominfo[i].nod_tmem=0;
	//opcnt->dominfo[i].nod_tmem_rsv=0;

	opcnt->dominfo[i].nod_puts_tot=0;
	opcnt->dominfo[i].nod_puts_succ=0;
    
	opcnt->dominfo[i].nod_gets_tot=0;
	opcnt->dominfo[i].nod_gets_succ=0;

	opcnt->dominfo[i].nod_flush_tot=0;
	opcnt->dominfo[i].nod_flush_succ=0;
	

        opcnt->dominfo[i].total_local_access = 0;
        opcnt->dominfo[i].nod_puts_local_tot=0;
	opcnt->dominfo[i].nod_puts_local_succ=0;
    
	opcnt->dominfo[i].nod_gets_local_tot=0;
	opcnt->dominfo[i].nod_gets_local_succ=0;

	opcnt->dominfo[i].nod_flush_local_tot=0;
	opcnt->dominfo[i].nod_flush_local_succ=0;
	

        opcnt->dominfo[i].total_rem_access = 0;
        opcnt->dominfo[i].nod_puts_rem_tot=0;
	opcnt->dominfo[i].nod_puts_rem_succ=0;
    
	opcnt->dominfo[i].nod_gets_rem_tot=0;
	opcnt->dominfo[i].nod_gets_rem_succ=0;

	opcnt->dominfo[i].nod_flush_rem_tot=0;
	opcnt->dominfo[i].nod_flush_rem_succ=0;
    }
    return SUCC_TMOPCNT_CLEAR;
}

int32_t graph_clear(struct llnode *graph) {
    int32_t i=0, j=0;

    for (i=0; i < MAX_RMTNODES; i++) {
        graph[i].label = 0;
        for (j=0; j < MAX_RMTNODES; j++) {
            graph[i].myedges[j].label = 0;
            graph[i].myedges[j].weight = 0;
        }
    }
}

int32_t node_register(struct tmop_cnt* opcnt, struct rmtaggr_cnt *rmopcnt, 
                         struct node_info *ninf, uint32_t node_no, uint32_t this_node_id) {
    int32_t i=0, j=0, k=0, rc =SUCC_NODE_REGISTER, itern=0;

    rc = graph_clear(prevgrph);
    if (rc != GRAPH_CLEAR_SUCC) {
        printf("func %s, error: graph structure cannot be cleared\n", __func__);
    }

    if (ninf[this_node_id].master == 1) 
	itern = node_no;
    else
	itern = 1;

    
    for (i=0; i < itern; i++) {
	rc = tmop_cnt_clear(&opcnt[i] );
   
	if (rc != SUCC_TMOPCNT_CLEAR) {
	    #ifdef MM_NODE_REGISTER_DEBUG
	    printf("func %s, error: could not clear opcnt\n", __func__);
	    #endif
	    rc = ERR_NODE_REGISTER_CANTCLR;
	    break;
	}
    }

    opcnt[0].region_id = ninf[this_node_id].region;
    opcnt[0].total_list = (ninf[this_node_id].num_pages > ninf[this_node_id].local_init_pages) ? (ninf[this_node_id].num_pages - ninf[this_node_id].local_init_pages) : 0;
    opcnt[0].local_list = ninf[this_node_id].local_init_pages;

    if (ninf[this_node_id].master == 1 ) {
        k=1;
        for (i = 0; i < itern; i++) {
            if (i != this_node_id) {
                opcnt[k].region_id = ninf[i].region;
                opcnt[k].total_list = (ninf[i].num_pages > ninf[i].local_init_pages) ? (ninf[i].num_pages - ninf[i].local_init_pages) : 0;
                opcnt[k].local_list = ninf[i].local_init_pages;
                k++;
            }
        }
    }

    k = 0;
    for (i = 0; i < itern; i++) {
        rc = rmtaggr_cnt_clear( &rmopcnt[i] );

        if ( ninf[this_node_id].master == 1 ) {
            rmopcnt[i].region_id = ninf[i].region;
        } else {
            rmopcnt[i].region_id = ninf[this_node_id].region;
        }
        printf("\t\tfunc %s, note: ninf[%i].region=%i, rmopcnt[%i].region_id=%i\n", __func__, 
                  i, ninf[i].region, i, rmopcnt[i].region_id );

        for (j = 0; j < node_no; j++) {
            if (j != this_node_id) {
                rmopcnt[i].nodinfo[k].region_id = ninf[j].region;
                k++;
            }
        }

    }

    return rc;
}

int32_t pernodechart_clear(struct pernodechart * nodecharts, uint32_t this_node_id) {
    uint32_t i=0, j=0;

    nodecharts->region_id=0;
    nodecharts->tmempages=0;
    nodecharts->sys_mem=0;
    nodecharts->domcnt=0;
    nodecharts->entry_count=0;
    nodecharts->start_ts=0;

    nodecharts->time.size=0;
    nodecharts->time.list_last = NULL;
    nodecharts->time.list_root = NULL;

    nodecharts->nodestats.size = 0;
    nodecharts->nodestats.list_last = NULL;
    nodecharts->nodestats.list_root = NULL;

    for (i = 0; i < MAX_DOMAINS; i++) {
	nodecharts->domcharts[i].region_id=0;
	nodecharts->domcharts[i].dom_id=0;
	nodecharts->domcharts[i].nod_mem=0;

	nodecharts->domcharts[i].domstats.size = 0;
	nodecharts->domcharts[i].domstats.list_root = NULL;
	nodecharts->domcharts[i].domstats.list_last = NULL;
    }

    return SUCC_PERNODECHART_CLEAR;

}

// graph building functions

int32_t build_graph( struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint64_t curr_ts, struct node_info *ninf, uint32_t node_no, uint32_t this_node_id ) {
    int32_t rc = 0, i =0, j=0, l=0, k=0, n=0, m=0, regnodes=0, grph_changed=0;
    struct llnode nodes[MAX_RMTNODES];
    char *cfn, *cfn_suffix;
    FILE *cfd = NULL;

    // clear the data structure
    for (i=0; i < MAX_RMTNODES; i++) {
        nodes[i].label = 0;
        for (j=0; j < MAX_RMTNODES; j++) {
            nodes[i].myedges[j].label = 0;
            nodes[i].myedges[j].weight = 0;
        }
    }
    j=0; 
    i=0;

    #ifdef MM_BUILD_GRAPH_DEBUG
    printf("\n\nfunc %s, note: starting to write new graph\n", __func__);
    #endif

    // To build the graph, we go into every entry of remopcnt (node), and see where it points to
    // We try to check for consistency among the edges while doing so, and send notifications
    // in case something is not right
    for (i=0; i < MAX_RMTNODES; i++) {
        if (rmopcnt[i].region_id > 0) {
            nodes[j].label = rmopcnt[i].region_id;

            #ifdef MM_BUILD_GRAPH_DEBUG
            printf("\tfunc %s, note: rmopcnt[%i].region_id=%i, nodes[%i].label=%i\n", 
                           __func__, i, rmopcnt[i].region_id, j, nodes[j].label );
            #endif
 
            for (k=0; k < MAX_RMTNODES; k++) {

                //if (rmopcnt[i].nodinfo[k].remote_pages_given > 0 && (rmopcnt[i].nodinfo[k].region_id > 0)) {
                if (rmopcnt[i].nodinfo[k].remote_pages_taken > 0 && (rmopcnt[i].nodinfo[k].region_id > 0)) {
                    nodes[j].myedges[l].label = rmopcnt[i].nodinfo[k].region_id;
                    //nodes[j].myedges[l].weight = rmopcnt[i].nodinfo[k].remote_pages_given;
                    nodes[j].myedges[l].weight = rmopcnt[i].nodinfo[k].remote_pages_taken;

                    #ifdef MM_BUILD_GRAPH_DEBUG
                    printf("\tfunc %s, note: rmopcnt[%i].nodinfo[%i], .region_id=%i, .remote_pages_given=%lu, .remote_pages_taken=%lu, nodes[%i].myedges[%i], .label=%i, .weight=%lu\n", 
                                 __func__, i, k, rmopcnt[i].nodinfo[k].region_id, rmopcnt[i].nodinfo[k].remote_pages_given, rmopcnt[i].nodinfo[k].remote_pages_taken,
                                           j, l, nodes[j].myedges[l].label, nodes[j].myedges[l].weight);
                    #endif
 
                    // check for consistency. Check that rmopcnt[].nodinfo[k].remote_pages_taken
                    /*for (m=0; m < MAX_RMTNODES; m++) {
                        if (rmopcnt[m].region_id == rmopcnt[i].nodinfo[k].region_id) {
                            for (n = 0; n < MAX_RMTNODES; n++) {
                                if (rmopcnt[i].region_id == rmopcnt[m].nodinfo[n].region_id) {
                                    if (rmopcnt[m].nodinfo[n].remote_pages_taken == rmopcnt[i].nodinfo[k].remote_pages_given) {
                                        printf("\tfunc %s, note: graph is consistent\n", __func__);
                                    } else {
                                        printf("\tfunc %s, note: graph NOT consistent\n", __func__);
                                    }
                                }
                            }
                            
                        }
                    }   */ 
                    // finished checking consistency                

                    l = l + 1;
                }
            }

            j = j + 1;  
            l=0; 
        }
    }
    regnodes = j;
    
    for (i=0; (i < MAX_RMTNODES) && (grph_changed == 0); i++) {
        if (prevgrph[i].label != nodes[i].label) {
            grph_changed = 1;
            prevgrph[i].label = nodes[i].label;
        }

        for (j=0; j < MAX_RMTNODES; j++) {
            if (nodes[i].myedges[j].label != prevgrph[i].myedges[j].label) {
                grph_changed = 1;
                prevgrph[i].myedges[j].label = nodes[i].myedges[j].label;
            }
            
            if (nodes[i].myedges[j].weight != prevgrph[i].myedges[j].weight) {
                grph_changed = 1;
                prevgrph[i].myedges[j].weight = nodes[i].myedges[j].weight;
            }
        }
    }
    //grph_changed = 1;

    // print it out to the file to the graph file if changes are detected
    if (grph_changed == 1 && curr_ts >0) {
        printf("\tfunc, %s, note: grph_changed == 1\n", __func__);
        cfn = (char*)malloc(GRAPH_FILENAME_SIZE*sizeof(char));
        cfn_suffix = (char *)malloc(GRAPH_FILENAME_SIZE*sizeof(char));
        for (i=0; i < GRAPH_FILENAME_SIZE; i++) {
	    cfn[i] = 0;
	    cfn_suffix[i] = 0;
        }
        strcpy(cfn, MM_GRAPHFILE);

        //printf("func %s, note: cfn = strcat\n", __func__);
        sprintf(cfn_suffix, "_ts=%lu", curr_ts);
        cfn = strcat(cfn, cfn_suffix);

        strcpy(cfn_suffix, MM_GRAPHFILE_EXTENSION);
        cfn = strcat(cfn, cfn_suffix);

        cfd = fopen(cfn, "w");

        fprintf(cfd, "digraph G {\n" );
        for (i=0; i < regnodes && (nodes[i].label>0); i++) {
            fprintf(cfd, "\tnode_%i [shape=circle,label=\"%i\"];\n", nodes[i].label, nodes[i].label);       
        }
        fprintf(cfd, "\n" );

        for (i=0; i < regnodes; i++) {
            for (j = 0; (j < regnodes) && (nodes[i].label>0); j++) {
                if (nodes[i].label != nodes[i].myedges[j].label && (nodes[i].myedges[j].label > 0)) {
                    fprintf(cfd, "\tnode_%i -> node_%i [label=\"%lu\"];\n", nodes[i].label, nodes[i].myedges[j].label, nodes[i].myedges[j].weight);
                }
            }
        }
        fprintf(cfd, "}" );


        fclose(cfd);
        free(cfn);
        free(cfn_suffix);
    }
    rc = 1;

    #ifdef MM_BUILD_GRAPH_DEBUG
    printf("\n\nfunc %s, note: finishing graph\n", __func__);
    #endif

    return rc;
}
