#ifndef MM_INC_TMEMIF_FILE
#define MM_INC_TMEMIF_FILE

#include "node_info.h"

#define TMIF_NETLINK_SOCKETS
//#define TMIF_PROCFS
//#define TMIF_DEBUG
//#define MM_TMIF_RCV_STATS_DEBUG

#define E_PFS_NOTOPEN         50
#define E_TMSOCK_CANNOT_OPEN  51
#define E_TMSOCK_RDFLAG       52
#define E_TMSOCK_WRFLAG       53

#define TMEM_MOD_CLOSE_SUCC   0
#define TMEM_MOD_READ_SUCC    0
#define TMIF_READ_SUCC	      0
#define SUCC_TMEM_MOD_INIT    0

#define TMEM_MOD_READ_FAIL    1
#define TMEM_MOD_WRITE_SUCC   2
#define TMIF_WRITE_SUCC   2
#define TMEM_MOD_WRITE_FAIL   3
#define TMIF_WRITE_FAIL   3


/* communication with netlink sockets */
#define NETLINK_USER 31
#define MAX_PAYLOAD_HYP_CMD	sizeof(struct mm_hyp_cmd)
#define MAX_PAYLOAD_STATS	sizeof(struct tmop_cnt)
#define TMIF_RCVD_EXPECTED	1224 // 1192  /*744*/


int tmif_init();
int tmif_send(struct mm_hyp_cmd *mmcmd);
int32_t tmif_rcv_stats(struct tmop_cnt *opcnt);
int tmif_rcv_mcap(struct mm_hyp_cmd *tmcmd, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                    int64_t * entries, uint32_t this_region_id, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf);
int tmif_close();

int tmif_wr_test();


#endif
