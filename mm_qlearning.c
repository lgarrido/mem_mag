#include <stdio.h>
#include <math.h>

#include "mm_policy.h"



#ifdef POLICY_5
// Machine learning policy


#include <stdlib.h>

#define NOS    20             // NOS: Number of States
#define NOA     3             // NOA: Number of actions
#define MTPFR   7000.0        // Maximum Tolerable Put Failed rate
#define MTPR    7000.0        // Maximum Tolerable Put Rate.

float pfr_ranges[NOS+1];          // these are the ranges of the quantizied state space. Plus 1, to account for the zero 
float pfr_rvalues[NOS+2][NOA];    // These are the reward values for a given state. Plus 2, to account for the zero, and the "beyond"
float pfr_qtable[NOS+2][NOA];     // Table for calculated Q-Values. To account for the zero and the beyond.

uint8_t init = 0;

uint32_t memmag(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt,
                        struct pernodechart *nodecharts, struct mm_hyp_cmd *rep,
                        uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {

    int32_t i, j, rc=0;
   
    if (init == 0) {
        // Populate qtable.
        int32_t j=0;
        float step = MTPFR/NOS;

        for (i=0; i < NOS+1; i++) {
            pfranges[i] = step*((float)i);
        }

        // Filling up the state of the zero for the corresponding actions
        for (j=0; j < NOA; j++)

        for (i=1; i < NOS+2; i++) {
            for (j=0; j < NOA; j++) {
                pfr_values[i][j] = ;

                pfr_qtable[i][j] = 0;
            }
        }

        init = 1;
    }

    return rc;
}


#endif
