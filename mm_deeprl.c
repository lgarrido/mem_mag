#include <stdio.h>
#include <math.h>


#include "mm_ctrl.h"
#include "mm_comm.h"
#include "mm_policy.h"



#ifdef POLICY_5
// Machine learning policy

#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#include <errno.h>
#include <arpa/inet.h>
#include <limits.h>
#include <math.h>
#include <linux/sockios.h>

#include <libxl.h>
static xentoollog_logger xc_logger;
static libxl_ctx* ctx;


#define NOS    20             // NOS: Number of States
#define NOA     3             // NOA: Number of actions
#define MTPFR   7000.0        // Maximum Tolerable Put Failed rate
#define MTPR    7000.0        // Maximum Tolerable Put Rate.

#define DIFF_THRESHOLD    100       // Difference of nod_mem and curr_target


float pfr_ranges[NOS+1];          // these are the ranges of the quantizied state space. Plus 1, to account for the zero 
float pfr_rvalues[NOS+2][NOA];    // These are the reward values for a given state. Plus 2, to account for the zero, and the "beyond"
float pfr_qtable[NOS+2][NOA];     // Table for calculated Q-Values. To account for the zero and the beyond.

struct target my_targets[MAX_DOMAINS];

uint8_t sock_init = 0;
int32_t drl_client_socket, drl_socket, drl_rmt_socket=-1, drl_rmt_connected=-1;

int drlskt_init(struct node_info *ninf, uint32_t node_no ) {
    int i, opt = 1, addrlen, max_clients=1, iof;
    struct sockaddr_in address;

    drl_client_socket = -1;

    if( (drl_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) {
        perror("drl_socket failed");
        exit(EXIT_FAILURE);
    }

    if( setsockopt(drl_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    bzero((char *) &address, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(46000); //htons( portno );

    if (bind(drl_socket, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("drl_socket bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(drl_socket, 10) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((iof = fcntl(drl_socket, F_GETFL, 0)) != -1)
        fcntl(drl_socket, F_SETFL, iof | O_NONBLOCK);

    return SUCC_MMSKT_INIT;
}

int32_t pyrl_skt_disconnect( struct node_info *ninf, uint32_t nid ) {
    close(drl_rmt_socket);
    drl_rmt_socket = -1;
    drl_rmt_connected = -1;

    return 1;
}

int32_t pyrl_skt_chk(struct node_info *ninf, uint32_t nid) {
    struct sockaddr_in serv_addr;
    int socket_opened = 0;

    if (drl_rmt_socket < 0 ) {
        printf("\tfunc %s, note: About to open socket\n", __func__);
        drl_rmt_socket = socket(AF_INET, SOCK_STREAM, 0);
        if (drl_rmt_socket < 0 ) {
            printf("\tfunc %s, error:Cannot go remote, drl_rmt_socket=%i\n", __func__, drl_rmt_socket);
            close(drl_rmt_socket);
            drl_rmt_socket = -1;
            drl_rmt_connected = -1;
        } else {
            printf("\tfunc %s, note: Socket opened succesfully\n", __func__);
            socket_opened = 1;
        }
    } else {
        printf("\tfunc %s, note: Socket already opened\n", __func__);
        socket_opened = 1;
    }

    if ( socket_opened ) {
        if ( drl_rmt_connected < 0 ) {
            printf("\tfunc %s, note: About to connect socket\n", __func__);
            serv_addr.sin_family = AF_INET;
            serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
            serv_addr.sin_port = htons(47000);

            drl_rmt_connected = connect(drl_rmt_socket,(struct sockaddr *) &serv_addr, sizeof(serv_addr));
            if (drl_rmt_connected < 0) {
                printf("\tfunc %s, note: ERROR connecting drl_rmt_socket=%i, drl_rmt_connected=%i, errno=%i\n",
                        __func__, drl_rmt_socket, drl_rmt_connected, errno);
                close( drl_rmt_socket );
                drl_rmt_socket = -1;
                drl_rmt_connected = -1;
                return -1;
            } else {
                printf("\tfunc %s, note: Socket connect success\n", __func__);
                return 1;
            }
        } else {
            printf("\tfunc %s, note: Socket connected\n", __func__);
            return 1;
        }
    }
    return 1;
}

int32_t drlskt_send( struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t this_node_id, struct node_info *ninf, uint32_t node_no ) {
    int32_t n, i, skt_cool=0;

    //memset(&curr_statcmd, 0, sizeof(struct mm_stat_rmt));

    // open the socket in the port of the py-rl application
    skt_cool = pyrl_skt_chk(ninf, this_node_id);
    if (skt_cool == -1) {
        printf("Cannot send data to pyrl, will abort transmission");
        return -1;
    }

    printf("About to write to drl_rmt_socket\n");
    n = write( drl_rmt_socket, opcnt, sizeof(struct tmop_cnt) );
    printf("Bytes written n=%i\n", n);

    skt_cool = pyrl_skt_disconnect(ninf, this_node_id);

    return n;
}

int32_t drlskt_rcv(struct pyrlact *allacts) {
    int32_t max_sd, activity=-1, new_socket=-1, valread=0, i=0;
    socklen_t addrlen;
    struct sockaddr_in address;

    fd_set readfds;
    struct timeval tv;

    addrlen = sizeof(address); 
    memset(&address, 0, sizeof(struct sockaddr_in));
    FD_ZERO(&readfds);
    FD_SET(drl_socket, &readfds);
    max_sd = drl_socket;

    tv.tv_sec = MMSKT_SELECT_TIMEOUT_SEC;
    tv.tv_usec = MMSKT_SELECT_TIMEOUT_USEC;

    printf("Will wait for new_socket on the drl_socket\n");
    while (new_socket < 0 && i < MSKT_CONNECTION_ACCEPT_ATTEMPTS) {
        new_socket = accept(drl_socket, (struct sockaddr *) &address, &addrlen);
        if (new_socket < 0) {
            //usleep(600000);
            usleep(10000);
            i = i + 1;
        } else {
            break;
        }
    }

    if ( i == MSKT_CONNECTION_ACCEPT_ATTEMPTS ) {
        printf("Too many failed attempts to accept connection");
        return -1;
    }
    i = 0;
    
    printf("Activity detected on drl_socket, will read from buffer\n");
    bzero(allacts, MSKT_PYRLACT_SIZE);
    valread = read( new_socket , (void *)(allacts), MSKT_PYRLACT_SIZE);
    printf("Attempted at valread=%i", valread);
    for (i=0; i < MAX_DOMAINS; i = i+1) {
        printf("allacts->vm_acts[%i], .dom_id=%i, .bmem=%lu, .tmem=%lu\n", i, allacts->vm_acts[i].dom_id, allacts->vm_acts[i].bmem, allacts->vm_acts[i].tmem);
    }

    close(new_socket);
    return valread;
}

uint32_t memmag(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt,
                        struct pernodechart *nodecharts, struct mm_hyp_cmd *rep,
                        uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {

    int32_t i, j, diff, rc=0, send_anyway=0;
    struct pyrlact allacts;

    // 0. Necessary to do it in a separate socket, because it has to be synchronous
    if (sock_init == 0) {
        printf("Initializing socket in POLICY_5\n");
        drlskt_init(ninf, node_no);
        libxl_ctx_alloc(&ctx, LIBXL_VERSION, 0, &xc_logger);
        sock_init = 1;
    }
    // 0. Check if balloon driver was able to reach the previous command, assuming target_mem is not -1
    for (i=0; i < MAX_DOMAINS; i++) {
        for (j=0; j < MAX_DOMAINS; j++) {
            if ((my_targets[j].dom_id == opcnt->dominfo[i].dom_id) && (opcnt->dominfo[i].dom_id > 0)) {

                if (my_targets[j].curr_target > opcnt->dominfo[i].nod_mem*4) {
                    diff = my_targets[j].curr_target - opcnt->dominfo[i].nod_mem*4;
                } else {
                    diff = opcnt->dominfo[i].nod_mem*4 - my_targets[j].curr_target;
                }

                if ( diff > DIFF_THRESHOLD ) {
                    my_targets[j].allow_send = 0;
                    my_targets[j].time_elapsed_since_last_send = my_targets[j].time_elapsed_since_last_send + 1;
                    //printf("TARGET FOR DOMAIN %u (target=%li) IS NOT FULFILLED BY ALLOCATION (mem*4=%lu). WAIT NEXT SAMPLE TIME\n", my_targets[j].dom_id, 
                    //                                                                                                                my_targets[j].curr_target, 
                    //                                                                                                                opcnt->dominfo[i].nod_mem*4);
                } else {
                    my_targets[j].allow_send = 1;
                    my_targets[j].time_elapsed_since_last_send = 0;
                }
            }

            if ( my_targets[j].time_elapsed_since_last_send >= MAXIMUM_TIMESTEPS_ELAPSED ) {
                send_anyway = 1;
                printf("\tCommand will be sent anyway\n");
            }
        }
    }


    for (j=0; j < MAX_DOMAINS; j++) {
        if (my_targets[j].dom_id > 0 && my_targets[j].allow_send == 0 && send_anyway == 0 ) {
            //printf("TARGET FOR DOMAIN %u (target=%li) IS NOT FULFILLED BY ALLOCATION (mem=%lu). WAIT NEXT SAMPLE TIME\n", my_targets[j].dom_id, my_target[j].curr_target);
            rc = 0;
            return rc;
        }
    }


    // 1. Send stats to the py-rl agent
    if (nodecharts->domcnt > 0) {
        printf("opcnt->curr_ts=%lu\n", opcnt->curr_ts);
        //for (i=0; i < nodecharts->domcnt; i=i+1) {
            //printf("\topcnt->dominfo[%i], .dom_id=%i, .nod_mem=%lu, .nod_puts_tot=%lu\n", i, opcnt->dominfo[i].dom_id, 
            //                                                                           opcnt->dominfo[i].nod_mem, opcnt->dominfo[i].nod_puts_tot);
        //}
        rc = drlskt_send( opcnt, rmopcnt, this_node_id, ninf, node_no );
    } else {
        printf("no domains, will not send data to pyrl");
        return 1;
    }

    // 2. Receive commands from the py-rl agent, and block here until the command has been received
    rc = drlskt_rcv(&allacts);
    if (rc == -1) {
        printf("Command reception has failed. Will fail return");
        return rc;
    } else if (rc < MSKT_PYRLACT_SIZE) {
        printf("Command reception is incomplete. Received %i bytes, but expecting %lu bytes", rc, MSKT_PYRLACT_SIZE );
        return rc;
    }

    // 3. Send the data to the hypervisor 
    rep->cmd = MM_RSVPAG;
    for (i=0; i < nodecharts->domcnt; i++) {
        rep->m.pg_mng[i].cmd = MM_RSVPAG;
        rep->m.pg_mng[i].dom_id = allacts.vm_acts[i].dom_id;
        rep->m.pg_mng[i].mm_target = allacts.vm_acts[i].tmem;
        rep->m.pg_mng[i].balloon_target = allacts.vm_acts[i].bmem;
        //if (rep->m.pg_mng[i].balloon_target > 250000 || rep->m.pg_mng[i].balloon_target < 110000) {
            //printf("rep->m.pg_mng[%i], .cmd=%i, .dom_id=%i, .mm_target=%lu, .balloon_target=%lu\n", i, rep->m.pg_mng[i].cmd, 
            //                               rep->m.pg_mng[i].dom_id, rep->m.pg_mng[i].mm_target, rep->m.pg_mng[i].balloon_target);
        //}
 
        if ( allacts.vm_acts[i].dom_id > 0 && ( allacts.vm_acts[i].bmem <= opcnt->dominfo[i].nod_mem*4 ) ) {  // associated to a negative action
            libxl_set_memory_target(ctx, allacts.vm_acts[i].dom_id, allacts.vm_acts[i].bmem, 0, 1);
 
            my_targets[i].dom_id = allacts.vm_acts[i].dom_id;
            my_targets[i].curr_target = allacts.vm_acts[i].bmem;
            my_targets[i].time_elapsed_since_last_send = 0;
            send_anyway = 0;
            printf("\tCommands got sent anway\n");
        }
    }

    for (i=0; i < nodecharts->domcnt; i++) {
        rep->m.pg_mng[i].cmd = MM_RSVPAG;
        rep->m.pg_mng[i].dom_id = allacts.vm_acts[i].dom_id;
        rep->m.pg_mng[i].mm_target = allacts.vm_acts[i].tmem;
        rep->m.pg_mng[i].balloon_target = allacts.vm_acts[i].bmem;
        //if (rep->m.pg_mng[i].balloon_target > 250000 || rep->m.pg_mng[i].balloon_target < 110000) {
            //printf("rep->m.pg_mng[%i], .cmd=%i, .dom_id=%i, .mm_target=%lu, .balloon_target=%lu\n", i, rep->m.pg_mng[i].cmd, 
            //                               rep->m.pg_mng[i].dom_id, rep->m.pg_mng[i].mm_target, rep->m.pg_mng[i].balloon_target);
        //}
 
        if ( allacts.vm_acts[i].dom_id > 0 && ( allacts.vm_acts[i].bmem > opcnt->dominfo[i].nod_mem*4 ) ) {  // associated to a negative action
            libxl_set_memory_target(ctx, allacts.vm_acts[i].dom_id, allacts.vm_acts[i].bmem, 0, 1);
 
            my_targets[i].dom_id = allacts.vm_acts[i].dom_id;
            my_targets[i].curr_target = allacts.vm_acts[i].bmem;
            my_targets[i].time_elapsed_since_last_send = 0;
        }
    }
    //rc = mm_pagealloc_cmd(rep, MM_RSVPAG, this_node_id, ninf, node_no);

    return rc;
}


#endif
