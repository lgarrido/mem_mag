#include "mm_strategies.h"



int memmag_v00(struct tmop_cnt *opcnt, struct mm_hyp_cmd *rep) {
    int j=0, i = 0, ind=0;
    float max_ptgtrt = 0;
    bool reg = false;
    float put_val=0, get_val=0, acum=0, fin_prop;
    uint8_t curr_more_trgt;
    uint64_t diff, avail_mem;


    avail_mem = opcnt->nod_totpag - XEN_PAGES - DOM0_PAGES;

    for (i=0; i < opcnt->nod_domcnt; i++) {	
	for (j=0; j < opcnt->nod_domcnt; j++ ) {
	    if (domcharts[j].dom_id == opcnt->dominfo[i].dom_id) {
		domcharts[j].putall[entries].x= opcnt->dominfo[i].curr_ts;
	    	domcharts[j].putall[entries].y= opcnt->dominfo[i].nod_puts_tot;

		domcharts[j].getall[entries].x= opcnt->dominfo[i].curr_ts;
	    	domcharts[j].getall[entries].y= opcnt->dominfo[i].nod_gets_tot;

		domcharts[j].nod_tmem[entries].x= opcnt->dominfo[i].curr_ts;
		domcharts[j].nod_tmem[entries].y= opcnt->dominfo[i].nod_tmem;

		reg = true;
	    }
	} 

	printf("\t\tdomcharts[%i].dom_id=%i, opcnt->dominfo[%i], .dom_id=%i, .nod_mem=%lu\n", 
			i, domcharts[j].dom_id,
			i, opcnt->dominfo[i].dom_id,
			opcnt->dominfo[i].nod_mem);
	printf("\t\t\t.nod_tmem%lu, .nod_tmem_rsv=%lu, reg=%i\n",
			opcnt->dominfo[i].nod_tmem,
			opcnt->dominfo[i].nod_tmem_rsv,
			reg);

	if (!reg) {
	    printf("\t\tdomain not registered\n");
	    for (j=0; j < opcnt->nod_domcnt; j++) {
		if (domcharts[j].dom_id == 0) {
		    domcharts[j].dom_id = opcnt->dominfo[i].dom_id;
		    domcharts[j].nod_mem = opcnt->dominfo[i].nod_mem;

		    domcharts[j].putall[entries].x= opcnt->dominfo[i].curr_ts;
		    domcharts[j].putall[entries].y= opcnt->dominfo[i].nod_puts_tot;
		
		    domcharts[j].getall[entries].x= opcnt->dominfo[i].curr_ts;
		    domcharts[j].getall[entries].y= opcnt->dominfo[i].nod_gets_tot;

		    domcharts[j].nod_tmem[entries].x= opcnt->dominfo[i].curr_ts;
		    domcharts[j].nod_tmem[entries].y= opcnt->dominfo[i].nod_tmem;

		    domcharts[j].ptgtrt = 0;
		    break;
		}
	    }
    	}
	reg=false;

	avail_mem -= opcnt->dominfo[i].nod_tmem;
    }

    printf("\t\ttotal pages=%lu, free pages=%lu, tmem freeable pages=%lu, avail_mem=%lu\n",
			opcnt->nod_totpag,
			opcnt->nod_free_pages, 
			opcnt->nod_tmem_freeable_pages,
			avail_mem);


    if (entries == (WINDOW_SIZE - 1) ) {
    /* 
	The policy in this case is as follows.

	1. Look for the ratio of puts and gets individual to each domain on a per timestamp basis, 
	   for a WINDOW_SIZE.
	   This is to determine if a domain requires more tmem pages due to high demand.
	2. Compare the ratios of each domain. 
	3. Compare the amount of tmem each one already posseses. 
	4. Depending on certain values determine:
	    3.1. If more tmem pages will increase the number of reserved pages to a domain
	    3.2. If more physical memory will be allocated to a domain.
	5. Consider the amount of (t)mem pages it already has, the amount of memory in the system,
	   the amount of free memory, to determine how much (t) mem pages will be given to the domain.

	Very simple and senseless policy. 
	For now focus on 3.2, and generate a hypercall to give more memory to the domain. 
    */

	
	// 1. Accumulate the put-to-get ratio of every domain
	check_command = 1;
	for (i=0; i < opcnt->nod_domcnt; i++) {
	    for (j=0; j < WINDOW_SIZE; j++) {
		
		if (domcharts[i].putall[j].y > 0 || domcharts[i].getall[j].y > 0) {
		    put_val = (float) domcharts[i].putall[j].y;
		    get_val = (float) domcharts[i].getall[j].y;

		    //printf("\t\tadding %2.4f\n", (put_val / (get_val + put_val)) );
		    domcharts[i].ptgtrt = domcharts[i].ptgtrt + (put_val / (put_val + get_val));
		}

		/*printf("\t\tdomain=%i, domcharts[%i].putall[%i].y=%lu, domcharts[%i].getall[%i].y=%lu, domcharts[%i].ptgtrt=%2.4f\n", 
			domcharts[i].dom_id, 
			i, j, domcharts[i].putall[j].y, 
			i, j, domcharts[i].getall[j].y,
			i, domcharts[i].ptgtrt );*/
	    }
	    domcharts[i].ptgtrt = domcharts[i].ptgtrt / WINDOW_SIZE;
	    acum = domcharts[i].ptgtrt + acum;

	    domcharts[i].tmemrt = 1.0 - ((float)opcnt->dominfo[i].nod_tmem)/((float)avail_mem);
	}
	
	// 2. Average the ratio across the window_size, 
	// 3. find the maximum put-to-get ratio. A way to estimate future demand
	//printf("\t\tentries=%i, opcnt->nod_domcnt=%i\n", entries, opcnt->nod_domcnt);
	for (j=0; j < opcnt->nod_domcnt; j++) {
	    domcharts[j].ptgtrt = domcharts[j].ptgtrt / acum;
	    if (max_ptgtrt < domcharts[j].ptgtrt) {
		max_ptgtrt = domcharts[j].ptgtrt;
		ind = j; 
	    }

	    // 4. Determine the amount of pages to reserve for each domain
	    fin_prop = ( (float)(domcharts[j].ptgtrt + domcharts[j].tmemrt))/2.0;
	    domcharts[j].page_target = (uint64_t)(((float)opcnt->nod_free_pages) * fin_prop);

	    // 5. Compare the amount of pages that should be reserved to the ones
	    // the domain already has, and determine if a hypercall is necessary.
	    domcharts[j].page_current = opcnt->dominfo[j].nod_tmem;

	    curr_more_trgt = domcharts[j].page_current > domcharts[j].page_target;
	    diff = (curr_more_trgt) ? domcharts[j].page_current - domcharts[j].page_target : 
					domcharts[j].page_target - domcharts[j].page_current;
	    //if ( diff > PAGE_THRESHOLD )  {
		if (curr_more_trgt)
		    rep[j].cmd = MM_RELPAG;
		else
		    rep[j].cmd = MM_RSVPAG;
		rep[j].dom_id = domcharts[j].dom_id;
		rep[j].page_target = domcharts[j].page_target;
		rep[j].page_current = domcharts[j].page_current;

		if ( (20*(diff)/100) > 0)
		    rep[j].imm_alloc = 20*diff/100;
		else
		    rep[j].imm_alloc = diff;

		//sprintf(rep[j].buf, "%s=%i,%s=%lu,%s=%lu\0", 
		//		  "domain_id", domcharts[j].dom_id, 
		//		  "page_target", domcharts[j].page_target,
		//		  "page_current", domcharts[j].page_current);
	    //} else {
		//rep[j].cmd = MM_VOID;	    
	    //}
	
	    printf("\t\tentries, rep[%i].cmd=%i, rep[%i].dom_id=%i, rep[%i].page_current=%lu, rep[%i].page_target=%lu\n", 
			j, rep[j].cmd,
			j, rep[j].dom_id,
			j, rep[j].page_current,
			j, rep[j].page_target);

	    printf("\t\tdomcharts[%i].ptgtrt=%1.5f, domcharts[%i].tmemrt=%1.5f, fin_prop=%1.5f, rep[%i].imm_alloc=%i, diff=%lu\n",
			j, domcharts[j].ptgtrt,
			j, domcharts[j].tmemrt, fin_prop,
			j, rep[j].imm_alloc, 
			diff);
	}
	printf("\n");

	entries = 0;
    }

    // I have to generate a call to the tmem_frontswap_store function with a page ofset, a page structure.
}
