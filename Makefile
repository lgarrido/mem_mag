CC=gcc

SRC= main.c mm_ctrl.c node_info.c tmem_mod_if.c mm_comm.c mm_policy.c mm_sbtmem_policy.c mm_deeprl.c
OBJ= main.o mm_ctrl.o node_info.o tmem_mod_if.o mm_comm.o mm_policy.o mm_sbtmem_policy.o mm_deeprl.o
HEADER=mm_ctrl.h node_info.h tmem_mod_if.h mm_comm.h mm_policy.h
OUT=mem_mag

$(OUT): $(SRC) $(OBJ)
	$(CC) $(OBJ) -lxlutil -lxenlight -lm -o $(OUT)

main.o: $(HEADER) $(SRC)
	$(CC) -g -c main.c

tmem_mod_if.o: tmem_mod_if.c tmem_mod_if.h node_info.o
	$(CC) -g -c tmem_mod_if.c

mm_ctrl.o: mm_ctrl.h mm_ctrl.c tmem_mod_if.o
	$(CC) -g -c mm_ctrl.c

mm_comm.o: mm_comm.c mm_comm.h node_info.o
	$(CC) -g -c mm_comm.c 

node_info.o: node_info.c node_info.h
	$(CC) -g -c node_info.c

mm_sbtmem_policy.o: mm_sbtmem_policy.c mm_policy.h
	$(CC) -g -c mm_sbtmem_policy.c

mm_deeprl.o: mm_deeprl.c mm_policy.h
	$(CC) -g -c mm_deeprl.c

mm_policy.o: mm_policy.c mm_policy.h
	$(CC) -g -c mm_policy.c

clean: 
	rm *.o
	rm $(OUT)
