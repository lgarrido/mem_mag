#include <sys/socket.h>
#include <linux/netlink.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "mm_ctrl.h"
#include "tmem_mod_if.h"

#define MSG_HEADER_SIZE		sizeof(struct msghdr)

#ifdef TMIF_NETLINK_SOCKETS
int tmsock_fd;

int tmif_init() {
    uint32_t flag;
    struct sockaddr_nl src_addr;

    tmsock_fd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if (tmsock_fd < 0) {
	printf("func %s, error: tmsock_fd < 0, tmsock_fd=%i\n", 
			__func__, tmsock_fd);
	return E_TMSOCK_CANNOT_OPEN;
    }

    if ( (flag = fcntl(tmsock_fd, F_GETFL, NULL)) < 0) {
	perror("tmsock_fd fcntl:");
	return E_TMSOCK_RDFLAG;
    }

    flag |= O_NONBLOCK;
    if ( fcntl(tmsock_fd, F_SETFL, flag) < 0 ) {
	perror("tmsock_fd fcntl:");
	return E_TMSOCK_WRFLAG;
    }

    memset(&src_addr, 0, sizeof(struct sockaddr_nl));
    src_addr.nl_family = AF_NETLINK;
    src_addr.nl_pid = getpid();		/* self pid */
    src_addr.nl_groups = 0;		/* not in mcast groups */
 
    bind(tmsock_fd, (struct sockaddr*)&src_addr, sizeof(struct sockaddr_nl) );

    #ifdef TMIF_DEBUG
    printf("returning from %s, tmsock_fd=%i\n", __func__, tmsock_fd);
    #endif
    return SUCC_TMEM_MOD_INIT;
}

int tmif_send(struct mm_hyp_cmd *mmcmd) {
    int rc;
    struct sockaddr_nl dst_addr;
    struct nlmsghdr *nlh = NULL;
    struct iovec iov;
    struct msghdr msg;
    
    #ifdef TMIF_DEBUG
    printf("in %s, mmcmd=%lu, mmcmd->cmd=%i, tmsock_fd=%i\n", __func__, 
		(long unsigned int)mmcmd, mmcmd->cmd, tmsock_fd);
    #endif

    memset(&dst_addr, 0, sizeof(struct sockaddr_nl) );
    dst_addr.nl_family = AF_NETLINK;
    dst_addr.nl_pid = 0;	// For Linux kernel
    dst_addr.nl_groups = 0; 	// unicast

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD));
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;

    iov.iov_base = (void*)nlh;
    iov.iov_len = nlh->nlmsg_len;
    memset(&msg, 0, sizeof(struct msghdr));
    msg.msg_name = (void*)&dst_addr;
    msg.msg_namelen = sizeof(struct sockaddr_nl);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = NULL; 
    msg.msg_controllen = 0;

    memcpy(NLMSG_DATA(nlh), mmcmd, sizeof(struct mm_hyp_cmd));
    rc = sendmsg(tmsock_fd, &msg, 0);
    #ifdef TMIF_DEBUG
    printf("rc=%i, NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD)=%lu, no buffer space available\n", rc, NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD));
    #endif
    if (rc==-1) {
	printf("errno=%i\n", errno);
    }
    if (rc < NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD)) {
	rc = TMIF_WRITE_FAIL;
	printf("Write to tmsock_fd failed\n");
    } else {
	rc = TMIF_WRITE_SUCC;
	#ifdef TMIF_DEBUG
	printf("Write to tmsock_fd success\n");
	#endif
    }
    free(nlh);

    return rc;
}

int32_t tmif_rcv_stats(struct tmop_cnt *opcnt) {
    int32_t rcvd=-1;
    uint32_t i=0;
    struct sockaddr_nl dst_addr;
    struct nlmsghdr *nlh = NULL;
    struct iovec iov;
    struct msghdr msg;

    // Need to comment out this line, otherwise the initial data in opcnt gets erased.
    //memset(opcnt, 0, sizeof(struct tmop_cnt) );

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD_STATS));

    memset(&dst_addr, 0, sizeof(struct sockaddr_nl) );
    dst_addr.nl_family = AF_NETLINK;
    dst_addr.nl_pid = 0;	// For Linux kernel
    dst_addr.nl_groups = 0; 	// unicast

    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD_STATS));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD_STATS);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;

    iov.iov_base = (void*)nlh;
    iov.iov_len = nlh->nlmsg_len;
    memset(&msg, 0, sizeof(struct msghdr));
    msg.msg_name = (void*)&dst_addr;
    msg.msg_namelen = sizeof(struct sockaddr_nl);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    //#ifdef MM_TMIF_RCV_STATS_DEBUG
    //printf("func %s, note: opcnt->domcnt=%i, nlh=%lu\n", __func__, opcnt->domcnt, (long unsigned int)nlh);
    //#endif
    rcvd = recvmsg(tmsock_fd, &msg, 0);
    if ( rcvd > 0 && (rcvd < TMIF_RCVD_EXPECTED /* sizeof(struct tmop_cnt) + sizeof(struct msghdr) */) && i < 10) {
        #ifdef MM_TMIF_RCV_STATS_DEBUG
	printf("func %s, note: waiting to receive, i=%i, rcvd=%i\n", 
			__func__, i, rcvd);
	#endif
	i++;
    }


    #ifdef MM_TMIF_RCV_STATS_DEBUG
    if (rcvd > 0 && rcvd <= TMIF_RCVD_EXPECTED)
    printf("func %s, note: rcv done, rcvd=%i\n", 
		__func__, rcvd);
    #endif

    if (rcvd > -1) {
        memcpy(opcnt, (struct tmop_cnt *)NLMSG_DATA(nlh), sizeof(struct tmop_cnt) );
    }

    #ifdef MM_TMIF_RCV_STATS_DEBUG
    if (rcvd > 0 && rcvd <= TMIF_RCVD_EXPECTED) {
	printf("func %s, note: amount of data received, rcvd=%i, i=%i, opcnt->domcnt=%i, MSG_HEADER_SIZE=%lu\n", 
			__func__, rcvd, i, opcnt->domcnt, MSG_HEADER_SIZE);

	for (i=0; i < opcnt->domcnt; i++) {
	    printf("func %s, note: opcnt->dominfo[%i].region_id=%i, .dom_id=%i, .nod_mem=%lu, .nod_tmem=%lu\n", 
			__func__, i, opcnt->dominfo[i].region_id, 
			             opcnt->dominfo[i].dom_id, 
                                     opcnt->dominfo[i].nod_mem,
                                     opcnt->dominfo[i].nod_tmem);
	}
    }
    #endif


    #ifdef MM_TMIF_RCV_STATS_DEBUG
    if (rcvd > 0 && rcvd <= TMIF_RCVD_EXPECTED) {
        printf("func %s, note: received message payload: opcnt->domcnt=%i, rcvd=%i, opcnt->nod_free_pages=%lu\n", 	
		 __func__, opcnt->domcnt, rcvd, opcnt->free_pages);

        printf("func %s, note: about to return from %s\n", 
		__func__, __func__);
    }
    #endif

    free(nlh);
    //return TMIF_READ_SUCC;
    return rcvd;
}

int tmif_rcv_mcap(struct mm_hyp_cmd *tmcmd, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                    int64_t * entries, uint32_t this_region_id, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf) {
    int32_t rcvd=-1, rc=0;
    uint32_t i=0;
    struct sockaddr_nl dst_addr;
    struct nlmsghdr *nlh = NULL;
    struct iovec iov;
    struct msghdr msg;

    memset(tmcmd, 0, sizeof(struct mm_hyp_cmd) );

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD));

    memset(&dst_addr, 0, sizeof(struct sockaddr_nl) );
    dst_addr.nl_family = AF_NETLINK;
    dst_addr.nl_pid = 0;	// For Linux kernel
    dst_addr.nl_groups = 0; 	// unicast

    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PAYLOAD_HYP_CMD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;

    iov.iov_base = (void*)nlh;
    iov.iov_len = nlh->nlmsg_len;
    memset(&msg, 0, sizeof(struct msghdr));
    msg.msg_name = (void*)&dst_addr;
    msg.msg_namelen = sizeof(struct sockaddr_nl);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    #ifdef TMIF_DEBUG
    printf("in %s, tmcmd->m.pglst_mng.len=%lu, nlh=%lu\n", __func__, 
		tmcmd->m.pglst_mng.len, 
		(long unsigned int)nlh );
    #endif
tmif_rcv_mcap_rcvmsg_again:
    rcvd = recvmsg(tmsock_fd, &msg, 0);

    if (rcvd == 1192) {    // Just received stats. Discard them, and see what happens
        //#ifdef TMIF_DEBUG
        printf("func %s, note: received amount of data corresponding to stats, rcvd=%i\n", __func__, rcvd);
        //#endif
        memcpy(opcnt, (struct tmop_cnt *)NLMSG_DATA(nlh), sizeof(struct tmop_cnt) );
        //#ifdef TMIF_DEBUG
        printf("func %s, note: checking if stats are correct, opcnt->curr_ts=%lu, opcnt->region_id=%u, opcnt->domcnt=%i\n", 
                          __func__, opcnt->curr_ts, opcnt->region_id, opcnt->domcnt );
        //#endif
        rc = stats_rcv(opcnt, rmopcnt, nodecharts, entries, this_region_id, 1, node_no, this_node_id, ninf);

        goto tmif_rcv_mcap_rcvmsg_again;
    } else if (rcvd != 2104) {
        //#ifdef TMIF_DEBUG
        printf("func %s, note: wrong amount of data received, rcvd=%i, not 2104\n", __func__,rcvd );
        //#endif
        goto tmif_rcv_mcap_ret;
    }

    //#ifdef TMIF_DEBUG
    printf("func %s, note: rcv done, rcvd=%i, msg.msg_flags=%i\n", __func__, rcvd, msg.msg_flags);
    //#endif
    memcpy(tmcmd, (struct mm_hyp_cmd *)NLMSG_DATA(nlh), sizeof(struct mm_hyp_cmd) );
    //#ifdef TMIF_DEBUG
    printf("after memcpy\n");
    printf("Received message payload: tmcmd->cmd=%u, tmcmd->m.pglst_mng.len=%lu, rcvd=%i\n", 	
		 tmcmd->cmd, tmcmd->m.pglst_mng.len, rcvd);
    //printf("about to return from %s\n", __func__);
    //#endif

tmif_rcv_mcap_ret:
    free(nlh);
    return TMIF_READ_SUCC;
}


int tmif_wr_test() {
    int rc;
    struct mm_hyp_cmd mmcmd;
    struct tmop_cnt opcnt;

    memset(&mmcmd, 0, sizeof(struct mm_hyp_cmd) );
    #ifdef TMIF_DEBUG
    printf("in %s\n", __func__);
    #endif
    mmcmd.cmd = MM_SETUP_HNINF;
    rc = tmif_send(&mmcmd);

    mmcmd.cmd = MM_SETUP_TFAL;
    rc = tmif_send(&mmcmd);

    mmcmd.cmd = MM_SEND_STATS;
    rc = tmif_send(&mmcmd);

    rc = tmif_rcv_stats(&opcnt);


    return 1;
}

int tmif_close() {
    close(tmsock_fd);
}

#endif


#ifdef TMIF_PROCFS
int tmem_module_open(char * procfs_filename) {
   int fd=-1, i=0;

    #ifdef TMEM_MOD_IF_DEBUG
    printf("%s, ", __func__);
    #endif
    while (fd < 0 && i < 10) {
	#ifdef TMEM_MOD_IF_DEBUG
	printf("\tfd=%i\n", fd);
	#endif
	fd = open(procfs_filename, O_RDWR);
	i++;
	#ifdef TMEM_MOD_IF_DEBUG
	printf("\ti=%i\n",i);
	#endif
    }
    #ifdef TMEM_MOD_IF_DEBUG
    printf("fd=%i\n", fd);
    #endif
    if (fd < 0 && i == 10) {
	printf("No entry in procfs, maybe tmem.ko not loaded or not sudo\n");
        return E_PFS_NOTOPEN;
    }

    return fd;
}

int tmem_module_close(int fd) {
   int rc =0;   

   if (fd > 1) {
	rc = close(fd);
   }

   if (rc == 0)
	return TMEM_MOD_CLOSE_SUCC;

   return rc;
}

int repair_module_if(int fd) {
    tmem_module_close(fd);
    sleep(1);
    fd = tmem_module_open("/proc/tmem_stats");
    return fd;
}

int tmem_module_write(struct mm_hyp_cmd *rep, int fd) {
    int n=0, i=0/*, j=0*/;

retry_write:
    #ifdef TMEM_MOD_IF_DEBUG
    printf("about to write, %s, fd=%i\n", __func__, fd);
    #endif
    while (n<=0 && i < 10) {
        n = write(fd, (void *)rep, sizeof(struct mm_hyp_cmd) );
	i++;
    }
    if (n <= 0) {
	#ifdef TMEM_MOD_IF_DEBUG
	printf("\tCould not issue command. Something is wrong somewhere, n=%i, i=%i, errno=%i.\n", 
			n, i, errno);
	#endif
	/*if (j < 10)
	    fd = repair_module_if(fd);
	else
	    goto return_error;
	j++;
	i=0; 
	n=0;
	goto retry_write;
return_error:*/
	return TMEM_MOD_WRITE_FAIL;
    }
    #ifdef TMEM_MOD_IF_DEBUG
    printf("write succesful, n=%i, i=%i\n", n, i);
    #endif
    return TMEM_MOD_WRITE_SUCC;
}
#endif
