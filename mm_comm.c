#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <stdint.h>
#include <unistd.h>

#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <errno.h>
#include <arpa/inet.h> 
#include <limits.h>
#include <math.h>
#include <linux/sockios.h>

#include "mm_comm.h"
#include "mm_ctrl.h"

int32_t client_socket[MAX_CLIENTS], master_socket;
struct sockaddr_in address;

struct sent_mm_rmtcmd rmtcmd_waiting;



int32_t pglst_rmt_clear(struct mm_pglst_rmt * mmplcmd) {
    uint32_t i = 0;

    mmplcmd->src_region = 0;
    mmplcmd->dst_region = 0;
    mmplcmd->trn_id = 0;
    mmplcmd->cmd = 0;
    mmplcmd->len = 0;

    mmplcmd->src_group = 0;
    mmplcmd->dst_group = 0;

    for (i=0; i < PG_BATCH_SIZE; i++) {
	mmplcmd->addr_blks[i].base = 0;
	mmplcmd->addr_blks[i].length = 0;
    }

    return 1;
}

int32_t client_socket_nid(int sd, uint32_t node_no, struct node_info *ninf) {
    int i, ind=-1;

    for (i=0; i < node_no; i++) {
	if (sd == ninf[i].sd ) {
	    ind = i;
	    break;
	}
    }

    return ind;
}

uint32_t region_nid(uint32_t region_id, uint32_t node_no, struct node_info *ninf) {
    uint32_t nid, i;

    for (i=0; i < node_no; i++) {
	if (ninf[i].region == region_id) {
	    nid = i;
	    break;
	}
    }

    return nid;
}

uint64_t char2dig(char * tb, int len) {
    int i; 
    unsigned int tmpnt;
    uint64_t rc=0;
    double factor=0;

    if (len < sizeof(uint64_t) ) {
	for (i=0; i < len; i++) {
	    tmpnt = (unsigned int)tb[i];
	    factor = pow(256, i);
	    rc += ((uint64_t)tmpnt)*((uint64_t)factor);
	}
    } else if (len == sizeof(uint64_t) ) {
	for (i=0; i < sizeof(uint32_t); i++) {
	    tmpnt = (unsigned int)tb[i];
	    if (tmpnt != 0)
		factor = pow(256, i + sizeof(uint32_t) );
	    rc += ((uint64_t)tmpnt)*((uint64_t)factor);
	}

	for (i=0; i < sizeof(uint32_t); i++) {
	    tmpnt = (unsigned int)tb[i+sizeof(uint32_t)];
 	    if (tmpnt != 0)
		factor = pow(256, i);
	    rc += ((uint64_t)tmpnt)*((uint64_t)factor);
	}
	
    }

    return rc;
}

int32_t bind_nid (int sd, char * tb, int valread, struct node_info *ninf, uint32_t node_no ) {
    int32_t i, rc=-1, region=-1;
    char * tmpbf0;

    tmpbf0 = (char*)malloc(sizeof(unsigned short));
    memset(tmpbf0, 0, sizeof(unsigned short) );
    if (rc == -1 && valread > sizeof(unsigned short) ) {
	// assuming that the source and destination are properly read
	// this assumption might require some revision
	memcpy(tmpbf0, tb, sizeof(unsigned short) );
	region = (unsigned short)char2dig(tmpbf0, sizeof(unsigned short) );

	for (i=0; i < node_no; i++) {
	    if (ninf[i].region == region) {
		ninf[i].sd = sd;
		rc = i;
		break;
	    }
	}

    } 
    #ifdef MMSKT_BIND_NID_DEBUG
    else if (rc == -1) {
	printf("func %s, error: not possible to bind source of transaction, tx error.\n", 
		__func__);
    }
    #endif

    free(tmpbf0);
    #ifdef MMSKT_BIND_NID_DEBUG
    if (rc== -1) {
	printf("func %s, error: no valid node for current transaction\n", 
			__func__);
    }
    #endif

    return rc;
}

uint8_t get_commtype(char * tb, int32_t valread) {
    char * tmpbf0;
    uint32_t rc=0, offset=0, command=0;

    #ifdef MMSKT_GET_COMMTYPE_DEBUG
    if (valread < BYTESREAD_COMMTYPE) {
	printf("func %s, warning: Not enough bytes read to get the command type. Useless call, segfault to be\n", 
			__func__); 
    }
    #endif
    
    tmpbf0 = (char*)malloc(sizeof(uint32_t));
    memset(tmpbf0, 0, sizeof(uint32_t) );

    #ifdef MMSKT_GET_COMMTYPE_DEBUG
    printf("func %s, note: \n", __func__);
    #endif

    if (valread >= BYTESREAD_COMMTYPE) {
	offset = sizeof(uint32_t)*3;
	memcpy(tmpbf0, tb + offset, sizeof(uint32_t) );
	command = (uint32_t)char2dig(tmpbf0, sizeof(uint32_t) );

	#ifdef MMSKT_GET_COMMTYPE_DEBUG
	printf("\tCommand received = %u, ", command);
	#endif

	switch (command) {
	    case MM_TEST_SEND:
	    case MM_REQ_MCAP:
	    case MM_FN2M_REQ_MCAP:
	    case MM_FM2N_REQ_MCAP:
	    case MM_FN2N_REQ_MCAP:
	    case MM_RECLAIM_MCAP:
	    case MM_RELINQ_MCAP:
	    case MM_REQ_STATS:
		rc = MM_CMD_PLAIN;
		#ifdef MMSKT_GET_COMMTYPE_DEBUG
		printf("func %s, note: command type = MM_CMD_PLAIN\n", __func__);
		#endif
		break;
	    case MM_TEST_RESP:
	    case MM_RCV_MCAP:
	    case MM_RECLACK_MCAP:
	    case MM_RELACK_MCAP:
            case MM_DISPERS_MCAP:
		rc = MM_CMD_PGLST;
		#ifdef MMSKT_GET_COMMTYPE_DEBUG
		printf("func %s, note: command type = MM_CMD_PGLST\n", __func__);
		#endif
	    	break;
	    case MM_SND_STATS:
		rc = MM_STATS;
		#ifdef MMSKT_GET_COMMTYPE_DEBUG
		printf("func %s, note: command type = MM_STATS\n", __func__);
		#endif
	    	break;
	    default:
		rc = 0;
		#ifdef MMSKT_GET_COMMTYPE_DEBUG
		printf("func %s, note: command type undefined\n", __func__);
		#endif
		break;
	}
    } else {
	rc = 0;
    } 

    free(tmpbf0);

    return rc;
}

int32_t skt_chk(struct node_info *ninf, uint32_t nid) {
    struct sockaddr_in serv_addr;
    int socket_opened = 0;

    #ifdef MMSKT_SKTCHK_DEBUG
    printf("func %s, note: in skt_chk, ninf[%i].rem_socket=%i\n", 
			__func__, nid, ninf[nid].rem_socket);
    #endif
    // No attempt to open socket if the block is on the same node
    if (ninf[nid].self == 1) {
	return 0;
    }

    if ( ninf[nid].rem_socket < 0 ) {
	#ifdef MMSKT_SKTCHK_DEBUG
	printf("\tfunc %s, note: About to open socket\n", __func__);
	#endif
	ninf[nid].rem_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (ninf[nid].rem_socket < 0 ) {
	    #ifdef MMSKT_SKTCHK_DEBUG
	    printf("\tfunc %s, error:Cannot go remote, ninf[%i].rem_socket=%i\n", 
			__func__, nid, ninf[nid].rem_socket); 
	    #endif
	    close(ninf[nid].rem_socket);
	    ninf[nid].rem_socket = -1;
	    ninf[nid].connected = -1;
	    return -1;
	} else {
	    #ifdef MMSKT_SKTCHK_DEBUG
	    printf("\tfunc %s, note: Socket opened succesfully\n", __func__);
	    #endif
	    socket_opened = 1;
	}
    } else {
	#ifdef MMSKT_SKTCHK_DEBUG
	printf("\tfunc %s, note: Socket already opened\n", __func__);
	#endif
	socket_opened = 1;
    }

    if ( socket_opened ) {
	if (ninf[nid].connected < 0) { 
	    serv_addr.sin_family = AF_INET;
	    serv_addr.sin_addr.s_addr = ninf[nid].naddr.sin_addr.s_addr;
	    serv_addr.sin_port = ninf[nid].portno;

	    #ifdef MMSKT_SKTCHK_DEBUG
	    printf("\tfunc %s, note: About to connect socket\n", __func__);
	    #endif

	    ninf[nid].connected = connect(ninf[nid].rem_socket,(struct sockaddr *) &serv_addr, sizeof(serv_addr));
	    if (ninf[nid].connected < 0) {
		#ifdef MMSKT_SKTCHK_DEBUG
		printf("\tfunc %s, note: ERROR connecting ninf[%i].rem_socket=%i, ninf[%i].connected=%i, errno=%i\n", 
			__func__, nid, ninf[nid].rem_socket, nid, ninf[nid].connected, errno);
		#endif
		close(ninf[nid].rem_socket);
	   	ninf[nid].rem_socket = -1;
	    	ninf[nid].connected = -1;
		return -1;
	    }
	    #ifdef MMSKT_SKTCHK_DEBUG 
	    else {
		printf("\tfunc %s, note: Socket connect success, ninf[%i].region=%i\n", 
				__func__, nid, ninf[nid].region);
		return 1;
	    }
	    #endif
	} else if ( ninf[nid].connected > 0 && socket_opened) {
	    #ifdef MMSKT_SKTCHK_DEBUG
	    printf("\tfunc %s, note: Socket connected\n", __func__);
	     #endif
	    return 1;
	}
    }

    return 1;
}

int mmskt_init(struct node_info *ninf, uint32_t nid) {
    int i, opt = 1, addrlen, max_clients= MAX_CLIENTS, iof;
    struct sockaddr_in address;

    for (i = 0; i < max_clients; i++) {
        client_socket[i] = -1;
    }

    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) {
	perror("master_socket failed");
	exit(EXIT_FAILURE);
    }

    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ) {
	perror("setsockopt");
	exit(EXIT_FAILURE);
    }

    bzero((char *) &address, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = ninf[nid].portno; //htons( portno );

    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) {
	perror("bind failed");
	exit(EXIT_FAILURE);
    }

    if (listen(master_socket, 10) < 0) {
	perror("listen");
	exit(EXIT_FAILURE);
    }

    if ((iof = fcntl(master_socket, F_GETFL, 0)) != -1)
	fcntl(master_socket, F_SETFL, iof | O_NONBLOCK);

    rmtcmd_waiting.size = 0;
    rmtcmd_waiting.root = NULL;
    rmtcmd_waiting.last = NULL;

    return SUCC_MMSKT_INIT;
}

int32_t mmskt_send( struct mm_hyp_cmd *mmcmd, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t trn_id, 
		uint32_t lnid, struct node_info *ninf, uint32_t node_no ) {
    struct mm_rmtcmd curr_rmtcmd;
    struct mm_pglst_rmt curr_pglstcmd;
    struct mm_stat_rmt curr_statcmd; 
    uint32_t rnid, sktchk_attempts=0, i=0;
    int32_t n=0, skt_cool=0;
    uint64_t pages_ops = 0;
    
    // Get node id of the remote node within the ninf data structure
    rnid = region_nid(mmcmd->m.pglst_mng.region_id, node_no, ninf);
    
    //printf("func %s, note: in here, rnid=%i\n", 
		//__func__, rnid);
    // check that we are able to establish connection with the remote node
    skt_cool = skt_chk(ninf, rnid);
    while (skt_cool < 0 && sktchk_attempts < MM_SKTCHK_ATTEMPTS) {
	skt_cool = skt_chk(ninf, rnid);
	usleep(MSKT_SEND_WAITSKT_USEC);
	sktchk_attempts++;
    }

    if (sktchk_attempts == MM_SKTCHK_ATTEMPTS && skt_cool < 0) {
	#ifdef MMSKT_SEND_DEBUG
	printf("function %s, error: cannot connect to remote node, sktchk_attempts=%u, skt_cool=%i\n",
			__func__, sktchk_attempts, skt_cool);
	#endif
	return ERR_MMSKT_SEND_CANTCONNECT;
    }
    sktchk_attempts=0;


    // 
    switch (mmcmd->cmd) {
	case MM_TEST_SEND:
	case MM_REQ_MCAP:
	case MM_FN2M_REQ_MCAP:
	case MM_FM2N_REQ_MCAP:
	case MM_FN2N_REQ_MCAP:
	case MM_RECLAIM_MCAP:
	case MM_REQ_STATS:
	    curr_rmtcmd.src_region = ninf[lnid].region;
	    curr_rmtcmd.dst_region = mmcmd->m.pglst_mng.region_id;
	    curr_rmtcmd.trn_id = (rand() % (UINT_MAX) );
	    curr_rmtcmd.cmd = mmcmd->cmd;
	    #ifdef MMSKT_SEND_DEBUG
	    printf("func %s, note: curr_rmtcmd.cmd=%i\n", __func__, curr_rmtcmd.cmd);
	    #endif

	    curr_rmtcmd.len = mmcmd->m.pglst_mng.len;

retry_sktwrt_0:
	    #ifdef MMSKT_SEND_DEBUG
	    printf("func %s, note: attempting write\n", __func__);
	    #endif
	    n = write( ninf[rnid].rem_socket, &curr_rmtcmd, sizeof(struct mm_rmtcmd) );
	    #ifdef MMSKT_SEND_DEBUG
	    printf("func %s, note: bytes written n=%i sdagdsag\n", 
			__func__, n);
	    #endif
	    printf("func %s, note: about to reattempt\n", __func__);
	    if (n < 0) {
		#ifdef MMSKT_SEND_DEBUG
		printf("func %s, note: about to reattempt socket opening and write\n", __func__);
		#endif

		if (ninf[rnid].rem_socket > 0)
		    close(ninf[rnid].rem_socket);
		ninf[rnid].rem_socket = -1;
		ninf[rnid].connected = -1;

		#ifdef MMSKT_SEND_DEBUG
	   	printf("function %s, error: writing to socket, n=%i of sizeof(struct mm_rmtcmd)=%lu\n", 
				__func__, n, sizeof(struct mm_rmtcmd) );
		printf(" retry ");
		#endif

		skt_cool = skt_chk(ninf, rnid);
		while (skt_cool < 0 && sktchk_attempts < MM_SKTCHK_ATTEMPTS) {
		    skt_cool = skt_chk(ninf, rnid);
		    usleep(MSKT_SEND_WAITSKT_USEC);
		    sktchk_attempts++;
		}

		if (sktchk_attempts == MM_SKTCHK_ATTEMPTS && skt_cool < 0) {
		    #ifdef MMSKT_SEND_DEBUG
		    printf("function %s, error: cannot connect to remote node, sktchk_attempts=%u, skt_cool=%i\n",
			__func__, sktchk_attempts, skt_cool);
		    #endif
		    sktchk_attempts=0;
		    return ERR_MMSKT_SEND_CANTCONNECT;
		}

		goto retry_sktwrt_0;
	    } 
	    #ifdef MMSKT_SEND_DEBUG
	    else {
		printf("func %s, note: not entering on 'if', n=%i\n", __func__, n);
	    }
	    #endif
	
	    /*if ( n == sizeof(struct mm_rmtcmd) ) {
		if ( (!rmtcmd_waiting.last) && (rmtcmd_waiting.size <= 0) ) {
		    rmtcmd_waiting.last = (struct mm_rmtcmd_list*)malloc(sizeof(struct mm_rmtcmd_list));
		    if (!rmtcmd_waiting.root) {
			rmtcmd_waiting.root = rmtcmd_waiting.last;
		    }
		} else {
		    rmtcmd_waiting.last->next = (struct mm_rmtcmd_list*)malloc(sizeof(struct mm_rmtcmd_list) );
		    rmtcmd_waiting.last->next->prev = rmtcmd_waiting.last;
		    rmtcmd_waiting.last = rmtcmd_waiting.last->next;
		}

		if (rmtcmd_waiting.last) {
		    rmtcmd_waiting.last->rmtcmd.src_region = curr_rmtcmd.src_region;
		    rmtcmd_waiting.last->rmtcmd.dst_region = curr_rmtcmd.dst_region;
		    rmtcmd_waiting.last->rmtcmd.trn_id = curr_rmtcmd.trn_id;
		    rmtcmd_waiting.last->rmtcmd.cmd = curr_rmtcmd.cmd;
		    rmtcmd_waiting.last->rmtcmd.len = curr_rmtcmd.len;

		    rmtcmd_waiting.size++;
		} 
		#ifdef MMSKT_SEND_DEBUG
		else {
		// ltar: list of transactions awaiting response.
		    printf("func %s, error: could not allocate entry on the ltar\n", 
				__func__);
		}
		#endif
	    }*/
	   
	break;
	case MM_TEST_RESP:
	case MM_RCV_MCAP:
	case MM_RELINQ_MCAP:	// MM_RELINQ_MCAP is never sent remotely
	case MM_RECLACK_MCAP:   // MM_RECLACK_MCAP is sent instead of MM_RELINQ_MCAP
	case MM_RELACK_MCAP:
        case MM_DISPERS_MCAP:
	    curr_pglstcmd.src_region = ninf[lnid].region;
    	    curr_pglstcmd.dst_region = mmcmd->m.pglst_mng.region_id;
    	    curr_pglstcmd.trn_id = trn_id;
    	    curr_pglstcmd.cmd = mmcmd->cmd;

    	    curr_pglstcmd.src_group = mmcmd->m.pglst_mng.src_group;
    	    curr_pglstcmd.dst_group = mmcmd->m.pglst_mng.dst_group;
    	    curr_pglstcmd.len = mmcmd->m.pglst_mng.len;

	    for (i=0; i < mmcmd->m.pglst_mng.len; i++) {
		curr_pglstcmd.addr_blks[i].base = mmcmd->m.pglst_mng.addr_blks[i].base;
		curr_pglstcmd.addr_blks[i].length = mmcmd->m.pglst_mng.addr_blks[i].length;

                pages_ops = pages_ops + mmcmd->m.pglst_mng.addr_blks[i].length;
	    }

retry_sktwrt_1:
	    n = write( ninf[rnid].rem_socket, &curr_pglstcmd, sizeof(struct mm_pglst_rmt) );
	    if (n <= 0) {
		close(ninf[rnid].rem_socket);
		ninf[rnid].rem_socket = -1;
		ninf[rnid].connected = -1;

		#ifdef MMSKT_SEND_DEBUG
	   	printf("function %s, error: writing to socket, n=%i of sizeof(struct mm_rmtcmd)=%lu\n", 
				__func__, n, sizeof(struct mm_pglst_rmt) );
		printf(" retry ");
		#endif

		skt_cool = skt_chk(ninf, rnid);
		while (skt_cool < 0 && sktchk_attempts < MM_SKTCHK_ATTEMPTS) {
		    skt_cool = skt_chk(ninf, rnid);
		    usleep(MSKT_SEND_WAITSKT_USEC);
		    sktchk_attempts++;
		}

		if (sktchk_attempts == MM_SKTCHK_ATTEMPTS && skt_cool < 0) {
		    #ifdef MMSKT_SEND_DEBUG
		    printf("function %s, error: cannot connect to remote node, sktchk_attempts=%u, skt_cool=%i\n",
				__func__, sktchk_attempts, skt_cool);
		    #endif
		    sktchk_attempts=0;
		    return ERR_MMSKT_RCV_CANTCONNECT;
		}

		goto retry_sktwrt_1;
	    }   

	break;
	case MM_SND_STATS:

	curr_statcmd.src_region = ninf[lnid].region;
	curr_statcmd.dst_region = mmcmd->m.pglst_mng.region_id;
	curr_statcmd.trn_id = (rand() % (UINT_MAX) );
	curr_statcmd.cmd = mmcmd->cmd;

	//memcpy(&curr_statcmd.opcnt, opcnt, sizeof(struct tmop_cnt) );
        curr_statcmd.rmt_opcnt.curr_ts = opcnt->curr_ts;
        curr_statcmd.rmt_opcnt.prev_ts = opcnt->prev_ts;
        curr_statcmd.rmt_opcnt.free_pages = opcnt->free_pages;
        curr_statcmd.rmt_opcnt.tmem_freeable_pages = opcnt->tmem_freeable_pages;
        curr_statcmd.rmt_opcnt.domcnt = opcnt->domcnt;
        curr_statcmd.rmt_opcnt.min_request_pages = opcnt->min_request_pages;
        curr_statcmd.rmt_opcnt.region_id = opcnt->region_id;
        curr_statcmd.rmt_opcnt.sys_mem = opcnt->sys_mem;
        curr_statcmd.rmt_opcnt.tmempages = opcnt->tmempages;

        curr_statcmd.rmt_opcnt.total_accesses = opcnt->total_accesses;

        curr_statcmd.rmt_opcnt.total_list = opcnt->total_list;
        curr_statcmd.rmt_opcnt.local_list = opcnt->local_list;
        curr_statcmd.rmt_opcnt.remote_list = opcnt->remote_list;
        curr_statcmd.rmt_opcnt.given_list = opcnt->given_list;
        curr_statcmd.rmt_opcnt.used_list = opcnt->used_list;

        curr_statcmd.rmt_opcnt.total_puts = opcnt->total_puts;
        curr_statcmd.rmt_opcnt.total_putssucc = opcnt->total_putssucc;
        curr_statcmd.rmt_opcnt.total_gets = opcnt->total_gets;
        curr_statcmd.rmt_opcnt.total_getssucc = opcnt->total_getssucc;
        curr_statcmd.rmt_opcnt.total_flush = opcnt->total_flush;
        curr_statcmd.rmt_opcnt.total_flushsucc = opcnt->total_flushsucc;

        curr_statcmd.rmt_opcnt.local_puts = opcnt->local_puts;
        curr_statcmd.rmt_opcnt.local_gets = opcnt->local_gets;
        curr_statcmd.rmt_opcnt.local_flush = opcnt->local_flush;
        curr_statcmd.rmt_opcnt.local_accesses = opcnt->local_accesses;

        curr_statcmd.rmt_opcnt.rem_puts = opcnt->rem_puts;
        curr_statcmd.rmt_opcnt.rem_gets = opcnt->rem_gets;
        curr_statcmd.rmt_opcnt.rem_flush = opcnt->rem_flush;
        curr_statcmd.rmt_opcnt.rem_accesses = opcnt->rem_accesses;

        curr_statcmd.rmt_opcnt.total_tmem = opcnt->total_tmem;
        curr_statcmd.rmt_opcnt.local_tmem = opcnt->local_tmem;
        curr_statcmd.rmt_opcnt.remote_tmem = opcnt->remote_tmem;

        // Include the rmopcnt
        curr_statcmd.rmt_opcnt.req_mcap_in = rmopcnt->req_mcap_in;
        curr_statcmd.rmt_opcnt.req_mcap_inpagreq = rmopcnt->req_mcap_inpagreq;
        curr_statcmd.rmt_opcnt.rcv_mcap_out = rmopcnt->rcv_mcap_out;
        curr_statcmd.rmt_opcnt.rcv_mcap_outpagsent = rmopcnt->rcv_mcap_outpagsent; 

        curr_statcmd.rmt_opcnt.req_mcap_out = rmopcnt->req_mcap_out;
        curr_statcmd.rmt_opcnt.req_mcap_outpagreq = rmopcnt->req_mcap_outpagreq;
        curr_statcmd.rmt_opcnt.rcv_mcap_in = rmopcnt->rcv_mcap_in;
        curr_statcmd.rmt_opcnt.rcv_mcap_inpagrcvd = rmopcnt->rcv_mcap_inpagrcvd;

        curr_statcmd.rmt_opcnt.reclaim_mcap_in = rmopcnt->reclaim_mcap_in;
        curr_statcmd.rmt_opcnt.reclaim_mcap_inpagret = rmopcnt->reclaim_mcap_inpagret;
        curr_statcmd.rmt_opcnt.reclack_mcap_out = rmopcnt->reclack_mcap_out;
        curr_statcmd.rmt_opcnt.reclack_mcap_outpagret = rmopcnt->reclack_mcap_outpagret;

        curr_statcmd.rmt_opcnt.reclaim_mcap_out = rmopcnt->reclaim_mcap_out;
        curr_statcmd.rmt_opcnt.reclaim_mcap_outpagreq = rmopcnt->reclaim_mcap_outpagreq;
        curr_statcmd.rmt_opcnt.reclack_mcap_in = rmopcnt->reclack_mcap_in;
        curr_statcmd.rmt_opcnt.reclack_mcap_inpagret = rmopcnt->reclack_mcap_inpagret;

        curr_statcmd.rmt_opcnt.relack_mcap_in = rmopcnt->relack_mcap_in;
        curr_statcmd.rmt_opcnt.relack_mcap_inpagret = rmopcnt->relack_mcap_inpagret;
        curr_statcmd.rmt_opcnt.relack_mcap_out = rmopcnt->relack_mcap_out;
        curr_statcmd.rmt_opcnt.relack_mcap_outpagret = rmopcnt->relack_mcap_outpagret;

        curr_statcmd.rmt_opcnt.remote_pages_taken = rmopcnt->remote_pages_taken;
        curr_statcmd.rmt_opcnt.remote_pages_given = rmopcnt->remote_pages_given;
        curr_statcmd.rmt_opcnt.remote_pages_dispin = rmopcnt->remote_pages_dispin;
        curr_statcmd.rmt_opcnt.remote_pages_dispout = rmopcnt->remote_pages_dispout;

        // Include the amount of remote memory actually received
        // from remote nodes, and from which nodes.
        for (i = 0; i < MAX_RMTNODES; i++) {
            curr_statcmd.rmt_opcnt.nodinfo[i].region_id = rmopcnt->nodinfo[i].region_id;
            
            curr_statcmd.rmt_opcnt.nodinfo[i].req_mcap_in = rmopcnt->nodinfo[i].req_mcap_in;
            curr_statcmd.rmt_opcnt.nodinfo[i].req_mcap_inpagreq = rmopcnt->nodinfo[i].req_mcap_inpagreq;
            curr_statcmd.rmt_opcnt.nodinfo[i].rcv_mcap_out = rmopcnt->nodinfo[i].rcv_mcap_out;
            curr_statcmd.rmt_opcnt.nodinfo[i].rcv_mcap_outpagsent = rmopcnt->nodinfo[i].rcv_mcap_outpagsent;

            curr_statcmd.rmt_opcnt.nodinfo[i].req_mcap_out = rmopcnt->nodinfo[i].req_mcap_out;
            curr_statcmd.rmt_opcnt.nodinfo[i].req_mcap_outpagreq = rmopcnt->nodinfo[i].req_mcap_outpagreq;
            curr_statcmd.rmt_opcnt.nodinfo[i].rcv_mcap_in = rmopcnt->nodinfo[i].rcv_mcap_in;
            curr_statcmd.rmt_opcnt.nodinfo[i].rcv_mcap_inpagrcvd = rmopcnt->nodinfo[i].rcv_mcap_inpagrcvd;

            curr_statcmd.rmt_opcnt.nodinfo[i].reclaim_mcap_in = rmopcnt->nodinfo[i].reclaim_mcap_in;
            curr_statcmd.rmt_opcnt.nodinfo[i].reclaim_mcap_inpagret = rmopcnt->nodinfo[i].reclaim_mcap_inpagret;
            curr_statcmd.rmt_opcnt.nodinfo[i].reclack_mcap_out = rmopcnt->nodinfo[i].reclack_mcap_out;
            curr_statcmd.rmt_opcnt.nodinfo[i].reclack_mcap_outpagret = rmopcnt->nodinfo[i].reclack_mcap_outpagret;

            curr_statcmd.rmt_opcnt.nodinfo[i].reclaim_mcap_out = rmopcnt->nodinfo[i].reclaim_mcap_out;
            curr_statcmd.rmt_opcnt.nodinfo[i].reclaim_mcap_outpagreq =rmopcnt->nodinfo[i].reclaim_mcap_outpagreq;
            curr_statcmd.rmt_opcnt.nodinfo[i].reclack_mcap_in = rmopcnt->nodinfo[i].reclack_mcap_in;
            curr_statcmd.rmt_opcnt.nodinfo[i].reclack_mcap_inpagret = rmopcnt->nodinfo[i].reclack_mcap_inpagret;

            curr_statcmd.rmt_opcnt.nodinfo[i].relack_mcap_in = rmopcnt->nodinfo[i].relack_mcap_in;
            curr_statcmd.rmt_opcnt.nodinfo[i].relack_mcap_inpagret = rmopcnt->nodinfo[i].relack_mcap_inpagret;
            curr_statcmd.rmt_opcnt.nodinfo[i].relack_mcap_out = rmopcnt->nodinfo[i].relack_mcap_out;
            curr_statcmd.rmt_opcnt.nodinfo[i].relack_mcap_outpagret = rmopcnt->nodinfo[i].relack_mcap_outpagret;

            curr_statcmd.rmt_opcnt.nodinfo[i].remote_pages_taken = rmopcnt->nodinfo[i].remote_pages_taken;
            curr_statcmd.rmt_opcnt.nodinfo[i].remote_pages_given = rmopcnt->nodinfo[i].remote_pages_given;
            curr_statcmd.rmt_opcnt.nodinfo[i].remote_pages_dispin = rmopcnt->nodinfo[i].remote_pages_dispin;
            curr_statcmd.rmt_opcnt.nodinfo[i].remote_pages_dispout = rmopcnt->nodinfo[i].remote_pages_dispout;

            curr_statcmd.rmt_opcnt.nodinfo[i].reallatency2node = rmopcnt->nodinfo[i].reallatency2node;
            curr_statcmd.rmt_opcnt.nodinfo[i].estilatency2node = rmopcnt->nodinfo[i].estilatency2node; 
        }
retry_sktwrt_2:
	    n = write( ninf[rnid].rem_socket, &curr_statcmd, sizeof(struct mm_stat_rmt) );
	    if (n <= 0) {
		close(ninf[rnid].rem_socket);
		ninf[rnid].rem_socket = -1;
		ninf[rnid].connected = -1;

		#ifdef MMSKT_SEND_DEBUG
	   	printf("function %s, error: writing to socket, n=%i of sizeof(struct mm_rmtcmd)=%lu\n", 
				__func__, n, sizeof(struct tmop_cnt) );
		printf(" retry ");
		#endif

		skt_cool = skt_chk(ninf, rnid);
		while (skt_cool < 0 && sktchk_attempts < MM_SKTCHK_ATTEMPTS) {
		    skt_cool = skt_chk(ninf, rnid);
		    usleep(MSKT_SEND_WAITSKT_USEC);
		    sktchk_attempts++;
		}

		if (sktchk_attempts == MM_SKTCHK_ATTEMPTS && skt_cool < 0) {
		    #ifdef MMSKT_SEND_DEBUG
		    printf("function %s, error: cannot connect to remote node, sktchk_attempts=%u, skt_cool=%i\n",
				__func__, sktchk_attempts, skt_cool);
		    #endif
		    sktchk_attempts=0;
		    return ERR_MMSKT_RCV_CANTCONNECT;
		}

		goto retry_sktwrt_2;
	    }
	break;
	default:
	    printf("func %s, warning: command not recognized, mmcmd->cmd=%i\n", 
			__func__, mmcmd->cmd);
	break;
    }

    

    return n;
}


int32_t mmskt_info_copy(uint8_t *cmd_type, struct mm_rmtcmd *rmtcmd, 	
			  struct mm_pglst_rmt* pglst_cmdrmt, struct mm_stat_rmt* stats_rmt,
			  char *tmpbuf) {
    uint64_t i=0;
    #ifdef MMSKT_CMDPLAIN_TREAT_DEBUG
    printf("func %s, note: in this function\n", __func__);
    #endif

    switch(*cmd_type) {
	case MM_CMD_PLAIN:
	    memcpy(rmtcmd, (struct mm_rmtcmd *)tmpbuf, sizeof(struct mm_rmtcmd) );
	break;
	case MM_CMD_PGLST:
	    memcpy(pglst_cmdrmt, (struct mm_pglst_rmt *)tmpbuf, sizeof(struct mm_pglst_rmt) );
            printf("\t\tfunc %s, note: pglst_cmdrmt->src_region=%u, .dst_region=%u, .trn_id=%i, .cmd=%i, .len=%lu, .src_group=%u, .dst_group=%u, .addr_blks[0].base=%lu, .addr_blks[0].length=%lu\n", 
                       __func__, pglst_cmdrmt->src_region, pglst_cmdrmt->dst_region, pglst_cmdrmt->trn_id, pglst_cmdrmt->cmd, pglst_cmdrmt->len, 
                       pglst_cmdrmt->src_group, pglst_cmdrmt->dst_group, pglst_cmdrmt->addr_blks[0].base, pglst_cmdrmt->addr_blks[0].length);
            for (i=0; i < pglst_cmdrmt->len; i++) {
                printf("\t\tpglst_cmdrmt->addr_blks[i].base=%lu, .length=%lu\n", 
                          pglst_cmdrmt->addr_blks[i].base, pglst_cmdrmt->addr_blks[i].length );
            }
            printf("\t\ttmpbuf= \n");
            //for (i=0; i < sizeof(struct mm_pglst_rmt); i++) {
                //printf("\t\t%lu. tmpbuf[%lu]=%u\n", i, i, tmpbuf[i]);
            //}
	break;
	case MM_STATS:
	    memcpy(stats_rmt, (struct mm_stat_rmt *)tmpbuf, sizeof(struct mm_stat_rmt) );
	break;
    }

    return 1;
}

int32_t mmskt_rcv(uint8_t *cmd_type, struct mm_rmtcmd *rmtcmd, struct mm_pglst_rmt* pglst_cmdrmt, 
		    struct mm_stat_rmt* stats_rmt, uint32_t lnid, struct node_info *ninf, uint32_t node_no) {

    int32_t max_sd, activity, new_socket, addrlen, sd, client_nid=-1, i =0;
    int32_t rdbuf_allocated=0, max_clients= MAX_CLIENTS, valread=0, pr=-1;
    int32_t already_read=0, header_read_attempt=0;
    uint32_t message_size=0, rc=0;
    uint8_t commtype;
    char *tmpbuf=NULL, *otherbuf=NULL;
    struct tmop_cnt *stats_data;
    struct mm_rmtcmd *cmdplain_data;
    struct mm_pglst_rmt *cmdpglst_data;

    fd_set readfds;
    struct timeval tv;

    #ifdef MMSKT_RCV_DEBUG
    //printf("func %s, note: in this function, master_socket=%i\n", __func__, master_socket);
    #endif

    FD_ZERO(&readfds);
    FD_SET(master_socket, &readfds);
    max_sd = master_socket;

    // Adding the client sockets into the readfds
    for (i=0; i < max_clients; i++) {
	if (client_socket[i] >= 0) {
	    FD_SET(client_socket[i], &readfds);
	    max_sd = (client_socket[i] > max_sd) ? client_socket[i] : max_sd;
	}
    }

    tv.tv_sec = MMSKT_SELECT_TIMEOUT_SEC;
    tv.tv_usec = MMSKT_SELECT_TIMEOUT_USEC;

    activity = select( max_sd + 1 , &readfds , NULL , NULL , &tv);

    //#ifdef MMSKT_RCV_DEBUG
    //printf("func %s, note: activity=%i\n", __func__, activity);
    //#endif

    if ((activity < 0) && (errno != EINTR))
    {
	#ifdef MMSKT_RCV_DEBUG
	printf("select error, activity=%i, errno=%i\n", activity, errno);
	#endif
	return 0;
    } else if (activity > 0) {
	#ifdef MMSKT_RCV_DEBUG
	//printf("func %s, note: checking if master_socket is in readfds\n", __func__);
	#endif
	if ( FD_ISSET(master_socket, &readfds) ) {
	    #ifdef MMSKT_RCV_DEBUG
	    printf("func %s, note: master_socket is in readfds\n", __func__);
	    #endif
	    if ((new_socket = accept(master_socket, 
				(struct sockaddr *)&address, (socklen_t*)&addrlen))<0)
	    {
		perror("accept");
                exit(EXIT_FAILURE);
	    } else {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, note: new_socket=%i\n", __func__, new_socket);
		#endif
		if ( !FD_ISSET(new_socket, &readfds) ) {
		    FD_SET(new_socket, &readfds);
		    max_sd = (max_sd < new_socket) ? new_socket:max_sd;
		
		    for (i=0; i < max_clients; i++) {
			if (client_socket[i] == -1) {
			    client_socket[i] = new_socket;
			    break;
			}
		    }
		} else {
		    for (i=0; i < max_clients; i++) {
			if (client_socket[i] == new_socket)
			    client_socket[i] = new_socket;
		    }
		}
	    }

	    FD_CLR(master_socket, &readfds);
	}
    } else if (activity == 0) {
	#ifdef MMSKT_RCV_DEBUG
	//printf("func %s, note: no activity detected in socket\n", __func__);
	#endif
	//return NEUT_NO_ACTIVITY;
    }

    for (i=0; i < max_clients; i++) {
	sd = client_socket[i];
	//#ifdef MMSKT_RCV_DEBUG
	//printf("func %s, note: looping through client sockets, %i. sd=%i\n", 
	//		__func__, i, sd);
	//#endif
	if (FD_ISSET( sd , &readfds) )
	{
	    //#ifdef MMSKT_RCV_DEBUG
	    //printf("func %s, note: looping through sd's\n", __func__);
	    //#endif
	    /*
	    1. If the socket is not bound
		1.1. Allocate tmpbuff to (COMM_HEADER - already_read) size
		1.2. Attempt to read (COMM_HEADER - already read) size from the buffer
		1.3. Try to bind the socket.
		     1.3.1. If socket can be bound, modify alread_read, go to 1.4.
		     1.3.2. If not, go to 1.2.
		1.4. Try to get the command type
		     1.4.1. If able to get it, go to 2.
		     1.4.2. If not, go to 1.2

	    2. If the socket is bound.
		2.1. If it is bounded, check the bffst of the entry
		2.2. If the bffst is equal to zero,
		    - Allocate tmpbuff to COMM_HEADER size
		    - Read COMM_HEADER size from the socket into tmpbuf
		    - 
		2.3. If the bffst is different to zero:
		    - Check if bffst is smaller that COMM_HEADER size. If so: 
			* Allocate tmpbuf to COMM_HEADER size
			* Determine the rest of the amount of data to be
			  read in order to complete the header
			* Attempt to read this amount from the socket
		    - Check if bffst is larger or equal to COMM_HEADER size. If so:
			* Comm type has to be set, too. If not, a problem is present
			* Allocate tmpbuff to the size of the comm type
			* Determine the rest of the amount of data to be read
			  in order to complete the rest of the message
			* Attempt to read this amount from the socket.
	   
	    3. Second, get the rest of the message according to the comm type
	    */
	    client_nid = client_socket_nid(sd, node_no, ninf);

	    if (client_nid > -1) {
		if (ninf[client_nid].bffst < MSKT_COMM_HEADER_SIZE) {
		    ninf[client_nid].rdbuf = (char *)malloc(sizeof(char)*MSKT_COMM_HEADER_SIZE);
		    if (ninf[client_nid].rdbuf == NULL) {
			#ifdef MMSKT_RCV_DEBUG
			printf("func %s, error: cannot allocate ninf[%i].rdbuf\n", 
				__func__, client_nid);
			#endif
			return 0;
		    }
mmskt_rcv_critical_read_with_cliend_nid:
		    valread = read( sd , ((void *)(ninf[client_nid].rdbuf + ninf[client_nid].bffst)),
					 MSKT_COMM_HEADER_SIZE - ninf[client_nid].bffst);

		    ninf[client_nid].bffst += valread;
		    if (ninf[client_nid].bffst < MSKT_COMM_HEADER_SIZE ) {
			#ifdef MMSKT_RCV_DEBUG
			printf("func %s, warning: need to read header, repeat.\n", __func__);
			#endif 
			goto mmskt_rcv_critical_read_with_cliend_nid;
		    }
		    already_read = ninf[client_nid].bffst;
		    goto mmskt_rcv_get_cmd_type;

		} else if (ninf[client_nid].bffst >= MSKT_COMM_HEADER_SIZE) {
		    // this part executes after the header has been received.
		    printf("func %s, note: bigger than MSK_COMM_HEADER, client_nid=%i, ninf[%i].bffst=%i, ninf[%i].cmd_type=%i\n", 
				__func__, client_nid, client_nid, ninf[client_nid].bffst, client_nid, ninf[client_nid].cmd_type);
mmskt_comm_recep:
		    switch (ninf[client_nid].cmd_type) {
			case MM_CMD_PLAIN:
			    #ifdef MMSKT_RCV_DEBUG
			    printf("\tfunc %s, note: MM_CMD_PLAIN command type.\n", __func__);
			    #endif
			    message_size = MM_CMD_PLAIN_SIZE;
			break;
			case MM_CMD_PGLST:
			    #ifdef MMSKT_RCV_DEBUG
			    printf("\tfunc %s, note: MM_CMD_PGLST command type.\n", __func__);
			    #endif
			    message_size = MM_CMD_PGLST_SIZE;
			break;
			case MM_STATS:
			    #ifdef MMSKT_RCV_DEBUG
			    printf("\tfunc %s, note: MM_STATS command type.\n", __func__);
			    #endif
			    message_size = MM_STATS_SIZE;
			break;
			default:
			    #ifdef MMSKT_RCV_DEBUG
			    printf("func %s, error: cannot verify command type.\n", __func__);
			    #endif
			    return 0;
			break;
		    }

		    tmpbuf = (char *)malloc(sizeof(char)*message_size);
		    memcpy(tmpbuf, ninf[client_nid].rdbuf, MSKT_COMM_HEADER_SIZE);
                    
                    printf("\tfunc %s, client_nid=%i, note: ninf[%i].rdbuf=\n", __func__, client_nid, client_nid );
                    /* To fix transmission error when partial transmissions occur	
                    //for (i=0; i < (int32_t)message_size; i++) {
                        //uint32_t a = ninf[client_nid].rdbuf[i];
                        //printf("\t\t%i.ninf[client_nid].rdbuf[[%i]=%u\n", i, i, ninf[client_nid].rdbuf[i] );
                    //} */

		    free(ninf[client_nid].rdbuf);
		    ninf[client_nid].rdbuf = NULL;
		    ninf[client_nid].rdbuf = (char *)malloc(sizeof(char)*message_size);

		    memcpy(ninf[client_nid].rdbuf, tmpbuf, MSKT_COMM_HEADER_SIZE);
		    //free(tmpbuf);
		    //tmpbuf = NULL;

mmskt_read_full_data:
		    valread = read( sd , ((void *)(ninf[client_nid].rdbuf + ninf[client_nid].bffst)),
					 message_size - ninf[client_nid].bffst);
		    ninf[client_nid].bffst += valread;
		    if (ninf[client_nid].bffst == message_size) {
			#ifdef MMSKT_RCV_DEBUG
			printf("func %s, note: transmission completed, ninf[%i].bffst=%i, message_size=%i.\n", 
					__func__, client_nid, ninf[client_nid].bffst, message_size);
			#endif
			pr = MMSKT_FULL_READ;
			memcpy(tmpbuf, ninf[client_nid].rdbuf, message_size);

			free(ninf[client_nid].rdbuf);
			ninf[client_nid].rdbuf = NULL;
			ninf[client_nid].bffst = 0;
			//ninf[client_nid].cmd_type = 0;
			goto mmskt_commtype_determined;
		    } else {
			#ifdef MMSKT_RCV_DEBUG
			printf("func %s, note: ninf[%i].bffst=%u, message_size=%i, transmission NOT completed.\n", 
					__func__, client_nid, ninf[client_nid].bffst, message_size);
			#endif
			pr = MMSKT_PARTIAL_READ;
			//goto manage_transaction;
                        goto mmskt_read_full_data;
		    }
		}
	    }
	
	    #ifdef MMSKT_RCV_DEBUG
	    printf("func %s, note: no client bounded to sd=%i\n", 
			__func__, sd);
	    #endif

	    tmpbuf = (char *)malloc(sizeof(char)*MSKT_COMM_HEADER_SIZE);
	    if (tmpbuf == NULL) {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, error: cannot allocate temporal buffer\n", __func__);
		#endif
		return 0;
	    }

mmskt_rcv_critical_read_no_client_nid:
	    valread = read( sd , ((void *)(tmpbuf + already_read)), MSKT_COMM_HEADER_SIZE - already_read);
	    already_read += valread;
	    if ( (already_read < MSKT_COMM_HEADER_SIZE) && (header_read_attempt < MM_SKTREAD_ATTEMPTS) ) {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, warning: need to read header, repeat.\n", 	
				__func__);
		#endif
		already_read = 0;
		header_read_attempt++;
		goto mmskt_rcv_critical_read_no_client_nid;
	    } else if ((already_read < MSKT_COMM_HEADER_SIZE) && (header_read_attempt == MM_SKTREAD_ATTEMPTS)) {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, error: cannot read header in socket, header_read_attempt=%i, already_read=%i.\n", 	
				__func__, header_read_attempt, already_read);
		#endif
		return ERR_MMSKT_RCV_READHDRSKT;
	    }

	    client_nid = bind_nid(sd, tmpbuf, already_read, ninf, node_no);
	    if (client_nid < 0) {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, error: cannot identify client.\n", __func__);
		#endif
		return 0;
	    } 

	    ninf[client_nid].bffst += already_read;
	    ninf[client_nid].rdbuf = (char *)malloc(sizeof(char)*MSKT_COMM_HEADER_SIZE);
	    if (ninf[client_nid].rdbuf == NULL) {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, error: cannot allocate ninf[%i].rdbuf\n", 
				__func__, client_nid);
		#endif
		return 0;
	    }
	    memcpy(ninf[client_nid].rdbuf, tmpbuf, MSKT_COMM_HEADER_SIZE);

mmskt_rcv_get_cmd_type:
	    #ifdef MMSKT_RCV_DEBUG
	    printf("func %s, note: already_read=%i, MSKT_COMM_HEADER_SIZE=%lu, about to call get_commtype\n", 
                    __func__, already_read, MSKT_COMM_HEADER_SIZE);
	    #endif
	    ninf[client_nid].cmd_type = get_commtype(ninf[client_nid].rdbuf, already_read);
	    if (ninf[client_nid].cmd_type == 0) {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, error: cannot verify command type, ninf[%i].bffst=%u, ninf[%i].cmd_type=%i.\n", 					__func__, client_nid, ninf[client_nid].bffst, client_nid, ninf[client_nid].cmd_type);
		#endif
		return 0;
	    } else {
		#ifdef MMSKT_RCV_DEBUG
		printf("func %s, note: ninf[%i].cmd_type=%i, cmd_type determined\n", 
				__func__, client_nid, ninf[client_nid].cmd_type);
		#endif
		if (tmpbuf != NULL) {
		    free(tmpbuf);
		    tmpbuf = NULL;
		}
		goto mmskt_comm_recep;
	    }	
	  

mmskt_commtype_determined:
	    switch(ninf[client_nid].cmd_type) {
		case MM_CMD_PLAIN:
		case MM_CMD_PGLST:
		case MM_STATS:
		break;
		default:
		    pr = MMSKT_UNKNOWN_CMDTYPE;
		break;
	    }

manage_transaction:
	    switch (pr) {
		case MMSKT_FULL_READ:
		    #ifdef MMSKT_RCV_DEBUG
		    printf("func %s, note: pr = MMSKT_FULL_READ\n", __func__);
		    #endif
		    *cmd_type = ninf[client_nid].cmd_type;
		    rc = mmskt_info_copy(cmd_type, rmtcmd, pglst_cmdrmt, stats_rmt, 
					   tmpbuf);
		    ninf[client_nid].cmd_type = 0;
		    break;
		case MMSKT_PARTIAL_READ:
		    #ifdef MMSKT_RCV_DEBUG
		    printf("func %s, note: pr = MMSKT_PARTIAL_READ\n", __func__);
		    #endif
		    break;
		case MMSKT_DEST_FAIL:
		    #ifdef MMSKT_RCV_DEBUG
		    printf("func %s, note: pr = MMSKT_DEST_FAIL\n", __func__);
		    #endif
		    break;
		case MMSKT_ZERO_READ:
		    #ifdef MMSKT_RCV_DEBUG
		    printf("func %s, note: pr = MMSKT_ZERO_READ\n", __func__);
		    #endif
		    break;
		case MMSKT_UNKNOWN_CMDTYPE:
		    #ifdef MMSKT_UNKNOWN_CMDTYPE
		    printf("func %s, error: MMSKT_UNKNOWN_CMDTYPE\n", __func__);
		    #endif
		    break;
	    }
	    
	    free(tmpbuf);
	    tmpbuf = NULL;

	}
    }


}
