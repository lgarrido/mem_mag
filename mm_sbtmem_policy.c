#include <stdio.h>
#include <math.h>

#include "mm_policy.h"



#ifdef POLICY_4
/*
    Page reservation policy, version A. Constant (de)allocation using visible tmem memory.

    This policy looks at the information coming from the stats (opcn, domcharts), and reserves pages
    for every domain in order to reduce the worst case swap rate (put fail rate, prf). The 

    This is a basic version. It works as follows:
    1. Read the pfr of every domain
    2. If the pfr > 0.1, then give the domain an additional percentage of the available pages. (ALLOCATION_PERCENTAGE)
    3. If the pfr is near zero, take a certain amount of pages from the domain, check the gap between the current target
       and the pages the domain has.
       3.1. If the gap is above a certain threshold, then take away 
            a small percentage of the pages already given to the domain. (DEALLOCATION_PERCENTAGE)
       3.2. If the gap is not above this threshold, keep the current target.

    4. Send to the HV.

    In this policy, all the pages returned by a domain are still reserved by that domain. That is, when a GET/FLUSH occurs,
    the domain does not suffer any changes on the pages it had reserved.

    When a PFR is less than zero, the domain will release pages at a constant rate.
    When the PFR is larger than zero, the domain will be allocated more pages according to priority-percentages.
    If the PFR is zero, the current utiliz
*/

#include <stdlib.h>

#define ALLOCATION_PERCENTAGE           4.00
#define DEALLOCATION_PERCENTAGE         4.00
#define THRESHOLD                       2500

uint32_t memmag(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt,
                        struct pernodechart *nodecharts, struct mm_hyp_cmd *rep,
                        uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {
    int32_t i, rc=0;
    uint64_t pages_available=0;
    uint8_t neggap=0, send_data = 0;
    struct data_pervm *rates_pervm;       // pfr: put fail rate (swap rate)
    struct sendinfo_pervm *vm_perc;       // vm_perc: constant percentages according to the number of domains

    if ( !(nodecharts->nodestats.size > 0 ))
        return 0;

    pages_available = ninf[this_node_id].num_pages;

    rates_pervm = (struct data_pervm *) malloc(nodecharts->domcnt*sizeof(struct data_pervm));
    vm_perc = (struct sendinfo_pervm *) malloc(nodecharts->domcnt*sizeof(struct sendinfo_pervm));

    rep->cmd = 0;
    for (i=0; i < nodecharts->domcnt; i++) {
        rates_pervm[i].dom_id = nodecharts->domcharts[i].dom_id;
        vm_perc[i].dom_id = nodecharts->domcharts[i].dom_id;

        if (nodecharts->domcharts[i].domstats.size >= 1) {
            rates_pervm[i].pfr_inst = nodecharts->domcharts[i].domstats.list_last->putfailrateinst;
            rates_pervm[i].pr_inst = nodecharts->domcharts[i].domstats.list_last->putrateinst;
            rates_pervm[i].fr_inst = nodecharts->domcharts[i].domstats.list_last->flushrateinst;
            rates_pervm[i].diff_prfr_inst = rates_pervm[i].pr_inst - rates_pervm[i].fr_inst;

            if (nodecharts->domcharts[i].domstats.size >= 2) {
                rates_pervm[i].oneprevdiff_prfr_inst = nodecharts->domcharts[i].domstats.list_last->prev->putrateinst - 
                                                           nodecharts->domcharts[i].domstats.list_last->prev->flushrateinst;
            } else {
                rates_pervm[i].oneprevdiff_prfr_inst = 0.0;
            }

            rates_pervm[i].mem_used = nodecharts->domcharts[i].domstats.list_last->nod_mem;
            rates_pervm[i].tmem_used = nodecharts->domcharts[i].domstats.list_last->nod_tmem;

            rates_pervm[i].tmem_target = nodecharts->domcharts[i].domstats.list_last->mm_target;
            rates_pervm[i].balloon_target = nodecharts->domcharts[i].domstats.list_last->balloon_target;             
        }

        // Check if the .diff_prfr_inst is actually increasing

        if ( (rates_pervm[i].pfr_inst >= PFR_THRESHOLD) && (rates_pervm[i].diff_prfr_inst - rates_pervm[i].oneprevdiff_prfr_inst >= DIFF_THRESHOLD )) {
            // In this case, tmem is overflowing and memory is increasing overflowing. Increase the tmem allocation by a larger amount
            // The "larger amount" is 2.0*ALLOCATION_PERCENTAGE.
            // Some of these tmem memory will be given to mem.
            vm_perc[i].tmem_flt_target = ((2.0)*ALLOCATION_PERCENTAGE/100.0)*((float)pages_available);
  
            rep->cmd = MM_RSVPAG;
            send_data = 1;  
        } else if (rates_pervm[i].pfr_inst >= PFR_THRESHOLD
                      && ( (rates_pervm[i].diff_prfr_inst - rates_pervm[i].oneprevdiff_prfr_inst) < DIFF_THRESHOLD) 
                      && ( (rates_pervm[i].diff_prfr_inst - rates_pervm[i].oneprevdiff_prfr_inst) > -DIFF_THRESHOLD ) ) {
            // In this case, tmem is overflowing and memory has overflown, but it is steady. Increase tmem
            // allocation by a slightly larger amount, 1.5*ALLOCATION_PERCENTAGE.
            // Some of these tmem memory will be given to mem.
            vm_perc[i].tmem_flt_target = ((1.5)*ALLOCATION_PERCENTAGE/100.0)*((float)pages_available);
     
            rep->cmd = MM_RSVPAG;
            send_data = 1;
        } else if (rates_pervm[i].pfr_inst >= PFR_THRESHOLD && ( (rates_pervm[i].diff_prfr_inst - rates_pervm[i].oneprevdiff_prfr_inst) <= -DIFF_THRESHOLD ) ) {
            // In this case, tmem keeps overflowing, but memory is underflowing (flushing more than putting). In this case, we
            // decrease mem, and move some of that memory to tmem
            vm_perc[i].balloon_flt_target = (DEALLOCATION_PERCENTAGE/100.0)*((float)pfr_pervm[i].balloon_target);
            vm_perc[i].tmem_flt_target = vm_perc[i].balloon_flt_target;

            rep->cmd = MM_RSVPAG;
            send_data = 1;
        } else if ( (rates_pervm[i].pfr_inst < PFR_THRESHOLD) && (rates_pervm[i].pfr_inst > -PFR_THRESHOLD)
                 && ((rates_pervm[i].diff_prfr_inst - rates_pervm[i].oneprevdiff_prfr_inst) >= DIFF_THRESHOLD) ) {
            // In this case, tmem is not overflowing, now we have to decide to move memory from tmem to mem, but we have to make
            // sure that some conditions are met.

            rep->cmd = MM_RSVPAG;
            send_data = 1;
        }
    }

    return rc;
}

#endif
