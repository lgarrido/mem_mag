#ifndef MM_INC_SOCKCOMM
#define MM_INC_SOCKCOMM

#include "mm_ctrl.h"
#include "node_info.h"

#define MAX_CLIENTS 30
#define MMSKT_SELECT_TIMEOUT_SEC 0
#define MMSKT_SELECT_TIMEOUT_USEC 10

#define MM_SKTCHK_ATTEMPTS	10
#define MM_SKTREAD_ATTEMPTS	10

/* Test commands to verify socket comms */
#define MMSKT_SEND_TEST  4567
#define MMSKT_SEND_TEST_REP 666		// test receive.

/* Control Macros */
// To identify the command type
#define BYTESREAD_COMMTYPE		sizeof(unsigned short)*2 + sizeof(uint32_t)*2
#define MSKT_SEND_WAITSKT_USEC		1000
#define MSKT_COMM_HEADER_SIZE		sizeof(struct mm_rmtcmd)

// Related to the transactions 
#define MMSKT_FULL_READ    	1
#define MMSKT_PARTIAL_READ	2
#define MMSKT_DEST_FAIL		3
#define MMSKT_ZERO_READ		4
#define MMSKT_UNKNOWN_CMDTYPE   5

// Command types
#define MM_CMD_PLAIN		1
#define MM_CMD_PLAIN_SIZE	sizeof(struct mm_rmtcmd)

#define MM_CMD_PGLST		2
#define MM_CMD_PGLST_SIZE	sizeof(struct mm_pglst_rmt)

#define MM_STATS 		3
#define MM_STATS_SIZE		sizeof(struct mm_stat_rmt)

/* MM-to-MM test value */
#define MM_TEST_VALUE_0		678
#define MM_TEST_VALUE_1		279
#define MM_TEST_VALUE_2		739
#define MM_TEST_VALUE_3		365
#define MM_TEST_BASEOFF		6389

#define MM_TESTRESP_LEN		24

/* Successful return values */
#define SUCC_MMSKT_INIT  0
#define SUCC_MMSKT_SEND  0

/* Error return values */
#define ERR_MMSKT_SEND_CANTCONNECT	1

#define NEUT_NO_ACTIVITY		4
#define ERR_MMSKT_RCV_CANTCONNECT	2
#define ERR_MMSKT_RCV_READHDRSKT	3

/* Debug Macros */
//#define MMSKT_GET_COMMTYPE_DEBUG
//#define MMSKT_BIND_NID_DEBUG
//#define MMSKT_SKTCHK_DEBUG
//#define MMSKT_SEND_DEBUG
//#define MMSKT_RCV_DEBUG
//#define MMSKT_CMDPLAIN_TREAT_DEBUG
//#define MMSKT_CMDPGLST_TREAT_DEBUG

struct tmop_rmt {
    uint64_t curr_ts;
    uint64_t prev_ts;
    uint64_t free_pages;
    uint64_t tmem_freeable_pages;
    uint8_t domcnt;
    uint64_t min_request_pages;
    uint32_t region_id;
    uint32_t sys_mem;
    uint32_t tmempages;

    uint64_t total_accesses;

    uint64_t total_list;
    uint64_t local_list;
    uint64_t remote_list;
    uint64_t given_list;
    uint64_t used_list;

    uint64_t total_puts;
    uint64_t total_putssucc;
    uint64_t total_gets;
    uint64_t total_getssucc;
    uint64_t total_flush;
    uint64_t total_flushsucc;

    uint64_t local_puts;
    uint64_t local_gets;
    uint64_t local_flush;
    uint64_t local_accesses;

    uint64_t rem_puts;
    uint64_t rem_gets;
    uint64_t rem_flush;
    uint64_t rem_accesses;

    uint64_t total_tmem;
    uint64_t local_tmem;
    uint64_t remote_tmem;


    // Remote stats coming from there   
    // request stats
    uint64_t req_mcap_in;          // amount of MM_REQ_MCAP rcvd by this node coming from diff nodes
    uint64_t req_mcap_inpagreq;        // amount of pages requested by MM_REQ_MCAPS coming from diff nodes
    uint64_t rcv_mcap_out;          // amount of MM_RCV_MCAP sent out as response of MM_REQ_MCAP
    uint64_t rcv_mcap_outpagsent;       // amount of pages sent with MM_RCV_MCAPs.

    uint64_t req_mcap_out;          // amount of MM_REQ_MCAPs sent by this node
    uint64_t req_mcap_outpagreq;        // amount of pages requested through MM_REQ_MCAPs
    uint64_t rcv_mcap_in;          // amount of MM_RCV_MCAP responses received after sending MM_REQ_MCAPs
    uint64_t rcv_mcap_inpagrcvd;       // amount of pages actually received after issuing a MM_REQ_MCAPs

    // reclaim stats
    uint64_t reclaim_mcap_in;         // amount of MM_RECLAIM_MCAPs rcvd by this node from diff nodes
    uint64_t reclaim_mcap_inpagret;   // amount of pages asked back by MM_RECLAIM_MCAPs coming from diff nodes
    uint64_t reclack_mcap_out;        // amount of MM_RECLACK_MCAPs sent back as response to MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_outpagret;  // amount of pages returned back with MM_RECLACK_MCAPs

    uint64_t reclaim_mcap_out;        // amount of MM_RECLAIM_MCAPs sent by this node 
    uint64_t reclaim_mcap_outpagreq;  // amount of pages asked back by this node when sending MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_in;         // amount of responses registered after issuing MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_inpagret;   // amount of pages received back when receiving MM_RECLACK_MCAP

    // relinquish stats
    uint64_t relack_mcap_in;          // amount of MM_RELACK_MCAPs received by this node from diff nodes
    uint64_t relack_mcap_inpagret;    // amount of pages received back by nodes that have relinquished
    uint64_t relack_mcap_out;
    uint64_t relack_mcap_outpagret;

    // remote pages this node now possesses
    uint64_t remote_pages_taken;     // remote pages the node currently has. This value is incremented
                                     // or decremented.
    uint64_t remote_pages_given;     // remote pages belonging to this node that has given to other
                                     // nodes.
    uint64_t remote_pages_dispin;   // remote pages belonging to other nodes that this node has, but
                                     // it has give away to other nodes
    uint64_t remote_pages_dispout;      // remote pages belonging to other nodes that this node has, but
                                     // it has give away to other nodes

    struct rmtaggr_nodcnt nodinfo[MAX_RMTNODES];
};

struct mm_stat_rmt
{
    uint32_t src_region;
    uint32_t dst_region;
    uint32_t trn_id;
    uint32_t cmd;
    uint64_t len;

    struct tmop_rmt rmt_opcnt;
};


struct mm_pglst_rmt
{
    uint32_t src_region;
    uint32_t dst_region;
    uint32_t trn_id;
    uint32_t cmd;
    uint64_t len;

    uint32_t src_group;	// total -> local/remote/given, given->total
    uint32_t dst_group;
	    
    struct cntg_blk_mm addr_blks[PG_BATCH_SIZE];
};

struct mm_rmtcmd
{
    uint32_t src_region;
    uint32_t dst_region;
    uint32_t trn_id;
    uint32_t cmd;
    uint64_t len;
};

struct mm_rmtcmd_list
{
    struct mm_rmtcmd rmtcmd;
    struct mm_rmtcmd_list *prev, *next;
};

struct sent_mm_rmtcmd		// this are the ones sent by this node
{				// awaiting response from a remote node.
    uint32_t size;
    struct mm_rmtcmd_list *root, *last;
};

struct rcv_mm_rmtcmd		// this are the ones received by this node
{				// that will be serviced.
    uint32_t size;
    struct mm_rmtcmd_list *root, *last;
};

//int32_t mm_hyp_cmd_clear(struct mm_hyp_cmd * mmcmd);
int32_t pglst_rmt_clear(struct mm_pglst_rmt * mmplcmd);

int32_t client_socket_nid(int sd, uint32_t node_no, struct node_info *ninf);
int32_t skt_chk(struct node_info *ninf, uint32_t nid);
uint32_t region_nid(uint32_t region_id, uint32_t node_no, struct node_info *ninf);
int32_t bind_nid(int sd, char * tb, int valread, struct node_info *ninf, uint32_t node_no);
uint8_t get_commtype(char * tb, int32_t valread);
uint64_t char2dig(char * tb, int len);

int mmskt_init( struct node_info *ninf, uint32_t nid);
int32_t mmskt_send( struct mm_hyp_cmd *mmcmd, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t trn_id, 
		uint32_t lnid, struct node_info *ninf, uint32_t node_no );

int32_t mmskt_rcv( uint8_t *cmd_type, struct mm_rmtcmd *rmtcmd, 
		   struct mm_pglst_rmt* pglst_cmdrmt, struct mm_stat_rmt* stats_rmt, 
			uint32_t lnid, struct node_info *ninf, uint32_t node_no );

int32_t mmskt_info_copy(uint8_t *cmd_type, struct mm_rmtcmd *rmtcmd, 	
			  struct mm_pglst_rmt* pglst_cmdrmt, struct mm_stat_rmt* stats_rmt,
			    char *tmpbuf);


int mmskt_close();
int mmskt_wr_test();


#endif
