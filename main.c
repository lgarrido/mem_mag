#include <assert.h>
#include <stdio.h>
#include <string.h> /* memset() */
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>

#include <sys/socket.h>
#include <linux/netlink.h>

#include <stdbool.h>

#include "mm_ctrl.h"
#include "node_info.h"
#include "mm_policy.h"
#include "tmem_mod_if.h"
#include "mm_comm.h"

#define E_PFS_NOTOPEN         50
#define E_TMSOCK_CANNOT_OPEN  51
#define E_TMSOCK_RDFLAG       52
#define E_TMSOCK_WRFLAG       53

#define TMEM_MOD_CLOSE_SUCC   0
#define TMEM_MOD_READ_SUCC    0
#define TMIF_READ_SUCC	      0
#define SUCC_TMEM_MOD_INIT    0

#define TMEM_MOD_READ_FAIL    1
#define TMEM_MOD_WRITE_SUCC   2
#define TMIF_WRITE_SUCC   2
#define TMEM_MOD_WRITE_FAIL   3
#define TMIF_WRITE_FAIL   3

#define ENTRIES_FOR_POLICY 1
#define MAIN_DEBUG 4

#ifdef MAIN_DEBUG
    #define TEST_RESP_EV1 27
    #define TEST_RESP_EV2 69

    //#define TEST_SEQUENCE_1
    //#define TEST_SEQUENCE_2
    //#define TEST_SEQUENCE_3
    //#define TEST_SEQUENCE_4     // for socket comm
    //#define TEST_SEQUENCE_5	// testing the new tmem-user
#endif

/* communication with netlink sockets */
#define NETLINK_USER 31
#define MAX_PAYLOAD sizeof(struct mm_hyp_cmd)

//unsigned int entries=0;
static unsigned int dom_cnt;
static uint8_t check_command;

int main(int argc, char **argv) {
    int32_t fd, j=0, rc;
    uint64_t entries=0, n=0, this_sys_mem=0, this_tmempages=0;
    uint32_t this_node_id=0, this_region_id=0, master_id=0, 
	     rm_region_id=0, rm_node_id=0, node_no;
    int tmsock_fd;
    struct node_info *ninf;

    struct tmop_cnt *opcnt;
    struct rmtaggr_cnt *rmopcnt;  // for every node to keep track how many remote pages it has and from where
    struct pernodechart nodecharts;
    struct mm_hyp_cmd rep;
    char str1[2];

    uint32_t i=0, counter=0; /* for debugging purposes*/
    time_t start_time;

    printf("/*** sizeof(struct tmop_cnt)=%lu ***/\n", sizeof(struct tmop_cnt));
    if (sizeof(struct tmop_cnt) > PAGE_SIZE) {
	printf("func %s, critical error: size of readstats data structure is bigger than a page. Won't work\n", 
			__func__);
	return 0;
    }

    printf("/*** sizeof(struct mm_hyp_cmd)=%lu ***/\n", sizeof(struct mm_hyp_cmd));
    if (sizeof(struct mm_hyp_cmd) > PAGE_SIZE) {
	printf("func %s, critical error: size of hv interface data structure is bigger than a page. Won't work\n", 
			__func__);
	return 0;
    }

    if (argc < 2) {
	printf("Necessary to give it a configuration file\n");
	return 0;
    }

    rc = parnoconf(argv[1], &ninf, &node_no);
    if (rc != CFG_READ_SUCC) {
	printf("Cannot parse configuration file\n");
	return 0;
    }
 
    //printf("node_no=%i, ninf=%lu\n", node_no, (uint64_t)ninf);
    this_node_id = get_this_node_id(ninf, node_no);
    this_region_id = get_this_region_id(ninf, node_no);
    this_tmempages = get_this_tmempages(ninf, node_no);

    // Clear rmopcnt, to keep track of remote memory operations
    // executed as part of the memory management aggregation strategy


    if (ninf[this_node_id].master == 1) {
	master_id = this_node_id;
	opcnt = (struct tmop_cnt *)malloc(sizeof(struct tmop_cnt)*node_no);
        memset(opcnt, 0, sizeof(struct tmop_cnt)*node_no);
        rmopcnt = (struct rmtaggr_cnt *)malloc(sizeof(struct rmtaggr_cnt)*node_no);
    } else {
	for (i=0;i < node_no; i++) {
	    if (ninf[i].master == 1) {
		master_id = i;
	    }
	}
	opcnt = (struct tmop_cnt*) malloc(sizeof(struct tmop_cnt));
        memset(opcnt, 0, sizeof(struct tmop_cnt));
        rmopcnt = (struct rmtaggr_cnt*) malloc( sizeof(struct rmtaggr_cnt) );
    }
    // Remote nodes are registered from the cfg file.
    
    printf("this_node_id=%i, this_region_id=%i, this_tmempages=%lu\n", this_node_id, this_region_id, this_tmempages);

    // Clear the nodecharts and put some relevant data into it.
    rc = pernodechart_clear(&nodecharts, this_region_id);
    if (rc != SUCC_PERNODECHART_CLEAR) {
	#ifdef MAIN_DEBUG
	printf("func %s, error: Cannot clear nodecharts\n", __func__);
	#endif
	return ERR_CLEARING_PERNODECHART;
    }
    nodecharts.region_id = this_region_id;
    nodecharts.tmempages = this_tmempages;
    
    // Initialize interface with TKM
    rc = tmif_init();
    if (rc != SUCC_TMEM_MOD_INIT) {
	printf("Error initializing tmif\n");
	return 0;
    }

    //check_command=0;

    rc = mm_hyp_cmd_clear(&rep);
    if (rc==S_MM_RELATED) {
	printf("Command structure cleared successfully\n");
    }

    //#ifndef TEST_SEQUENCE_4
    rc = set_hyp_ninf(ninf, this_node_id);
    if ( rc != SUCC_SET_HNINF) {
	printf("Error %i: cannot set node information\n", rc);
    }

    printf("about to enter set_total_free_list\n");
    rc = set_total_free_list(ninf, this_node_id);
    rc = set_local_free_list(ninf, this_node_id);
    //#endif

    #ifdef MAIN_DEBUG
    rc = send_test_command();
    rc = resp_test_command(&opcnt[0]);
    rc = resp_test_command(&opcnt[0]);
    if (rc != TMIF_RCVD_EXPECTED) {
	printf("func %s, error: Error receiving messages from kernel, rc=%i\n", __func__, rc);
	printf("Program will be terminated\n");
	return 0;
    }

    if (opcnt[0].domcnt == TEST_RESP_EV1) {
	printf("opcnt[0].domcnt=%i, TEST_RESP_EV1=%i, message received ok\n", 
			opcnt[0].domcnt, TEST_RESP_EV1);
    } else {
	printf("opcnt[0].domcnt=%i, TEST_RESP_EV1=%i, message NOT received ok\n", 
			opcnt[0].domcnt, TEST_RESP_EV1);
    }

    rc = send_test_command();
    rc = resp_test_command(&opcnt[0]);
    if (rc != TMIF_RCVD_EXPECTED ) {
	printf("func %s, error: Error in receiving messages from kernel, sizeof(struct tmop_cnt)=%i, rc=%i\n", 
			__func__, (uint32_t)sizeof(struct tmop_cnt), rc);
	printf("Program will be terminated\n");
	return 0;
    }
    if (opcnt[0].domcnt == TEST_RESP_EV2) {
	printf("opcnt[0].domcnt=%i, TEST_RESP_EV2=%i, message received ok\n", 
			opcnt[0].domcnt, TEST_RESP_EV2);
    } else {
	printf("opcnt[0].domcnt=%i, TEST_RESP_EV2=%i, message NOT received ok\n", 
			opcnt[0].domcnt, TEST_RESP_EV2);
    }
    #endif

    // Initialize socket interface with remote MM
    rc = mmskt_init( ninf, this_node_id);
    if (rc != SUCC_MMSKT_INIT) {
	printf("Error initializing socket for remote MMs\n");
	return -1;
    }

    scanf("%s", str1);

    #ifdef MAIN_DEBUG
    rc = mm_fn2n_test_cmd(this_region_id, this_node_id, ninf, node_no);
    sleep(2);
    rc = mm_cmd_rcv(opcnt, rmopcnt, &nodecharts, &entries, this_region_id, node_no, this_node_id, ninf );
    if (rc <= 0) {
	printf("func %s, error: Error receiving messages from remote MM, rc=%i\n", __func__, rc);
	printf("Program will be terminated\n");
	return 0;
    }
    rc = mm_cmd_rcv(opcnt, rmopcnt, &nodecharts, &entries, this_region_id, node_no, this_node_id, ninf );
    if (rc <= 0) {
        printf("func %s, error: Error receiving messages from remote MM, rc=%i\n", __func__, rc);
        printf("Program will be terminated\n");
        return 0;
    }

    sleep(1);
    // Final response from the test commands
    rc = mm_cmd_rcv(opcnt, rmopcnt, &nodecharts, &entries, this_region_id, node_no, this_node_id, ninf );
    if (rc <= 0) {
	printf("func %s, error: Error receiving messages from remote MM, rc=%i\n", __func__, rc);
	printf("Program will be terminated\n");
	return 0;
    }

    #endif

    printf("func %s, note: before node registration\n", __func__);
    sleep(2);

    rc = node_register(opcnt, rmopcnt, ninf, node_no, this_node_id );

    printf("*** Checking node registration in opcnt and rmopcnt: ***\n");
    printf("\topcnt: \n");
    if ( ninf[this_node_id].master == 1 ) {
        for (i = 0; i < node_no; i++) {
            printf("\t\tthis_node_id=%i, opcnt[%i].region_id=%i, .total_list=%lu, .local_list=%lu, .remote_list=%lu\n", 
                  this_node_id, i, opcnt[i].region_id, opcnt[i].total_list, opcnt[i].local_list,
                  opcnt[i].remote_list );
        }
    } else {
       printf("\t\tthis_node_id=%i, opcnt[%i].region_id=%i, .total_list=%lu, .local_list=%lu, .remote_list=%lu\n",
                  this_node_id, this_node_id, opcnt[0].region_id, opcnt[0].total_list, opcnt[0].local_list,
                  opcnt[0].remote_list );
    }
    printf("\trmopcnt (some entries will have region_id=0): \n");
    if ( ninf[this_node_id].master == 1 ) {
        for (i = 0; i < node_no; i++) {
            printf("\t\trmopcnt[%i].region_id=%u, .remote_pages_dispout=%lu\n", i, rmopcnt[i].region_id, rmopcnt[i].remote_pages_dispout);
            for (j = 0; j < MAX_RMTNODES; j++) {
                printf("\t\trmopcnt[%i].nodinfo[%i].region_id=%u, .remote_pages_dispout=%lu\n",
                  i, j, rmopcnt[i].nodinfo[j].region_id, rmopcnt[i].nodinfo[j].remote_pages_dispout);
            }
        }
    } else {

        printf("\t\trmopcnt.region_id=%u, .remote_pages_dispout=%lu\n", rmopcnt[0].region_id, rmopcnt[0].remote_pages_dispout);
        for (i = 0; i < MAX_RMTNODES; i++) {
            printf("\t\trmopcnt.nodinfo[%i].region_id=%u, .remote_pages_dispout=%lu\n", 
                  i, rmopcnt[0].nodinfo[i].region_id, rmopcnt[0].nodinfo[i].remote_pages_dispout);
        }

    }

    printf("***** starting the program *****\n"); 
    start_time = time(NULL);

    // Send stats to remote node if the macro for remoteness is defined
    #ifdef POLICY_RMT 
    if (ninf[this_node_id].master != 1 ) {
        rc = mm_stats_cmd(&opcnt[0], &rmopcnt[0], MM_SND_STATS, ninf[this_node_id].region,
                                 ninf[master_id].region, this_node_id, ninf, node_no );
    }
    #endif

    entries = 0;
    while (1) {
	//printf("Entry: %i\n", counter);

	// Receive stats from the Hypervisor if any
	rc = stats_rcv(&opcnt[0], rmopcnt,  &nodecharts, &entries, this_region_id, 0, node_no, this_node_id, ninf);
        //printf("func %s, note: before mm_cmd_rcv, this_node_id=%i, opcnt[%i].total_list=%lu\n", 
        //             __func__, this_node_id, this_node_id, opcnt[this_node_id].total_list );

	// Receive commands from remote MM (stats, mem ops, etc)
	rc = mm_cmd_rcv( opcnt, rmopcnt, &nodecharts, &entries, this_region_id, node_no, this_node_id, ninf );
        //printf("func %s, note: after mm_cmd_rcv, this_node_id=%i, opcnt[%i].total_list=%lu\n", __func__, this_node_id, this_node_id, opcnt[this_node_id].total_list );

	// Call up the desired memory management policy 
	if (entries == ENTRIES_FOR_POLICY) {
	    //printf("func %s, note: calling up management policy, entries=%lu\n", __func__, entries);
            #ifdef POLICY_LOCAL 
	    rc = memmag(&opcnt[0], rmopcnt, &nodecharts, &rep, this_node_id, ninf, node_no);
            #endif

            #ifdef POLICY_RMT 
	    rc = memmag(opcnt, rmopcnt, &nodecharts, &rep, this_node_id, ninf, node_no);
            #endif

	    if (rc < 0) {
	        printf("\tSome kind of error occured when executing the algorithm.\n");
	    }
	    entries = 0;
	} else if (ENTRIES_FOR_POLICY == 0) {
	    entries = 0;
	}
	

	// Check if remote mms are awaiting any response from me, and
	// execute whatever they need
	//rc = mm_remote_comm(&opcnt);

	#ifdef MAIN_DEBUG
	#ifndef TEST_SEQUENCE_4
	if (time(NULL) >= start_time + 10 && this_region_id == 19) {
	    struct mm_hyp_cmd tcmd;

	    if (counter == 1) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue MM_REQ_MCAP\n", i);
		mm_req_mcap(&tcmd, &opcnt[0], 5000);
		#endif

		#ifdef TEST_SEQUENCE_2
		printf("%i. About to issue MM_REQ_MCAP\n", i);
		mm_req_mcap(&tcmd, &opcnt[0], 5000);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    } else if (counter == 2) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue move_total_to_remote\n", i);
		move_total_to_remote(3000);
		#endif

		#ifdef TEST_SEQUENCE_2
		printf("%i. About to issue MM_RCV_MCAP\n", i);
		mm_rcv_mcap(&tcmd);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    } else if (counter == 3) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue MM_RCV_MCAP\n", i);
		mm_rcv_mcap(&tcmd);
		#endif

		#ifdef TEST_SEQUENCE_2
		printf("%i. About to issue MM_RELINQ_MCAP\n", i);
		mm_relinq_mcap(&tcmd, 200, 20);
		#endif
	
		start_time = time(NULL);
	    } else if (counter == 4) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue move_total_to_local\n", i);
		move_remote_to_total(5000);
		#endif

		#ifdef TEST_SEQUENCE_2
		printf("%i. About to issue MM_RELACK_MCAP\n", i);
		tcmd.cmd = MM_RELACK_MCAP;
		mm_relack_mcap(&tcmd);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    } else if (counter == 5) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(456);
		#endif

		#ifdef TEST_SEQUENCE_2
		rc = mm_hyp_cmd_clear(&rep);
    		if (rc!=S_MM_RELATED) {
		    printf("Command structure not cleared successfully\n");
    		}
		printf("%i. About to issue MM_RECLAIM_MCAP\n", i);
		mm_reclaim_mcap(&tcmd, 305, 20);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    } else if (counter == 6) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(456);
		#endif

		#ifdef TEST_SEQUENCE_2
		printf("%i. About to issue MM_RECLACK_MCAP\n", i);
		tcmd.cmd = MM_RECLACK_MCAP;
		mm_reclack_mcap(&tcmd);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    } else if (counter == 10) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue move_remote_to_total\n", i);
		move_total_to_remote(3000);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    } else if (counter != 3) {
		#ifdef TEST_SEQUENCE_1
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(1000);
		#endif

		#ifdef TEST_SEQUENCE_3
		printf("%i. About to issue move_total_to_local\n", i);
		move_total_to_local(500);
		#endif

		start_time = time(NULL);
	    }
	    counter++;
	}
	#endif
	#endif

	#ifdef TEST_SEQUENCE_4
	if (ninf[this_node_id].master != 1) {
	    opcnt[this_node_id].total_list = entries*5+1;
	    opcnt[this_node_id].local_list = entries*6 + 2;
	    printf("func %s, note: not master\n", __func__);
	} else {
	    opcnt[this_node_id].total_list = entries;
	    opcnt[this_node_id].local_list = entries*2;
	}

	printf("/*** Waiting two seconds before sending ***/");
	sleep(1);

	if (ninf[this_node_id].master == 1) {
	    for (i=0; i < node_no; i++) {
		if (i != this_node_id) {
		    rc = mm_stats_cmd(&opcnt[0], &rmopcnt[0], MM_REQ_STATS, 
					this_region_id, ninf[i].region, this_node_id, ninf, node_no);
		}
	    }
	}
	else {
	    rc = mm_stats_cmd(&opcnt[0], &rmopcnt[0], MM_SND_STATS, 
				this_region_id, ninf[master_id].region, this_node_id, ninf, node_no);
	}


	if ((counter /*% 5*/) == 1) {
	    // Have them both send each other MM_REQ_MCAP commands, request different amount of memories
	    for (i=0; i < node_no; i++ ){
		printf("func %s, note: about to send MM_REQ_MCAP to node=%i, region=%i\n\n", 
			__func__, i, ninf[i].region);
		rc = mm_plain_cmd(MM_REQ_MCAP, ninf[i].region, (this_region_id*1000), 
			this_node_id, ninf, node_no);
	    }
	} /*else if ( (counter % 3) == 0) {
	    for (i=0; i < node_no; i++) {
		if (ninf[i].master == 1) {
		    printf("func %s, note: about to send MM_FN2M_REQ_MCAP to node=%i, region=%i\n", 
			__func__, i, ninf[i].region);
		    rc = mm_plain_cmd(MM_FN2M_REQ_MCAP, ninf[i].region, (this_region_id*1000),
					   this_node_id, ninf, node_no);
		}
	    }
	}*/ else if ( (counter /*% 7*/) == 15 ) {
	    for (i=0; i < node_no; i++) {
		if (i != this_node_id) {
		    printf("func %s, note: about to send MM_RECLAIM_MCAP to node=%i, region=%i\n", 
				__func__, i, ninf[i].region);
		    rc = mm_plain_cmd(MM_RECLAIM_MCAP, ninf[i].region, (this_region_id*400),
					   this_node_id, ninf, node_no);
		}
	    }	
	} else if ( counter == 7) {
	    for (i=0; i < node_no; i++) {
		if (i != this_node_id) {
		    rc = mm_relinq_mcap(&rep, 1200, ninf[i].region);

		    printf("func %s, note: about to send MM_RECLAIM_MCAP to node=%i, region=%i, rep.m.pglst_mng.region_id=%i\n", 
				__func__, i, ninf[i].region, rep.m.pglst_mng.region_id);
		    rc = mm_pglst_cmd(&rep, MM_RELACK_MCAP, this_node_id, ninf, node_no);
		}
	    }
	}

	counter++;
	#endif

	#ifdef TEST_SEQUENCE_5
	for (i=0; i < node_no && (counter > 1 && counter < 100); i++ ){
	    //if (this_region_id == 20) {
	    printf("func %s, note: about to send MM_REQ_MCAP to node=%i, region=%i\n\n", 
			__func__, i, ninf[i].region);
	    rc = mm_plain_cmd(MM_REQ_MCAP, ninf[i].region, (this_region_id*200), 
			this_node_id, ninf, node_no);
	    //}
	}
	
	counter++;
	#endif

	i++;
    }

    free(opcnt);
    return 0;
}
