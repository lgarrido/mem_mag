#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <arpa/inet.h> 
#include <limits.h>
#include <linux/sockios.h>

#include "socket_comm.h"

int client_socket[MAX_CLIENTS], master_socket;
struct sockaddr_in address;


/*int client_socket_nid(int sd, unsigned int node_no) {
    int i, ind=-1;

    for (i=0; i < node_no; i++) {
	if (sd == ninf[i].sd ) {
	    ind = i;
	    break;
	}
    }

    return ind;
}


int mmskt_init(struct node_info *ninf, int nid) {
    int i, opt = 1, addrlen, max_clients= MAX_CLIENTS;

    for (i = 0; i < max_clients; i++)
    {
        client_socket[i] = 0;
    }

    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0)
    {
	printf("socket failed");
	exit(EXIT_FAILURE0);
    }

    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )
    {
	printf("setsockopt");
	exit(EXIT_FAILURE0);
    }

    /*bzero((char *) &address, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = ninf[nid].portno; //htons( portno );

    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0)
    {
	perror("bind failed");
	exit(EXIT_FAILURE);
    }

    if (listen(master_socket, 10) < 0)
    {
	perror("listen");
	exit(EXIT_FAILURE);
    }

    return SUCC_MMSKT_INIT;
}*/

/*int mmskt_rcv(struct tmop_cnt *opcnt) {
    int max_sd, activity, new_socket, addrlen, sd;
    fd_set readfds;

    FD_ZERO(&readfds);
    FD_SET(master_socket, &readfds);
    max_sd = master_socket;

    // Adding the client sockets into the readfds
    for (i=0; i < max_clients; i++) {
	if (client_socket[i] >= 0) {
	    FD_SET(client_socket[i], &readfds);
	    max_sd = (client_socket[i] > max_sd) ? client_socket[i] : max_sd;
	}
    }

    tv.tv_sec = MMSKT_SELECT_TIMEOUT_SEC;
    tv.tv_usec = MMSKT_SELECT_TIMEOUT_USEC;

    activity = select( max_sd + 1 , &readfds , NULL , NULL , &tv);

    if ((activity < 0) && (errno != EINTR))
    {
	printf("select error, activity=%i, errno=%i\n", activity, errno);
	return 0;
    } else if (activity > 0) {
	if ( FD_ISSET(master_socket, &readfds) ) {

	    if ((iof = fcntl(master_socket, F_GETFL, 0)) != -1)
         	fcntl(master_socket, F_SETFL, iof | O_NONBLOCK);

	    if ((new_socket = accept(master_socket, 
				(struct sockaddr *)&address, (socklen_t*)&addrlen))<0)
	    {
		perror("accept");
                exit(EXIT_FAILURE);
	    } else {
		if ( !FD_ISSET(new_socket, &readfds) ) {
		    FD_SET(new_socket, &readfds);
		    max_sd = (max_sd < new_socket) ? new_socket:max_sd;
		
		    for (i=0; i < max_clients; i++) {
			if (client_socket[i] == -1) {
			    client_socket[i] = new_socket;
			    break;
			}
		    }
		} else {
		    for (i=0; i < max_clients; i++) {
			if (client_socket[i] == new_socket)
			    client_socket[i] = new_socket;
		    }
		}
	    }

	    FD_CLR(master_socket, &readfds);
	}
    }

    for (i=0; i < max_clients; i++) {
	sd = client_socket[i];
	if (FD_ISSET( sd , &readfds) )
	{
	    client_nid = client_socket_nid(sd);

	    if (client_nid > -1)
		valread = read( sd , (void *)tmpbuf, sizeof(struct rmtrns_op) - ninf[client_nid].bffst);
	    else
		valread = read( sd, (void*)tmpbuf,  sizeof(struct rmtrns_op) );

	    ioctl(sd, SIOCINQ, &pending);

	    if (valread == 0) {
		pr = TRNS_ZERO_READ;
		goto manage_transaction;
    	    }

	    if (client_nid == -1) {
 		client_nid = bind_nid(sd, tmpbuf, valread);
   	    }


	    if (valread < RDBUF_SIZE) {
		if (rdbuf_allocated == 0) {
		    ninf[client_nid].rdbuf = (char *)malloc( sizeof(struct rmtrns_op) );
		    rdbuf_allocated=1;
		}

		memcpy(ninf[client_nid].rdbuf + ninf[client_nid].bffst, tmpbuf, valread);
		ninf[client_nid].bffst += valread;
		pr = TRNS_PARTIAL_READ;

		if (ninf[client_nid].bffst == RDBUF_SIZE) {
		    valread = RDBUF_SIZE;
		    ninf[client_nid].bffst = 0;
		    memcpy(tmpbuf, ninf[client_nid].rdbuf, RDBUF_SIZE);
		    free(ninf[client_nid].rdbuf);
		    rdbuf_allocated = 0;
		    goto call_tmpbuf_proc;
		}
		goto manage_transaction;
	    }


call_tmpbuf_proc:
	    pr = TRNS_FULL_READ; 
	    memcpy(&trns, tmpbuf, sizeof(struct rmtrns_op)

manage_transaction:
	    switch (pr) {
		case TRNS_FULL_READ:
		    #ifdef VERBOSE
		    printf("pr = TRNS_FULL_READ\n");
		    #endif

		    mmskt_treat(&trns, ninf[client_nid].sd);
		    break;
		case TRNS_PARTIAL_READ:
		    #ifdef VERBOSE
		    printf("pr = TRNS_PARTIAL_READ\n");
		    #endif
		    break;
		case TRNS_DEST_FAIL:
		    #ifdef VERBOSE
		    printf("pr = TRNS_DEST_FAIL\n");
		    #endif
		    break;
		case TRNS_ZERO_READ:
		    #ifdef VERBOSE
		    //printf("pr = TRNS_ZERO_READ\n");
		    #endif
		    break;
	    }

	}
    }


}

int skt_chk(unsigned int nid) {
    struct sockaddr_in serv_addr;
    int socket_opened = 0;

    // No attempt to open socket if the block is on the same node
    if (ninf[nid].self == 1) {
	return 0;
    }

    if ( ninf[nid].rem_socket < 0 ) {
	#ifdef VERBOSE
	printf("About to open socket ");
	#endif
	ninf[nid].rem_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (ninf[nid].rem_socket < 0 ) {
	    #ifdef VERBOSE
	    printf("ERROR opening socket. Cannot go remote, ninf[%i].rem_socket=%i ", 
			nid, ninf[nid].rem_socket); 
	    #endif
	    close(ninf[nid].rem_socket);
	    ninf[nid].rem_socket = -1;
	    ninf[nid].connected = -1;
	    return -1;
	} else {
	    #ifdef VERBOSE
	    printf("Socket opened succesfully ");
	    #endif
	    socket_opened = 1;
	}
    } else {
	#ifdef VERBOSE
	//printf("Socket already opened ");
	#endif
	socket_opened = 1;
    }

    if ( socket_opened ) {
	if (ninf[nid].connected < 0) { 
	    serv_addr.sin_family = AF_INET;
	    serv_addr.sin_addr.s_addr = ninf[nid].naddr.sin_addr.s_addr;
	    serv_addr.sin_port = ninf[nid].portno;

	    #ifdef VERBOSE
	    printf("About to connect socket ");
	    #endif

	    ninf[nid].connected = connect(ninf[nid].rem_socket,(struct sockaddr *) &serv_addr, sizeof(serv_addr));
	    if (ninf[nid].connected < 0) {
		#ifdef VERBOSE
		printf("ERROR connecting ninf[%i].rem_socket=%i, ninf[%i].connected=%i, errno=%i\n", 
			nid, ninf[nid].rem_socket, nid, ninf[nid].connected, errno);
		#endif
		close(ninf[nid].rem_socket);
	   	ninf[nid].rem_socket = -1;
	    	ninf[nid].connected = -1;
		return -1;
	    }
	    #ifdef VERBOSE 
	    else {
		printf("Socket connect success ");
		return 1;
	    }
	    #endif
	} else if ( ninf[nid].connected > 0 && socket_opened) {
	    #ifdef VERBOSE
	    printf("Socket connected ");
	     #endif
	    return 1;
	}
    }

    return 1;
}


void mmskt_treat(struct rmtrns_op * trns) {
    char *the_block;
    struct rmtrns_op ack_trns;
    struct remote_reply reply;
    struct rnid curr_rnid;
    int n, i, skt_cool=0, get_error=0;

    switch (trns->cmd) {
	case REMOTE_OP_PUT:

	    for (i=0; i < node_no; i++) {
		if (ninf[i].region == trns->src_region) {
		    curr_rnid.nid= i;
		    curr_rnid.rid= trns->src_region; 
		}
	    }
#ifdef VERBOSE
	    printf("PUT from remote region=%i trns->cmd=%i, trns->block_id=%lu src_region=%i, nid=%i ninf[%i].rem_socket=%i, trns->trn_id=%i ", 
			ninf[ninf_ind].region, trns->cmd, trns->block_id, 
			curr_rnid.rid, curr_rnid.nid, curr_rnid.nid, 
			ninf[curr_rnid.nid].rem_socket, trns->trn_id);
#endif


	    skt_cool = skt_chk(curr_rnid.nid);
	    while (skt_cool < 0) {
	 	skt_cool = skt_chk(curr_rnid.nid);
	    }

	    add_block(trns->block_id, trns->buf);

	    ack_trns.src_region = ninf[ninf_ind].region;
	    ack_trns.dst_region = trns->src_region;
	    ack_trns.cmd = REMOTE_OP_PUT_ACK;
	    ack_trns.block_id = trns->block_id;
	    ack_trns.trn_id = trns->trn_id;
	    memset(ack_trns.buf, 0, BLOCK_SIZE);

retry_putack_remote:	
	    #ifdef VERBOSE
	    printf(" sending put ack ");
	    #endif
	    n = write(ninf[curr_rnid.nid].rem_socket, &ack_trns, sizeof(struct rmtrns_op) );
	    if (n <= 0) {
		close(ninf[curr_rnid.nid].rem_socket);
		ninf[curr_rnid.nid].rem_socket = -1;
		ninf[curr_rnid.nid].connected = -1;
		
		#ifdef VERBOSE
	   	printf("ERROR writing to socket, n=%i \n", n);
		printf(" retry ");
		#endif

		skt_cool = skt_chk(curr_rnid.nid);
		while (skt_cool < 0) {
		    skt_cool = skt_chk(curr_rnid.nid);
		}

	 	goto retry_putack_remote;
	    }
		
	    #ifdef VERBOSE
		printf(" ack written ");
	    #endif 

	break;
	case REMOTE_OP_GET:

	    for (i=0; i < node_no; i++) {
		if (ninf[i].region == trns->src_region) {
		    curr_rnid.nid= i;
		    curr_rnid.rid= trns->src_region; 
		}
	    }

	    skt_cool = skt_chk(curr_rnid.nid);
	    while (skt_cool < 0) {
		skt_cool = skt_chk(curr_rnid.nid);
	    }
#ifdef VERBOSE
	    printf("GET from remote region=%i trns->cmd=%i, trns->block_id=%lu src_region=%i, nid=%i ninf[%i].rem_socket=%i, trns->trn_id=%i", 
	    		ninf[ninf_ind].region, trns->cmd, trns->block_id, 
			curr_rnid.rid, curr_rnid.nid, curr_rnid.nid, ninf[curr_rnid.nid].rem_socket, trns->trn_id );
#endif
	    the_block = get_block(trns->block_id);
	    if (the_block)
	    {
		ack_trns.cmd = REMOTE_OP_GETOK;
		ack_trns.block_id = trns->block_id;
		memcpy(ack_trns.buf, the_block, BLOCK_SIZE);
            }
            else
            {
		ack_trns.cmd = REMOTE_OP_GETFAIL;
		ack_trns.block_id = trns->block_id;
		memset(ack_trns.buf, 0, 4096);
            }
		
	    ack_trns.src_region = ninf[ninf_ind].region;
	    ack_trns.dst_region = trns->src_region;
   	    ack_trns.trn_id = trns->trn_id;

retry_getack_remote:
	    n = write(ninf[curr_rnid.nid].rem_socket, &ack_trns, sizeof(struct rmtrns_op) );

	    if (n <= 0) {
		close(ninf[curr_rnid.nid].rem_socket);
		ninf[curr_rnid.nid].rem_socket = -1;
		ninf[curr_rnid.nid].connected = -1;
		#ifdef VERBOSE
	   	printf("ERROR writing to socket, n=%i \n", n);
		printf(" retry ");
		#endif

		skt_cool = skt_chk(curr_rnid.nid);
		while (skt_cool < 0) {
		    #ifdef VERBOSE
	    	    printf("socket not cool, ninf[%i].rem_socket=%i\n", curr_rnid.nid, ninf[curr_rnid.nid].rem_socket);
	    	    #endif
	    	    skt_cool = skt_chk(curr_rnid.nid);
		}

		goto retry_getack_remote;
	    }

		#ifdef VERBOSE		
		printf(" get ack written ");
		#endif
		n=0;

	break;
	case REMOTE_OP_GETOK:
#ifdef VERBOSE
	    printf("GETOK region=%i trns->cmd=%i, trns->block_id=%lu, trns->trn_id=%i ", 
			ninf[ninf_ind].region, trns->cmd, trns->block_id, trns->trn_id );
#endif
	    //cntrmt_get_good++;
	    reply.op.cmd = trns->cmd;
	    reply.op.block_id = trns->block_id;
	    memcpy(reply.buf, trns->buf, BLOCK_SIZE);

	    // TODO
	    #ifdef PROCFS
	    n = write(fd, (void *)&reply, sizeof(struct remote_reply));
	    rmtgt_srvd = 1;
	    #endif

	    #ifdef TRACE
	    the_block = get_block(trns->block_id);
	    if (the_block)
	    {
		for (i=0; i < BLOCK_SIZE; i++) {
		     if (the_block[i] != trns->buf[i]) {
			get_error = 1;
		     }
		}
            }

	    if (get_error == 1) {
		printf("\nGET/PUT not right with the_block and trns->buf\n");
		//printf("\nprinting every block that was got\n");
		for (i=0; i < BLOCK_SIZE; i++) {
		     printf("%i. the_block[%i]=%i and trns->buf[%i]=%i ", i, i, the_block[i], i, trns->buf[i]);
		     if (the_block[i] != trns->buf[i]) {
			printf(" NOT equal ");		
		     }
		     printf("\n");
		}

		//if (get_error == 1)
		exit(0);
	    }
	    #endif

	    #ifdef DEBUG
	    getblkrtn = gtblktst(trns->block_id);
	    switch(getblkrtn) {
		case BLKGET_CNTINCRSD:
		    printf("\nBLKGET_CNTINCRSD, trns->block_id=%lu, get_cnt=%i\n", trns->block_id, gtblkcnt(trns->block_id));
		    break;
		case BLKGET_NOTPRESENT:
		    printf("\nBLKGET_NOTPRESENT, trns->block_id=%lu\n", trns->block_id);
		    exit(0);
		    break;
	    }
	    #endif

	break;
	case REMOTE_OP_GETFAIL:
#ifdef VERBOSE
	    printf("GETFAIL region=%i, trns->cmd=%i, trns->block_id=%lu, ", 
				ninf[ninf_ind].region, trns->cmd, trns->block_id);
#endif

	    reply.op.cmd = trns->cmd;
 	    reply.op.block_id = trns->block_id;

	    // TODO
	    #ifdef PROCFS
	    n = write(fd, (void *)&reply.op, sizeof(struct remote_op));
	    rmtgt_srvd = 1;
	    #endif

	break;
	case REMOTE_OP_PUT_ACK:
#ifdef VERBOSE
	    printf(" PUTACK trns->cmd=%i, trns->block_id=%lu, trns->src_region=%i, trns->dst_region=%i, trns->trn_id=%i ", 
			trns->cmd, trns->block_id, trns->src_region, trns->dst_region, trns->trn_id);
#endif
	break;
        default:
#ifdef VERBOSE
                printf("Unrecognised remote %i\n", trns->cmd);
#endif
        break;
    }

#ifdef VERBOSE
    printf("\n");
#endif
}*/
