

#include "mm_strategies.h"



#ifdef POLICY_0
int memmag(struct tmop_cnt *opcnt, 
			struct perdomchart *domcharts, struct mm_hyp_cmd *rep) {

    /* 
	The policy in this case is as follows.

	1. Look for the ratio of puts and gets individual to each domain on a per timestamp basis, 
	   for a WINDOW_SIZE.
	   This is to determine if a domain requires more tmem pages due to high demand.
	2. Compare the ratios of each domain. 
	3. Depending on certain values determine:
	    3.1. If more tmem pages will increase the number of reserved pages to a domain
	    3.2. If more physical memory will be allocated to a domain.
	4. Consider the amount of (t)mem pages it already has, the amount of memory in the system,
	   the amount of free memory, to determine how much (t) mem pages will be given to the domain.

	Very simple policy. For now focus on 3.2, and generate a hypercall to give more memory to the domain. 
    */
	// 1. Accumulate the put-to-get ratio of every domain
	check_command = 1;
	for (i=0; i < opcnt->nod_domcnt; i++) {
	    for (j=0; j < WINDOW_SIZE; j++) {
		
		if (domcharts[i].putall[j].y > 0 || domcharts[i].getall[j].y > 0) {
		    put_val = (float) domcharts[i].putall[j].y;
		    get_val = (float) domcharts[i].getall[j].y;

		    //printf("\t\tadding %2.4f\n", (put_val / (get_val + put_val)) );
		    domcharts[i].ptgtrt = domcharts[i].ptgtrt + (put_val / (put_val + get_val));
		}

		/*printf("\t\tdomain=%i, domcharts[%i].putall[%i].y=%lu, domcharts[%i].getall[%i].y=%lu, domcharts[%i].ptgtrt=%2.4f\n", 
			domcharts[i].dom_id, 
			i, j, domcharts[i].putall[j].y, 
			i, j, domcharts[i].getall[j].y,
			i, domcharts[i].ptgtrt );*/
	    }
	    domcharts[i].ptgtrt = domcharts[i].ptgtrt / WINDOW_SIZE;
	    acum = domcharts[i].ptgtrt + acum;
	}
	
	// 2. Average the ratio across the window_size, 
	// 3. find the maximum put-to-get ratio
	printf("\t\tentries=%i, opcnt->nod_domcnt=%i\n", entries, opcnt->nod_domcnt);
	for (j=0; j < opcnt->nod_domcnt; j++) {
	    domcharts[j].ptgtrt = domcharts[j].ptgtrt / acum;
	    if (max_ptgtrt < domcharts[j].ptgtrt) {
		max_ptgtrt = domcharts[j].ptgtrt;
		ind = j; 
	    }

	    // 4. Determine the amount of pages to reserve for each domain
	    domcharts[j].page_target = (uint64_t)(((float)opcnt->nod_freepag) * domcharts[j].ptgtrt);

	    // 5. Compare the amount of pages that should be reserved to the ones the domain already has, and determine
	    // if a hypercall is necessary.
	    domcharts[j].page_current = opcnt->dominfo[j].nod_mem + opcnt->dominfo[j].nod_tmem;
	    //if ( (domcharts[j].page_current < (domcharts[j].page_target - PAGE_THRESHOLD) ) 
		//|| (domcharts[j].page_current > (domcharts[j].page_target + PAGE_THRESHOLD) ) ) {
		rep[j].cmd = MM_RSVPAG;
		rep[j].dom_id = domcharts[j].dom_id;
		rep[j].page_target = domcharts[j].page_target;
		rep[j].page_current = domcharts[j].page_current;

		//sprintf(rep[j].buf, "%s=%i,%s=%lu,%s=%lu\0", 
		//		  "domain_id", domcharts[j].dom_id, 
		//		  "page_target", domcharts[j].page_target,
		//		  "page_current", domcharts[j].page_current);
	    //} else {
		//rep[j].cmd = MM_VOID;	    
	    //}
	
	    //printf("\t\tdomain=%i, current pages=%lu, target pages=%lu, total pages=%lu, free pages=%lu, percent=%1.5f\n", 
		//	domcharts[j].dom_id, 
		//	domcharts[j].page_current, 
		//	domcharts[j].page_target,
		//	opcnt->nod_totpag,
		//	opcnt->nod_freepag,
		//	domcharts[j].ptgtrt );
	}
	printf("\n");

}

#endif

#ifdef POLICY_1
int memmag(struct tmop_cnt *opcnt, 
			struct perdomchart *domcharts, struct mm_hyp_cmd *rep) {


}

#endif
