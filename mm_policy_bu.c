#include <stdio.h>

#include "mm_policy.h"


#ifdef POLICY_STATIC
uint32_t memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep, 
			uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {

    return 1;
}
#endif

#ifdef POLICY_0
int memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep) {

    /* 
	The policy in this case is as follows.

	1. Look for the ratio of puts and gets individual to each domain on a per timestamp basis, 
	   for a WINDOW_SIZE.
	   This is to determine if a domain requires more tmem pages due to high demand.
	2. Compare the ratios of each domain. 
	3. Depending on certain values determine:
	    3.1. If more tmem pages will increase the number of reserved pages to a domain
	    3.2. If more physical memory will be allocated to a domain.
	4. Consider the amount of (t)mem pages it already has, the amount of memory in the system,
	   the amount of free memory, to determine how much (t) mem pages will be given to the domain.

	Very simple policy. For now focus on 3.2, and generate a hypercall to give more memory to the domain. 
    */
	// 1. Accumulate the put-to-get ratio of every domain
	check_command = 1;
	for (i=0; i < opcnt->nod_domcnt; i++) {
	    for (j=0; j < WINDOW_SIZE; j++) {
		
		if (domcharts[i].putall[j].y > 0 || domcharts[i].getall[j].y > 0) {
		    put_val = (float) domcharts[i].putall[j].y;
		    get_val = (float) domcharts[i].getall[j].y;

		    //printf("\t\tadding %2.4f\n", (put_val / (get_val + put_val)) );
		    domcharts[i].ptgtrt = domcharts[i].ptgtrt + (put_val / (put_val + get_val));
		}

		/*printf("\t\tdomain=%i, domcharts[%i].putall[%i].y=%lu, domcharts[%i].getall[%i].y=%lu, domcharts[%i].ptgtrt=%2.4f\n", 
			domcharts[i].dom_id, 
			i, j, domcharts[i].putall[j].y, 
			i, j, domcharts[i].getall[j].y,
			i, domcharts[i].ptgtrt );*/
	    }
	    domcharts[i].ptgtrt = domcharts[i].ptgtrt / WINDOW_SIZE;
	    acum = domcharts[i].ptgtrt + acum;
	}
	
	// 2. Average the ratio across the window_size, 
	// 3. find the maximum put-to-get ratio
	printf("\t\tentries=%i, opcnt->nod_domcnt=%i\n", entries, opcnt->nod_domcnt);
	for (j=0; j < opcnt->nod_domcnt; j++) {
	    domcharts[j].ptgtrt = domcharts[j].ptgtrt / acum;
	    if (max_ptgtrt < domcharts[j].ptgtrt) {
		max_ptgtrt = domcharts[j].ptgtrt;
		ind = j; 
	    }

	    // 4. Determine the amount of pages to reserve for each domain
	    domcharts[j].page_target = (uint64_t)(((float)opcnt->nod_freepag) * domcharts[j].ptgtrt);

	    // 5. Compare the amount of pages that should be reserved to the ones the domain already has, and determine
	    // if a hypercall is necessary.
	    domcharts[j].page_current = opcnt->dominfo[j].nod_mem + opcnt->dominfo[j].nod_tmem;
	    //if ( (domcharts[j].page_current < (domcharts[j].page_target - PAGE_THRESHOLD) ) 
		//|| (domcharts[j].page_current > (domcharts[j].page_target + PAGE_THRESHOLD) ) ) {
		rep[j].cmd = MM_RSVPAG;
		rep[j].dom_id = domcharts[j].dom_id;
		rep[j].page_target = domcharts[j].page_target;
		rep[j].page_current = domcharts[j].page_current;

		//sprintf(rep[j].buf, "%s=%i,%s=%lu,%s=%lu\0", 
		//		  "domain_id", domcharts[j].dom_id, 
		//		  "page_target", domcharts[j].page_target,
		//		  "page_current", domcharts[j].page_current);
	    //} else {
		//rep[j].cmd = MM_VOID;	    
	    //}
	
	    //printf("\t\tdomain=%i, current pages=%lu, target pages=%lu, total pages=%lu, free pages=%lu, percent=%1.5f\n", 
		//	domcharts[j].dom_id, 
		//	domcharts[j].page_current, 
		//	domcharts[j].page_target,
		//	opcnt->nod_totpag,
		//	opcnt->nod_freepag,
		//	domcharts[j].ptgtrt );
	}
	printf("\n");

}

#endif

#ifdef POLICY_1A
/*
   Always Yes policy, version A.

   This policy looks at the information coming from the stats (opcn, domcharts), and if the
   total list page is running out, it basically will request memory from a remote node.

   The addresses will be put into the total list, and then the hypervisor will move them from
   total list to the local/remote list for the domains to use.

   In short, the MM is reduced only to ask for remote memory when needed.
*/

uint8_t mm_req_mcap_sent=0;

		// number calculated
uint32_t memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep, 
			uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {
    int32_t rc=0, i=0;
    uint64_t amt2req=0, threshold=0;

    threshold = 1000;		// number might be calculated depending on the
				// information avaialble on the nodecharts. STatic for now

    #ifdef POLICY_1A_DEBUG
    printf("func %s, note: opcnt->total_list=%lu, threshold=%lu\n", 
		__func__, opcnt->total_list, threshold);
    #endif

    // Check the amount of pages in the total list
    if (opcnt->total_list < threshold && mm_req_mcap_sent==0) {
	amt2req = 2000;		// number might be calculated depending on the information available
				// on the nodecharts. Static for now
	#ifdef POLICY_1A_DEBUG
	printf("func %s, note: about to request, amt2req=%lu, opcnt->total_list=%lu, threshold=%lu\n", 
			__func__, amt2req, opcnt->total_list, threshold);
	#endif

	while (rc <= 0 && i < node_no) {
	    #ifdef POLICY_1A_DEBUG
	    printf("func %s, note: iterating, i=%i, this_node_id=%i\n", 
			__func__, i, this_node_id );
	    #endif

	    if (i != this_node_id){
		rc = mm_plain_cmd(MM_REQ_MCAP, ninf[i].region, amt2req, 
			this_node_id, ninf, node_no);
		mm_req_mcap_sent = 1;
	    }
	    i++;
	}
    } else if (opcnt->total_list >= threshold && mm_req_mcap_sent == 1) {
	mm_req_mcap_sent = 0;
    }
    // Check	
}
#endif


#ifdef POLICY_2A
/*
    Page reservation policy, version A. Constant (de)allocation using pages currently available.

    This policy looks at the information coming from the stats (opcn, domcharts), and reserves pages
    for every domain in order to reduce the worst case swap rate (put fail rate, prf). The 

    This is a basic version. It works as follows:
    1. Read the pfr of every domain
    2. If the pfr > 0.1, then give the domain an additional percentage of the available pages. (ALLOCATION_PERCENTAGE)
    3. If the pfr is near zero, take a certain amount of pages from the domain, check the gap between the current target
       and the pages the domain has.
       3.1. If the gap is above a certain threshold, then take away 
	    a small percentage of the pages already given to the domain. (DEALLOCATION_PERCENTAGE)
       3.2. If the gap is not above this threshold, keep the current target.

    4. Send to the HV.

    In this policy, all the pages returned by a domain are still reserved by that domain. That is, when a GET/FLUSH occurs,
    the domain does not suffer any changes on the pages it had reserved.

    When a PFR is less than zero, the domain will release pages at a constant rate.
    When the PFR is larger than zero, the domain will be allocated more pages according to priority-percentages.
    If the PFR is zero, the current utiliz
*/

#include <stdlib.h>

#define ALLOCATION_PERCENTAGE		15.0
#define DEALLOCATION_PERCENTAGE		5.0
#define THRESHOLD			500
 
uint32_t memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep, 
			uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {

    uint32_t i=0, rc = 0;
    uint64_t pages_available=0, gap=0;
    uint8_t neggap=0, send_data = 0;
    struct data_pervm *pfr_pervm;	// pfr: put fail rate (swap rate)
    struct data_pervm *vm_perc;		// vm_perc: constant percentages according to the number of domains

    if ( !(nodecharts->nodestats.size > 0 ))
	return 0;

    pages_available = nodecharts->nodestats.list_last->total_list + nodecharts->nodestats.list_last->local_list;
    pfr_pervm = (struct data_pervm *)malloc(nodecharts->domcnt*sizeof(struct data_pervm));
    vm_perc = (struct data_pervm *) malloc(nodecharts->domcnt*sizeof(struct data_pervm));

    rep->cmd = 0;
    for (i=0; i < nodecharts->domcnt; i++) {
	pfr_pervm[i].dom_id = nodecharts->domcharts[i].dom_id;
	vm_perc[i].dom_id = nodecharts->domcharts[i].dom_id;

	if (nodecharts->domcharts[i].domstats.size >= 1) {
	    pfr_pervm[i].x = nodecharts->domcharts[i].domstats.list_last->putfailrateinst;
	    pfr_pervm[i].y = nodecharts->domcharts[i].domstats.list_last->nod_tmem;
	    pfr_pervm[i].z = nodecharts->domcharts[i].domstats.list_last->mm_target;

	    #ifdef POLICY_2A_DEBUG
	    printf("func %s, note: nodecharts->domcharts[%i].domstats.list_last->, mm_target=%lu, nod_tmem=%lu, dom_id=%i, .putfailrateinst=%3.4f\n", 
			__func__, i, nodecharts->domcharts[i].domstats.list_last->mm_target, 
				     nodecharts->domcharts[i].domstats.list_last->nod_tmem,
				     nodecharts->domcharts[i].dom_id, 
				     nodecharts->domcharts[i].domstats.list_last->putfailrateinst);
	    #endif
	} else {
	    printf("func %s, note: nodecharts->domcharts[%i].domstats.size=%lu, not enough entries (less to 1)\n", 
			__func__, i, nodecharts->domcharts[i].domstats.size);
	}

	if (pfr_pervm[i].x > 0.1) {						// the positive
	    vm_perc[i].x = (ALLOCATION_PERCENTAGE/100.0)*pages_available;
	    vm_perc[i].z = pfr_pervm[i].z + (uint64_t)vm_perc[i].x;

	    /*if ( ((uint64_t)vm_perc[i].x) <= pages_available ) {
		pages_available = pages_available - (uint64_t)vm_perc[i].x;
	    } else {
		pages_available = 0;
	    }*/

	    #ifdef POLICY_2A_DEBUG
	    printf("func %s, note: pfr_pervm[i].x (pfr) > 0.1, vm_perc[%i], .x=%4.4f (increase), .z=%lu (new target), pages_available=%lu\n\n", 
			__func__, i, vm_perc[i].x, vm_perc[i].z, pages_available);
	    #endif

	    rep->cmd = MM_RSVPAG;
	    send_data = 1;
	} else if ( (pfr_pervm[i].x <= 0.1)  && (-0.1 <= pfr_pervm[i].x) ) {	// the zero
	    // This condition should be reached after detecting a decrease on the swap-rate
	    // But, because of resolution (floating point, time), the pfr might be zero too fast to be
	    // detected while still progressing, or might be a number to close to zero without being zero.

	    if (pfr_pervm[i].z > pfr_pervm[i].y ) {
		gap = pfr_pervm[i].z - pfr_pervm[i].y;
		neggap = 0;
		#ifdef POLICY_2A_DEBUG
		printf("func %s, note: pfr_pervm[i].x (pfr) is zero, pfr_pervm[%i], .z=%lu (mm_target), .y=%lu (nod_tmem), gap=%lu\n", 	
				__func__, i, pfr_pervm[i].z, pfr_pervm[i].y, gap);
		#endif
	    } else {
		gap = 0;
		neggap = 1;
		#ifdef POLICY_2A_DEBUG
		printf("func %s, note: pfr_pervm[i].x (pfr) is zero, gap=%lu, pfr_pervm[%i], .z = %lu (mm_target), .y=%lu (nod_tmem), .y large\n", 
				__func__, gap, i, pfr_pervm[i].z, pfr_pervm[i].y);
		#endif
	    }

	    if (gap > THRESHOLD && neggap == 0) {
		vm_perc[i].x = (DEALLOCATION_PERCENTAGE/100.0)*pfr_pervm[i].z;
		vm_perc[i].z = pfr_pervm[i].z - (uint64_t)vm_perc[i].x;

		pages_available = pages_available + ((uint64_t)vm_perc[i].x);
		rep->cmd = MM_RSVPAG;

		#ifdef POLICY_2A_DEBUG
		printf("func %s, note: pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i], .x = %3.4f (decrease), .z = %lu (new target), .y large, error\n\n", 
				__func__, pages_available, i, pfr_pervm[i].z, i, vm_perc[i].x, vm_perc[i].z);
		#endif
	    } else if (neggap == 1) {
		vm_perc[i].x = (ALLOCATION_PERCENTAGE/100)*pages_available;
		vm_perc[i].z = pfr_pervm[i].z + (uint64_t)vm_perc[i].x;

		#ifdef POLICY_2A_DEBUG
		printf("func %s, note: pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i], .x = %3.4f (increase), .z = %lu (new target), nod_tmem is larger, error\n\n", 
				__func__, pages_available, i, pfr_pervm[i].z, i, vm_perc[i].x, vm_perc[i].z);
		#endif
	    } else { // (gap <= THRESHOLD )
		vm_perc[i].z = nodecharts->domcharts[i].domstats.list_last->mm_target;

		#ifdef POLICY_2A_DEBUG
		printf("func %s, note: pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i].z = %lu (same target)\n\n", 
				__func__, pages_available, i, pfr_pervm[i].z, i, vm_perc[i].z);
		#endif
	    }
	  
	/*} else if (pfr_pervm[i].x < -0.1) {					// the negative, the put fail rate is never negative.
	    vm_perc[i].x = (DEALLOCATION_PERCENTAGE/100)*pfr_pervm[i].z;
	    vm_perc[i].z = pfr_pervm[i].z - (uint64_t)vm_perc[i].x;

	    pages_available = pages_available + ((uint64_t)vm_perc[i].x);
	    rep->cmd = MM_RSVPAG;

	    #ifdef POLICY_2A_DEBUG
	    printf("func %s, error: pages_available=%lu, vm_perc[%i], .z = %3.4f, .z = %lu, .y large, error\n", 
			__func__, pages_available, i, vm_perc[i].x, vm_perc[i].z);
	    #endif*/
	} else {		// pfr then must be flat zero. 
	    #ifdef POLICY_2A_DEBUG
	    printf("func %s, note: \n", __func__);
	    #endif
	    vm_perc[i].z = nodecharts->domcharts[i].domstats.list_last->mm_target;
	    #ifdef POLICY_2A_DEBUG
		printf("func %s, note: pfr is flat zero. pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i].z = %lu (same target)\n\n", 
				__func__, pages_available, i, pfr_pervm[i].z, i, vm_perc[i].z);
	    #endif
	}

	rep->m.pg_mng[i].cmd = MM_RSVPAG;
	rep->m.pg_mng[i].dom_id = vm_perc[i].dom_id;
	rep->m.pg_mng[i].mm_target = vm_perc[i].z;
    }

    // Send it to the hypervisor
    if ( rep->cmd != 0 && send_data == 1) {
	rc = mm_pagealloc_cmd(rep, MM_RSVPAG, this_node_id, ninf, node_no);
    }

    free(pfr_pervm);
    free(vm_perc);
}

#endif

#ifdef POLICY_2B
/*
    Page reservation policy, version A. Constant (de)allocation using visible tmem memory.

    This policy looks at the information coming from the stats (opcn, domcharts), and reserves pages
    for every domain in order to reduce the worst case swap rate (put fail rate, prf). The 

    This is a basic version. It works as follows:
    1. Read the pfr of every domain
    2. If the pfr > 0.1, then give the domain an additional percentage of the available pages. (ALLOCATION_PERCENTAGE)
    3. If the pfr is near zero, take a certain amount of pages from the domain, check the gap between the current target
       and the pages the domain has.
       3.1. If the gap is above a certain threshold, then take away 
	    a small percentage of the pages already given to the domain. (DEALLOCATION_PERCENTAGE)
       3.2. If the gap is not above this threshold, keep the current target.

    4. Send to the HV.

    In this policy, all the pages returned by a domain are still reserved by that domain. That is, when a GET/FLUSH occurs,
    the domain does not suffer any changes on the pages it had reserved.

    When a PFR is less than zero, the domain will release pages at a constant rate.
    When the PFR is larger than zero, the domain will be allocated more pages according to priority-percentages.
    If the PFR is zero, the current utiliz
*/

#include <stdlib.h>

#define ALLOCATION_PERCENTAGE		6.0
#define DEALLOCATION_PERCENTAGE		5.0
#define THRESHOLD			2500

uint32_t memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep, 
			uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {
    uint32_t i=0, rc = 0, continues=0;
    uint64_t pages_available=0, gap=0, sum_new_targets=0, residue;
    uint8_t neggap=0, send_data = 0;
    struct data_pervm *pfr_pervm;	// pfr: put fail rate (swap rate)
    struct sendinfo_pervm *vm_perc;	// vm_perc: constant percentages according to the number of domains

    if ( !(nodecharts->nodestats.size > 0 ))
	return 0;

    pages_available = ninf[this_node_id].num_pages;

    pfr_pervm = (struct data_pervm *) malloc(nodecharts->domcnt*sizeof(struct data_pervm));
    vm_perc = (struct sendinfo_pervm *) malloc(nodecharts->domcnt*sizeof(struct sendinfo_pervm));

    rep->cmd = 0;
    for (i=0; i < nodecharts->domcnt; i++) {
	pfr_pervm[i].dom_id = nodecharts->domcharts[i].dom_id;
	vm_perc[i].dom_id = nodecharts->domcharts[i].dom_id;

	if (nodecharts->domcharts[i].domstats.size >= 1) {
	    pfr_pervm[i].pfr_inst = nodecharts->domcharts[i].domstats.list_last->putfailrateinst;
	    pfr_pervm[i].tmem_used = nodecharts->domcharts[i].domstats.list_last->nod_tmem;
	    pfr_pervm[i].current_target = nodecharts->domcharts[i].domstats.list_last->mm_target;

	    #ifdef POLICY_2B_DEBUG
	    printf("func %s, note: nodecharts->domcharts[%i].domstats.list_last->, mm_target=%lu, nod_tmem=%lu, dom_id=%i, .putfailrateinst=%3.4f\n", 
			__func__, i, nodecharts->domcharts[i].domstats.list_last->mm_target, 
				     nodecharts->domcharts[i].domstats.list_last->nod_tmem,
				     nodecharts->domcharts[i].dom_id, 
				     nodecharts->domcharts[i].domstats.list_last->putfailrateinst);
	    #endif
	} else {
	    printf("func %s, note: nodecharts->domcharts[%i].domstats.size=%lu, not enough entries (less to 1)\n", 
			__func__, i, nodecharts->domcharts[i].domstats.size);
	}

	if (pfr_pervm[i].pfr_inst > 0.1) {						// the positive
	    vm_perc[i].flt_target = (ALLOCATION_PERCENTAGE/100.0)*pages_available;
	    vm_perc[i].new_target = pfr_pervm[i].current_target + (uint64_t)vm_perc[i].flt_target;

	    /*if ( ((uint64_t)vm_perc[i].x) <= pages_available ) {
		pages_available = pages_available - (uint64_t)vm_perc[i].x;
	    } else {
		pages_available = 0;
	    }*/

	    #ifdef POLICY_2B_DEBUG
	    printf("func %s, note: pfr_pervm[i].x (pfr) > 0.1, vm_perc[%i], .x=%4.4f (increase), .z=%lu (new target), pages_available=%lu\n\n", 
			__func__, i, vm_perc[i].flt_target, vm_perc[i].new_target, pages_available);
	    #endif

	    rep->cmd = MM_RSVPAG;
	    send_data = 1;
	} else if ( (pfr_pervm[i].pfr_inst <= 0.1)  && (-0.1 <= pfr_pervm[i].pfr_inst) ) {	// the zero
	    // This condition should be reached after detecting a decrease on the swap-rate
	    // But, because of resolution (floating point, time), the pfr might be zero too fast to be
	    // detected while still progressing, or might be a number to close to zero without being zero.

	    if (pfr_pervm[i].current_target > pfr_pervm[i].tmem_used ) {
		gap = pfr_pervm[i].current_target - pfr_pervm[i].tmem_used;
		neggap = 0;
		#ifdef POLICY_2B_DEBUG
		printf("func %s, note: pfr_pervm[i].x (pfr) is zero, pfr_pervm[%i], .z=%lu (mm_target), .y=%lu (nod_tmem), gap=%lu\n", 	
				__func__, i, pfr_pervm[i].current_target, pfr_pervm[i].tmem_used, gap);
		#endif
	    } else {
		gap = 0;
		neggap = 1;
		#ifdef POLICY_2B_DEBUG
		printf("func %s, note: pfr_pervm[i].x (pfr) is zero, gap=%lu, pfr_pervm[%i], .z = %lu (mm_target), .y=%lu (nod_tmem), .y large\n", 
				__func__, gap, i, pfr_pervm[i].current_target, pfr_pervm[i].tmem_used);
		#endif
	    }

	    if (gap > THRESHOLD && neggap == 0) {
		vm_perc[i].flt_target = (DEALLOCATION_PERCENTAGE/100.0)*pfr_pervm[i].current_target;
		vm_perc[i].new_target = pfr_pervm[i].current_target - (uint64_t)vm_perc[i].flt_target;

		//pages_available = pages_available + ((uint64_t)vm_perc[i].x);
		rep->cmd = MM_RSVPAG;

		#ifdef POLICY_2B_DEBUG
		printf("func %s, note: pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i], .x = %3.4f (decrease), .z = %lu (new target), .y large, error\n\n", 
				__func__, pages_available, i, 
				pfr_pervm[i].current_target, i, vm_perc[i].flt_target, vm_perc[i].new_target);
		#endif
	    } else if (neggap == 1) {
		vm_perc[i].flt_target = (ALLOCATION_PERCENTAGE/100)*pages_available;
		vm_perc[i].new_target = pfr_pervm[i].current_target + (uint64_t)vm_perc[i].flt_target;

		#ifdef POLICY_2B_DEBUG
		printf("func %s, note: pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i], .x = %3.4f (increase), .z = %lu (new target), nod_tmem is larger, error\n\n", 
				__func__, pages_available, i, 
				pfr_pervm[i].current_target, i, vm_perc[i].flt_target, vm_perc[i].new_target);
		#endif
	    } else { // (gap <= THRESHOLD )
		vm_perc[i].new_target = nodecharts->domcharts[i].domstats.list_last->mm_target;

		#ifdef POLICY_2B_DEBUG
		printf("func %s, note: pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i].z = %lu (same target)\n\n", 
				__func__, pages_available, i, pfr_pervm[i].current_target, i, vm_perc[i].new_target);
		#endif
	    }
	  
	/*} else if (pfr_pervm[i].x < -0.1) {					// the negative, the put fail rate is never negative.
	    vm_perc[i].x = (DEALLOCATION_PERCENTAGE/100)*pfr_pervm[i].z;
	    vm_perc[i].z = pfr_pervm[i].z - (uint64_t)vm_perc[i].x;

	    pages_available = pages_available + ((uint64_t)vm_perc[i].x);
	    rep->cmd = MM_RSVPAG;

	    #ifdef POLICY_2B_DEBUG
	    printf("func %s, error: pages_available=%lu, vm_perc[%i], .z = %3.4f, .z = %lu, .y large, error\n", 
			__func__, pages_available, i, vm_perc[i].x, vm_perc[i].z);
	    #endif*/
	} else {		// pfr then must be flat zero. 
	    #ifdef POLICY_2B_DEBUG
	    printf("func %s, note: \n", __func__);
	    #endif
	    vm_perc[i].new_target = nodecharts->domcharts[i].domstats.list_last->mm_target;
	    #ifdef POLICY_2B_DEBUG
		printf("func %s, note: pfr is flat zero. pages_available=%lu, pfr_pervm[%i].z=%lu (old target), vm_perc[%i].z = %lu (same target)\n\n", 
				__func__, pages_available, i, 
				pfr_pervm[i].current_target, i, vm_perc[i].new_target);
	    #endif
	}

	sum_new_targets = sum_new_targets + vm_perc[i].new_target;

	rep->m.pg_mng[i].cmd = MM_RSVPAG;
	rep->m.pg_mng[i].dom_id = vm_perc[i].dom_id;
	rep->m.pg_mng[i].mm_target = vm_perc[i].new_target;
    }

reduce_targets:
    if ( (send_data == 1) && (sum_new_targets > pages_available) && (continues < nodecharts->domcnt) ) {

	gap = (sum_new_targets - pages_available) / nodecharts->domcnt;
	residue = (sum_new_targets - pages_available) % (nodecharts->domcnt);

	#ifdef POLICY_2B_DEBUG
	printf("func %s, note: sum_new_targets=%lu, pages_available=%lu, gap=%lu, residue=%lu\n", 
		__func__, sum_new_targets, pages_available, gap, residue);	
	#endif

	for (i = 0; (i < nodecharts->domcnt) && (gap > 0) && (sum_new_targets >= gap); i++) {

	    if (rep->m.pg_mng[i].mm_target < gap) {
		continues = continues + 1;
		#ifdef POLICY_2B_DEBUG
		printf("func %s, note: sum_new_targets=%lu, pages_available=%lu, gap=%lu, residue=%lu, rep->m.pg_mng[%i].mm_target=%lu\n", 
				__func__, sum_new_targets, pages_available, gap, residue, i, rep->m.pg_mng[i].mm_target);
		#endif
		continue;
	    }
	    vm_perc[i].new_target = vm_perc[i].new_target - gap;
	    sum_new_targets = sum_new_targets - gap;
	    rep->m.pg_mng[i].mm_target = rep->m.pg_mng[i].mm_target - gap;
	}

	if (residue > 0 && (continues < nodecharts->domcnt)) {
	    i = 0;
	    continues = 0;
	    while (residue > 0 && (continues < nodecharts->domcnt) ) {

		if (rep->m.pg_mng[i].mm_target < 1) {
		    continues = continues + 1;
		    continue;
		}

		vm_perc[i].new_target = vm_perc[i].new_target - 1;
		sum_new_targets = sum_new_targets - 1;
		residue = residue - 1;
		rep->m.pg_mng[i].mm_target = rep->m.pg_mng[i].mm_target - 1;

		i = (i+1) % (nodecharts->domcnt);
	    }
	}

	goto reduce_targets;
    }

    // Send it to the hypervisor
    if ( rep->cmd != 0 && send_data == 1) {
	#ifdef POLICY_2B_DEBUG
	printf("func %s, note: about to send to HV, sum_new_targets=%lu, pages_available=%lu\n", 
		__func__, sum_new_targets, pages_available);
	#endif
	rc = mm_pagealloc_cmd(rep, MM_RSVPAG, this_node_id, ninf, node_no);
    }

    free(pfr_pervm);
    free(vm_perc);
}

#endif


#ifdef POLICY_2C
/*
    Page reservation policy, version B. Priority-based constant allocation.

    This policy looks at the information coming from the stats (opcn, domcharts), and reserves pages
    for every domain in order to reduce the worst case swap rate (put fail rate, prf). The 

    This is a basic version. It works as follows:
    0. Determine the percentages according to the amount of domains.
    1. Determine the pfr of every domain
    2. Tries to sort them
    3. If the pfr is bigger than zero, it will be given more pages from the available list
       The amount of pages given to every domain that has non-zero pfr is a constant
       percentage of the pages present in the total/local list.
    4. If the pfr is not zero, no additional pages will be allocated to the domain.
    5. Send the data to the HV.

    In this policy, all the pages returned by a domain are still reserved by that domain. That is, when a GET/FLUSH occurs,
    the domain does not suffer any changes on the pages it had reserved.

    When a PFR is less than zero, the domain will release pages at a constant rate.
    When the PFR is larger than zero, the domain will be allocated more pages according to priority-percentages.
    If the PFR is zero, the current utiliz
*/

#include <float.h>

#define MAX_PERCENTAGE		80
#define RELEASE_PERCENTAGE	15

uint8_t first_iter=0;
uint32_t memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep, 
			uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {
    uint32_t i=0, j=0, k=0;
    uint64_t temp64t=0;
    struct data_pervm *pfr_pervm;	// pfr: put fail rate (swap rate)
    struct data_pervm *vm_perc;		// vm_perc: constant percentages according to the number of domains
    struct data_pervm temp;
    float m=0.0, b=0.0, temp=0;

    pfr_pervm = (struct data_pervm *)malloc(nodecharts->domcnt*sizeof(struct data_pervm));
    vm_perc = (struct data_pervm *) malloc(nodecharts->domcnt*sizeof(struct data_pervm));
    //pfr_sorted = (struct pfr_sorted_pervm*)malloc(sizeof(struct data_pervm)*nodecharts->domcnt);


    // 0. Determine the percentages according to the amount of domains. Only necessary to do the first time
    // 1. Determine pfr for every domain
    for (i=0; i < nodecharts->domcnt; i++) {
	pfr_pervm[i].dom_id = nodecharts->domcharts[i].dom_id;

	if (nodecharts->domcharts[i].domstats.size > 1) {
	    pfr_pervm[i].x = nodecharts->domcharts[i].domstats.list_last->putfailrateinst;
	    pfr_pervm[i].y = nodecharts->domcharts[i].domstats.list_last->nod_tmem;
	    pfr_pervm[i].z = nodecharts->domcharts[i].domstats.list_last->mm_target;
	} else {
	    printf("func %s, note: nodecharts->domcharts[%i].domstats.size=%lu, not enough entries (2)\n", 
			__func__, i, nodecharts->domcharts[i].domstats.size);
	}

	m = 1/((nodecharts->domcnt)*(nodecharts->domcnt) - (nodecharts->domcnt)*(nodecharts->domcnt-1)/2);
	b = 2/(nodecharts->domcnt+1);
	vm_perc[i].x = b - i*m;
    }

    // 2. Sort the pfrs. The priority of allocation is based on this.
    i = 1;
    j = 2;
    while (i < nodecharts->domcnt) {

	if ( pfr_pervm[i-1] >= pfr_pervm[i] ) {
	    i = j;
	    j = j + 1;	
	} else {
	    temp.x = pfr_pervm[i].x;
	    temp.dom_id = pfr_pervm[i].dom_id;
	    
	    pfr_pervm[i].x = pfr_pervm[i-1].x;
	    pfr_pervm[i].dom_id = pfr_pervm[i-1].dom_id;
	    pfr_pervm[i-1].x = temp.x;
	    pfr_pervm[i-1].dom_id = temp.dom_id;

	    i = i - 1;
	    if (i == 0) {
	 	i = j;
		j = j + 1;
	    } 
	}
	
    }


    // 3. Check the total/local list (single node implementation), to see how many pages we have. 
    //    Together with this, we check the constant percentages, and assign the pages to the domain
    for (i=0; i < nodecharts->domcnt; i++) {
	if (pfr_pervm[i].x > 0.1) {
	    temp = (MAX_PERCENTAGE/100)*(nodecharts->nodestats.list_last->total_list + nodecharts->nodestats.list_last->local_list);
	    vm_perc[i].x = vm_perc[i].x*temp;					// new offset
	} else if (pfr_pervm[i].x < -0.1) {
	    vm_perc[i].x = -RELEASE_PERCENTAGE*((float)pfr_pervm[i].z);		// new offset
	} else {	// Case when almost zero. A problem here for floating point numbers if using exact inequalities
	    if (pfr_pervm[i].z > pfr_pervm[i].y) {
		temp64t = pfr_pervm[i].z - pfr_pervm[i].y;
	    } else {
		#ifdef POLICY_2A_DEBUG
		printf("func %s, warning: this value should be larger\n", __func__);
		#endif
		temp64t = 0;
	    }
	    vm_perc[i].x = pfr_pervm[i].z -RELEASE_PERCENTAGE((float)temp64t);
	}
	vm_perc[i].dom_id = pfr_pervm[i].dom_id;
    }


    // 4. Calculate the new target based on the current target and the current utilization of tmem.
    rep->cmd = MM_RSVPAG;
    for (i=0; i < nodecharts->domcnt; i++ ) {

	if (nodecharts->domcharts[i].dom_id)
	temp = 


	rep->m.pg_mng[j].cmd = MM_RSVPAG;
	rep->m.pg_mng[j].dom_id = domcharts[j].dom_id;
		rep[j].page_target = domcharts[j].page_target;
		rep[j].page_current = domcharts[j].page_current;
    }

    // 5. Send it to the HV.

    free(pfr_pervm);
    free(vm_perc);
    //free(pfr_sorted);
}
#endif


#ifdef POLICY_2C

/*
   Page reservation policy, version C.

   This policy looks at the information coming from the stats (opcn, domcharts), and reserves pages
   for every domain in order to reduce the worst case swap rate. The 

   We set a priority list, determine realloaction percentages for all domains simultaneously.
*/

#include <float.h>


uint32_t memmag(struct tmop_cnt *opcnt, 
			struct pernodechart *nodecharts, struct mm_hyp_cmd *rep, 
			uint32_t this_node_id, struct node_info *ninf, uint32_t node_no) {
    uint32_t i=0, j=0, temp=0;
    uint32_t *plist;
    struct data_pervm *prr_pervm;	// prr: page rate release, flush rate + get rate
    struct data_pervm *dpadt_pervm;  // slope of pages allocated to the domain vs. time
    struct data_pervm max_pfr;
    struct data_pervm min_pfr;

    max_pfr.dom_id =0;
    max_pfr.x = 0.0;
    min_pfr.dom_id = 0;
    min_pfr.x = FLT_MAX;

    prr_pervm = (struct data_pervm *)malloc(node_no*sizeof(struct data_pervm));
    dpadt_pervm = (struct data_pervm *)malloc(node_no*sizeof(struct data_pervm) );
    plist = (uint32_t*)malloc(node_no*sizeof(uint32_t));
    for (i=0; i < node_no; i++) {
	plist[i] = 0;
    }

    j=0;
    for (i=0; i < nodecharts->domcnt; i++) {
	prr_pervm[i].dom_id = nodecharts->domcharts[i].dom_id;
	dpadt_pervm[i].dom_id = nodecharts->domcharts[i].dom_id;

	if (nodecharts->domcharts[i].domstats.size > 1) {
	    prr_pervm[i].x =  nodecharts->domcharts[i].domstats.list_last->flushrateinst + 
				nodecharts->domcharts[i].domstats.list_last->getrateinst;
	    dpadt_pervm[i].x = prr_pervm[i].x - nodecharts->domcharts[i].domstats.list_last->putrateinst;

	} else {
	    printf("func %s, note: nodecharts->domcharts[%i].domstats.size=%lu, not enough entries (2)\n", 
			__func__, i, nodecharts->domcharts[i].domstats.size);
	}

	if (nodecharts->domcharts[i].domstats.list_last->putfailrateinst > max_pfr.x) {
	    max_pfr.x = nodecharts->domcharts[i].domstats.list_last->putfailrateinst;
	    max_pfr.dom_id = nodecharts->domcharts[i].dom_id;				// domain that will receive some pages
	}

	if (nodecharts->domcharts[i].domstats.list_last->putfailrateinst < min_pfr.x) {
	    min_pfr.x = nodecharts->domcharts[i].domstats.list_last->putfailrateinst;
	    min_pfr.dom_id = nodecharts->domcharts[i].dom_id;
	}

	plist[i] =

    }

    // Objective: Minimize max_pfr.x
    //    1. Check the utilization of other domains and their current allocation
    //		In this version, will only determine the order in which they are expected
    //		to run out of memory. This determines a priority. We define the percentages of
    //		pages that will be allocated to them.
    
    //    2. Determine how many pages can be re-allocated from the free lists
    //    3. Determine how many pages do we actually need to reduce the pfr of the offending domain
    //    2. If there are not enough pages, try to take pages from one of the domains.
    //        2.1. How many 


    // And if it cannnot be minimized,

    free(prr_pervm);
    free(dpadt_pervm);
    free(plist);
}
#endif
