#ifndef MM_POLICYFILE
#define MM_POLICYFILE

#include "node_info.h"
#include "mm_ctrl.h"

#define ENTRY_OFFSET_FOR_LISTSTAT	WINDOW_SIZE*0.20  // 20 percent of the enties
#define PAGES2RETURN_TO_TOTAL              2000
#define PAGES2RETURN_TO_LOCORRMT           2000
#define VARFACT                         4.5
#define TOTAL_LIST_THRSH                10000

// Policies for the single node
#define POLICY_LOCAL 

//#define POLICY_NONE	0
//#define POLICY_STATIC	1		// test policy impmlemented in 2015
//#define POLICY_0	2		// test policy impmlemented in 2015
//#define POLICY_1A	3		// Always yes, first version
//#define POLICY_1B	4		
//#define POLICY_2A	5		// Page reservation policy.
//#define POLICY_2B	6		// Page reservation policy.
//#define POLICY_2C	7		// Page reservation policy.
//#define POLICY_2D	7		// Page reservation policy.
//#define POLICY_3	8

//#define POLICY_4      9               // Page reservation policy, selfballoon+tmem, ballooning extended TLP
#define POLICY_5      10              // First Q-Learning Policy

// Policies for the remote node
//#define POLICY_RMT

//#define POLICY_NONE_RMT        100
//#define POLICY_STATIC_RMT    101
//#define POLICY_2B_RMT    102

#define FIX_PAGE_REQUEST       1500

// Debug Macros
//#define POLICY_NONE_DEBUG
//#define POLICY_2A_DEBUG
//#define POLICY_2B_DEBUG
//#define POLICY_3_DEBUG

//#define POLICY_2B_RMT_DEBUG


// Macros for deciding on which node
#define DEST_NODE_NONE           -1

uint32_t pagemoving_allocators_local(struct pernodechart *nodecharts, 
                                     uint32_t this_node_id, struct node_info *ninf, uint32_t node_no);

#ifdef POLICY_2A
struct data_pervm {
    uint32_t dom_id;
    float x;
    uint64_t y;
    uint64_t z;
};
#endif

#if defined(POLICY_2B) || defined(POLICY_2D) || defined(POLICY_3) || defined(POLICY_2B_RMT)
struct data_pervm {
    uint32_t dom_id;
    float pfr_inst;
    float pr_inst;
    uint64_t tmem_used;
    uint64_t current_target;
};

struct sendinfo_pervm {
    uint32_t dom_id;
    float flt_target;
    uint64_t new_target;
};
#endif

#if defined(POLICY_4)
struct data_pervm {
    uint32_t dom_id;
    float pfr_inst;
    float pr_inst;
    float fr_inst;
    float diff_prfr_inst;
    float oneprevdiff_prfr_inst;
    uint64_t mem_used;
    uint64_t tmem_used;

    uint64_t tmem_target;
    uint64_t balloon_target;
};

struct sendinfo_pervm {
    uint32_t dom_id;
    float tmem_flt_target;
    float balloon_flt_target;

    uint64_t tmem_target;
    uint64_t balloon_target;
};
#endif

#if defined(POLICY_5)
struct action_struct {
    uint32_t dom_id;
    uint64_t bmem;
    uint64_t tmem;
};

struct target {
    int32_t dom_id;
    uint64_t curr_target;

    int32_t allow_send;
    uint64_t time_elapsed_since_last_send;
};

#define MAXIMUM_TIMESTEPS_ELAPSED  5

struct pyrlact {
    struct action_struct vm_acts[MAX_DOMAINS];
};

#define MSKT_PYRLACT_SIZE		sizeof(struct pyrlact)
#define MSKT_CONNECTION_ACCEPT_ATTEMPTS 5
#endif



/* Debug Macros */
//#ifdef POLICY_1A_DEBUG

uint32_t memmag(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt,
 		struct pernodechart *nodecharts, 
		struct mm_hyp_cmd *rep, 
		uint32_t this_node_id, struct node_info *ninf, uint32_t node_no);



int32_t which_node2ask (struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, int32_t *id_array, 
                         uint32_t this_node_id, struct node_info *ninf, uint32_t node_no);

//#endif
#endif


