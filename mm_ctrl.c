#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "mm_ctrl.h"
#include "mm_comm.h"
#include "tmem_mod_if.h"
#include "mm_policy.h"

//#define TEST_SEQUENCE_4
uint32_t inact_count = 0;
static int time_size = 0;

int send_test_command() {
    int rc;
    struct mm_hyp_cmd rep;

    rep.cmd = MM_TEST;
    rc = tmif_send(&rep);
    if (!rc) {
	printf(" in %s, problem with tmif_send\n", __func__);
    }

    return rc;
}

int resp_test_command(struct tmop_cnt *opcnt) {
    int rc=0;

    rc = tmif_rcv_stats(opcnt);
    if (!rc) {
	printf("Problem receiving data from tmif_rcv, rc=%i\n", rc);
    }

    return rc;
}

// called in the following circumstances
//    1. Set up the global address list for control purposes (LIST_GRP_TOT).
//    2. Set up the local addresses for use of the local HV on tmem ops (LIST_GRP_LOCAL_LOCALADDR).
//    3. Set up the remote addresses for use of the local HV on tmem ops (LIST_GRP_LOCAL_RMTADDR).
//	 This occurs when a remote memory manager has returned an address list for the domain
//    4. Set up an address list that a remote domain is using (LIST_GRP_RMT).
int send_free_addr_list( uint32_t cmd,
			 uint8_t src_group, uint8_t dst_group,
			 struct cntg_blk_mm *addr_contg_blks,
			 uint64_t len ) {
    int i=0, j=0;
    uint32_t hyp_calls=0, last_batch=0, rc=0;
    uint64_t pg_batch_length = 0, it=0;
    struct cntg_blk_mm addr_blks[PG_BATCH_SIZE];
    struct mm_hyp_cmd rep;

    printf("in %s\n", __func__);

    hyp_calls = (len / PG_BATCH_SIZE) + 1;
    last_batch = len % PG_BATCH_SIZE;

    #ifdef MM_CTRL_DEBUG
    printf("\thyp_calls=%i, last_batch=%i\n", 
		hyp_calls, last_batch);
    #endif

    for (i=0; i < hyp_calls; i++) {

	if (i < hyp_calls - 1)
	    pg_batch_length = PG_BATCH_SIZE;
	else
	    pg_batch_length = last_batch;	

	#ifdef MM_CTRL_DEBUG
	printf("\ti=%i, pg_batch_length=%lu, addr_contg_blks[%lu], .base=%lu, .length=%lu\n", 
		i, pg_batch_length, it, 
		addr_contg_blks[it].base, 
		addr_contg_blks[it].length);
	#endif

	rep.cmd = cmd;
	rep.m.pglst_mng.src_group = src_group; 
	rep.m.pglst_mng.dst_group = dst_group;
	rep.m.pglst_mng.len = len;

	for (j=0; j < pg_batch_length; j++) {
	    rep.m.pglst_mng.addr_blks[j].base = addr_contg_blks[it].base;
	    rep.m.pglst_mng.addr_blks[j].length = addr_contg_blks[it].length;
	    it++;
	}

	#ifdef MM_CTRL_DEBUG
	printf("\t\trep.cmd=%i, rep.m.pglst_mng.src_group=%i, rep->m.pglst_mng.len=%lu\n",
		rep.cmd, rep.m.pglst_mng.src_group, rep.m.pglst_mng.len);
	for (j=0; j < len; j++) {
	    printf("\t\trep->m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n",
			j, rep.m.pglst_mng.addr_blks[j].base,
			rep.m.pglst_mng.addr_blks[j].length );
	}
	#endif

	#ifdef MM_CTRL_DEBUG
	printf("about to call tmif_send for MM_SETUP_TFAL\n");
	#endif
	rc = tmif_send(&rep);
	if (!rc) {
	    printf("problem with tmif_send\n");
	}
    }

    return S_MM_RELATED;
}


int32_t is_address_local( uint64_t address, struct node_info *ninf, uint32_t lnid) {
    uint32_t region_id;

    region_id = (uint32_t)( (address & 0xFF000000) >> (sizeof(uint64_t)*4 - 8) );

    return ninf[lnid].region == region_id;
}


int32_t address_region( uint64_t address, struct node_info *ninf, uint32_t lnid) {
    uint32_t region_id;

    region_id = (uint32_t)( (address & 0xFF000000) >> (sizeof(uint64_t)*4 - 8) );

    return region_id;
}

int move_addresses(uint32_t cmd, uint8_t src_group, uint8_t dst_group, uint64_t len) {
    int rc;
    struct mm_hyp_cmd rep;

    rep.cmd = cmd;
    rep.m.pglst_mng.src_group = src_group;
    rep.m.pglst_mng.dst_group = dst_group;
    rep.m.pglst_mng.len = len;

    rc = tmif_send(&rep);

    return rc;
}

int32_t mm_hyp_cmd_clear(struct mm_hyp_cmd * rep) {
    int j=0;

    rep->cmd = MM_VOID;
    for (j=0; j < MAX_DOMAINS; j++) {
	rep->m.pg_mng[j].cmd = 0;
	rep->m.pg_mng[j].dom_id = 0;
	rep->m.pg_mng[j].mm_target = 0;
    	rep->m.pg_mng[j].dn_target = 0;
	rep->m.pg_mng[j].dn_target_prev = 0;

	rep->m.pg_mng[j].mm_dn_slk = 0;
	rep->m.pg_mng[j].breached = 0;
	rep->m.pg_mng[j].reclaim_priority = 0;
	rep->m.pg_mng[j].retrieve_fraction = 0;

	rep->m.pg_mng[j].pages_freed = 0;
	rep->m.pg_mng[j].freed_threshold = 0;
    }

    rep->m.pglst_mng.src_group = 0;
    rep->m.pglst_mng.dst_group = 0;
    rep->m.pglst_mng.len = 0;
    for (j=0; j < PG_BATCH_SIZE; j++) {
	rep->m.pglst_mng.addr_blks[j].base = 0;
	rep->m.pglst_mng.addr_blks[j].length = 0;
    }

    return S_MM_RELATED;
}

int set_hyp_ninf(struct node_info *ninf, uint32_t this_node_id) {
    int i=0, rc=0;
    struct mm_hyp_cmd rep;
  
    printf("func %s, note: within\n", __func__);

    rep.cmd = MM_SETUP_HNINF;
    rep.m.pg_mng[0].dom_id = ninf[this_node_id].region;
    rep.m.pg_mng[0].mm_target = ninf[this_node_id].addr_strt;
    rep.m.pg_mng[0].dn_target = ninf[this_node_id].num_pages;
    rep.m.pg_mng[0].dn_target_prev = INIT_FRACTION_RESOLUTION;

    #ifdef POLICY_NONE
    rep.m.pg_mng[0].mm_dn_slk = POLICY_NONE;
    #endif
    #ifdef POLICY_STATIC
    rep.m.pg_mng[0].mm_dn_slk = POLICY_STATIC;
    #endif
    #ifdef POLICY_0
    rep.m.pg_mng[0].mm_dn_slk = POLICY_0;
    #endif
    #ifdef POLICY_1A
    rep.m.pg_mng[0].mm_dn_slk = POLICY_1A;
    #endif
    #ifdef POLICY_1B
    rep.m.pg_mng[0].mm_dn_slk = POLICY_1B;
    #endif
    #ifdef POLICY_2A
    printf("func %s, note: sending POLICY_2A\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = POLICY_2A;
    #endif
    #ifdef POLICY_2B
    printf("func %s, note: sending POLICY_2B\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = 5;	// The same value as Policy2A, same thing as far as HV is concerned
    #endif
    #ifdef POLICY_2D
    printf("func %s, note: sending POLICY_2D\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = 5;	// The same value as Policy2A, same thing as far as HV is concerned
    #endif
    #ifdef POLICY_3
    printf("func %s, note: sending POLICY_3\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = 5;	// The same value as Policy2A, same thing as far as HV is concerned
    #endif
    #ifdef POLICY_5
    printf("func %s, note: sending POLICY_5\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = 5;      // The same value as Policy2A, same thing as far as HV is concerned
    #endif

    #ifdef POLICY_NONE_RMT
    printf("func %s, note: sending POLICY_NONE_RMT\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = 0;
    #endif
    #ifdef POLICY_2B_RMT
    printf("func %s, note: sending POLICY_2B_RMT\n", __func__);
    rep.m.pg_mng[0].mm_dn_slk = 5;
    #endif
    rep.m.pg_mng[0].breached = MM_INTERRUPTS_STATS_ENABLED;

    #ifdef POLICY_3
    rep.m.pg_mng[0].reclaim_priority = 0;
    #else
    rep.m.pg_mng[0].reclaim_priority = ninf[this_node_id].local_init_pages/3;//MAX_DOMAINS;
    #endif

    #ifdef POLICY_NONE
    rep.m.pg_mng[0].reclaim_priority = ninf[this_node_id].local_init_pages;//MAX_DOMAINS;
    #endif

    #ifdef POLICY_NONE_RMT
    rep.m.pg_mng[0].reclaim_priority = ninf[this_node_id].local_init_pages/3;//MAX_DOMAINS;
    #endif

    


    rep.m.pg_mng[0].pages_freed = REMOTE_ACCESS_LATENCY;

    rc = tmif_send(&rep);

    //printf("back in %s, fd=%i\n", __func__, fd);
    printf("\tafter returning from tmif_send\n");
    return SUCC_SET_HNINF;
}

int set_total_free_list(struct node_info *ninf, uint32_t nid ) {
    int i=0, rc=0;
    struct cntg_blk_mm tmp_cntg_blk;

    printf("in %s\n", __func__);
    tmp_cntg_blk.base = ninf[nid].addr_strt;
    tmp_cntg_blk.length = ninf[nid].num_pages;

    send_free_addr_list(MM_SETUP_TFAL, LIST_GRP_TOTAL, LIST_GRP_TOTAL, &tmp_cntg_blk, 1);

    return 0;
}

int set_local_free_list(struct node_info *ninf, uint32_t nid ) {
    int rc=0;

    printf("in %s\n", __func__);

    rc = move_addresses(MM_SETUP_LFAL, LIST_GRP_TOTAL, 
				LIST_GRP_LOCAL_LOCALADDR, ninf[nid].local_init_pages);

    return 0;
}


int move_total_to_local(uint64_t amount) {
    int rc=0;

    printf("in %s\n", __func__);

    rc = move_addresses(MM_TFAL_TO_LFAL, LIST_GRP_TOTAL, 
				LIST_GRP_LOCAL_LOCALADDR, amount);

    return rc;
}

int move_local_to_total(uint64_t amount) {
    int rc=0;

    printf("in %s\n", __func__);

    rc = move_addresses(MM_LFAL_TO_TFAL, LIST_GRP_LOCAL_LOCALADDR, 
				LIST_GRP_TOTAL, amount);

    return rc;
}


int move_total_to_remote(uint64_t amount) {
    int rc=0;

    printf("in %s\n", __func__);

    rc = move_addresses(MM_TFAL_TO_RFAL, LIST_GRP_TOTAL, 
				LIST_GRP_LOCAL_RMTADDR, amount);

    return rc;
}

int move_remote_to_total(uint64_t amount) {
    int rc=0;

    printf("in %s\n", __func__);

    rc = move_addresses(MM_RFAL_TO_TFAL, LIST_GRP_LOCAL_RMTADDR, 
				LIST_GRP_TOTAL, amount);

    return rc;
}

int move_localOrRemote_to_total( uint64_t amount ) {
    int rc=0;
          
    printf("func %s, note: amount=%lu\n", __func__, amount);
    
    rc = move_addresses(MM_LORRFAL_TO_TFAL, LIST_GRP_LOCAL_RMTADDR,
                            LIST_GRP_TOTAL, amount);    
    return rc;
}

int move_total_to_localOrRemote( uint64_t amount ) {
    int rc=0;
          
    printf("func %s, note: amount=%lu\n", __func__, amount);
    
    rc = move_addresses(MM_TFAL_TO_LORRFAL, LIST_GRP_TOTAL,
                            LIST_GRP_LOCAL_RMTADDR, amount);    
    return rc;
}


int32_t mm_req_mcap(struct mm_hyp_cmd *rep, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                      uint64_t * entries, uint32_t this_region_id, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf,
                      uint64_t amount) {
    int i, rc;
	
    rep->cmd = MM_REQ_MCAP;
    rep->m.pglst_mng.src_group = LIST_GRP_TOTAL;
    rep->m.pglst_mng.dst_group = HASH_GRP_RMT;
    rep->m.pglst_mng.len = amount;
    //#ifdef MM_REQ_MCAP_DEBUG
    //rep->m.pglst_mng.region_id = 20;	// for testing. This is suppose to come from somewhere else.
    //#endif

    for (i=0; i < PG_BATCH_SIZE; i++) {
	rep->m.pglst_mng.addr_blks[i].base = 0;
	rep->m.pglst_mng.addr_blks[i].length = 0;
    }

    rc = tmif_send(rep);
    #ifdef MM_REQ_MCAP_DEBUG
    printf("\tfunc %s, note: after returning from tmif_send\n", __func__);
    #endif

    rc = tmif_rcv_mcap(rep, opcnt, rmopcnt, nodecharts, entries, this_region_id, node_no, this_node_id, ninf);
    #ifdef MM_REQ_MCAP_DEBUG	
    printf("\trep->m.pglst_mng.len=%lu\n", rep->m.pglst_mng.len);
    for (i=0; i < rep->m.pglst_mng.len; i++) {
	printf("\t\trep->m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n", i, 
			rep->m.pglst_mng.addr_blks[i].base, 
			rep->m.pglst_mng.addr_blks[i].length);
    }
    #endif

    return SUCC_REQ_MCAP;
}

int32_t mm_rcv_mcap(struct mm_hyp_cmd *rep) {
    int i, rc, j;

    rep->cmd = MM_RCV_MCAP;
    rep->m.pglst_mng.src_group = RMT_REGION;
    rep->m.pglst_mng.dst_group = LIST_GRP_LOCAL_RMTADDR;

    #ifdef MM_RCV_MCAP_DEBUG_ZERO
    printf("func %s, note: modifying values for debugging\n", __func__);
    for (i=0; i < rep->m.pglst_mng.len; i++) {
	uint64_t base = rep->m.pglst_mng.addr_blks[i].base;
	for (j=0; j < rep->m.pglst_mng.addr_blks[i].length; j++) {
	    rep->m.pglst_mng.addr_blks[i].base = base + 0x01000000;
	}

	if (i==0) {
	    printf("\trep->m.pglst_mng.addr_blks[%i].base=%lu\n", 
			i, rep->m.pglst_mng.addr_blks[i].base);
	}
    }
    #endif

    #ifdef MM_RCV_MCAP_DEBUG_ONE 
    printf("func %s, note: checking values coming from remote node\n", __func__);
    for (i=0; i < rep->m.pglst_mng.len; i++) {
        printf("\t\trep->m.pglst_mng.addr_blks[%i].base=%lu, .length=%lu\n", 
                   i, rep->m.pglst_mng.addr_blks[i].base, rep->m.pglst_mng.addr_blks[i].length);
    }
    #endif

    rc = tmif_send(rep);
    printf("\tfunc %s, note: after returning from tmif_send\n", __func__);

    return SUCC_RCV_MCAP;
}


int32_t mm_relinq_mcap(struct mm_hyp_cmd *rep, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                            uint64_t * entries, uint32_t this_region_id, uint64_t amount, 
                            uint32_t lnid, struct node_info *ninf, uint32_t node_no ) {
    int rc;
    uint64_t relinq_amount=0;
    #ifdef MM_RELINQ_MCAP_DEBUG
    uint32_t i,j;
    uint64_t add_count;
    #endif

    rc = mm_hyp_cmd_clear(rep);

    rep->cmd = MM_RELINQ_MCAP;
    rep->m.pglst_mng.len = amount;
    rep->m.pglst_mng.region_id = ninf[lnid].region;
    rep->m.pglst_mng.src_group = LIST_GRP_TOTAL_RMTADDR;
    rep->m.pglst_mng.dst_group = LIST_GRP_NONE_AWAY;

    rc = tmif_send(rep);
    #ifdef MM_RELINQ_MCAP_DEBUG
    printf("\tfunc %s, note: after returning from tmif_send\n", __func__);
    #endif

    rc = tmif_rcv_mcap(rep, opcnt, rmopcnt, nodecharts, entries, this_region_id, node_no, lnid, ninf);
    #ifdef MM_RELINQ_MCAP_DEBUG
    for (i=0; i < rep->m.pglst_mng.len; i++) {	
	relinq_amount += rep->m.pglst_mng.addr_blks[i].length;
    }

    if (relinq_amount != amount) {
	printf("func %s, warning: amount=%lu, relinq_amount=%lu, not equal\n", 
			__func__, amount, relinq_amount);

	printf("Batches in the address list\n");
	for (i=0; i < rep->m.pglst_mng.len; i++) {
	    printf("batch: %i\n", i);
	    printf("\tmmcmd.m.pglst_mng.addr_blks[%i].base=%lx\n", i, rep->m.pglst_mng.addr_blks[i].base);
	    printf("\tmmcmd.m.pglst_mng.addr_blks[%i].length=%lu\n", i, rep->m.pglst_mng.addr_blks[i].length);
	    add_count += rep->m.pglst_mng.addr_blks[i].length;
	}
	printf("add_count=%lu\n", add_count);
    }  
    #endif

    return SUCC_RELINQ_MCAP;
    
}

int32_t mm_relack_mcap(struct mm_hyp_cmd *rep) {
    int rc, i, j;
 
    if (rep->m.pglst_mng.len <= 0) {
	#ifdef MM_RELACK_MCAP_DEBUG
	printf("func %s, warning: there are no pages to relack. hypcall senseless\n", 
			__func__);
	#endif
	return SUCC_RELACK_MCAP;
    }

    if (rep->cmd != MM_RELACK_MCAP) {
	#ifdef MM_RELACK_MCAP_DEBUG
	printf("func %s, error: wrong rep->cmd=%u, should be MM_RELACK_MCAP=%u\n", 
		    __func__, rep->cmd, MM_RELACK_MCAP);
	#endif
	rep->cmd = MM_RELACK_MCAP;
    }

    //#ifdef MM_RELACK_MCAP_DEBUG
    //for (i=0; i < rep->m.pglst_mng.len; i++) {
	//uint64_t base = rep->m.pglst_mng.addr_blks[i].base;
	//for (j=0; j < rep->m.pglst_mng.addr_blks[i].length; j++) {
	    //rep->m.pglst_mng.addr_blks[i].base = base - 0x01000000;
	//}
    //}
    //#endif
    
    rc = tmif_send(rep);
    #ifdef MM_RELACK_MCAP_DEBUG
    printf("\tfunction %s, note: after returning from tmif_send\n", __func__);
    #endif

    return SUCC_RELACK_MCAP;
}

int32_t mm_reclaim_mcap(struct mm_hyp_cmd *rep, struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts,
                         uint64_t * entries, uint32_t this_region_id, uint32_t node_no, 
                         uint32_t this_node_id, struct node_info *ninf, uint64_t amount, uint32_t region_id) {
    int rc;
    uint64_t reclaim_amount=0;
    #ifdef MM_RECLAIM_MCAP_DEBUG
    uint32_t i,j;
    #endif

    rep->cmd = MM_RECLAIM_MCAP;
    rep->m.pglst_mng.len = amount;
    rep->m.pglst_mng.region_id = region_id;
    rep->m.pglst_mng.src_group = LIST_GRP_TOTAL_RMTADDR;
    rep->m.pglst_mng.dst_group = LIST_GRP_NONE_AWAY;

    rc = tmif_send(rep);
    #ifdef MM_RECLAIM_MCAP_DEBUG
    printf("\tfunction %s, note: after returning from tmif_send\n", __func__);
    #endif

    rc = tmif_rcv_mcap(rep, opcnt, rmopcnt, nodecharts, entries, this_region_id, node_no, this_node_id, ninf);
    #ifdef MM_RECLAIM_MCAP_DEBUG
    for (i=0; i < rep->m.pglst_mng.len; i++) {
	reclaim_amount += rep->m.pglst_mng.addr_blks[i].length;
    }

    if (reclaim_amount != amount) {
	printf("\tfunction %s, warning: amount=%lu, reclaim_amount=%lu, not equal, rep->m.pglst_mng.len=%lu\n", 
			__func__, amount, reclaim_amount, rep->m.pglst_mng.len);
    } 
    #endif

    return SUCC_RECLAIM_MCAP;
}

int mm_reclack_mcap(struct mm_hyp_cmd *rep) {
    int rc, i=0, j=0;

    if (rep->m.pglst_mng.len <= 0) {
	#ifdef MM_RECLACK_MCAP_DEBUG
	printf("func %s, warning: there are no pages to relack. hypcall senseless\n", 
			__func__);
	#endif
	return SUCC_RECLACK_MCAP;
    }

    if (rep->cmd != MM_RECLACK_MCAP) {
	#ifdef MM_RECLACK_MCAP_DEBUG
	printf("func %s, error: wrong rep->cmd=%u, should be MM_RECLACK_MCAP=%u\n", 
		    __func__, rep->cmd, MM_RECLACK_MCAP);
	#endif
	rep->cmd = MM_RECLACK_MCAP;
    }

    #ifdef MM_RECLACK_MCAP_DEBUG
    for (i=0; i < rep->m.pglst_mng.len; i++) {
	uint64_t base = rep->m.pglst_mng.addr_blks[i].base;
	for (j=0; j < rep->m.pglst_mng.addr_blks[i].length; j++) {
	    rep->m.pglst_mng.addr_blks[i].base = base - 0x01000000;
	}
    } 
    #endif

    rc = tmif_send(rep);
    #ifdef MM_RECLACK_MCAP_DEBUG
    printf("\tfunc %s, note: after returning from tmif_send\n", __func__);
    #endif

    return SUCC_RECLACK_MCAP;
}


int32_t mm_pagealloc_cmd(struct mm_hyp_cmd *rep, uint32_t cmd, 
				uint32_t lnid, struct node_info *ninf, uint32_t node_no) {

    int32_t rc, i=0;


    rc = tmif_send(rep);


    return rc;
}




/* Commands for remote MMs under single master-multiple node implementation */
/* Functions to test the socket functionality */
int32_t mm_fn2n_test_cmd( uint32_t src_region_id, uint32_t src_node_id, 
			struct node_info *ninf, uint32_t node_no) {
    struct mm_hyp_cmd mmcmd;
    struct mm_pglst_rmt mmplcmd;
    uint32_t i=0, rm_node_id, rm_region_id;
    int32_t rc;

    rc = mm_hyp_cmd_clear(&mmcmd);
    rc = pglst_rmt_clear(&mmplcmd);

    for (i=0; i < node_no; i++) {
	if (i != src_node_id) {
	    mmcmd.cmd = MM_TEST_SEND;

	    mmcmd.m.pglst_mng.src_group = RMT_REGION;
	    mmcmd.m.pglst_mng.dst_group = LIST_GRP_TOTAL;
	    mmcmd.m.pglst_mng.len = MM_TEST_VALUE_0;
    	    mmcmd.m.pglst_mng.region_id = ninf[i].region;

	    rc = mmskt_send( &mmcmd, NULL, NULL, 0, src_node_id, ninf, node_no);
	    
	    if (rc != sizeof(struct mm_rmtcmd)) {
		printf("\tfunc %s, error: cannot send to ninf[%i].region=%i, rc=%i, sizeof(struct mm_rmtcmd)=%lu\n", 
				__func__, i, ninf[i].region, rc, sizeof(struct mm_rmtcmd) );
	    }
	}
    }

    return rc; 
}

int32_t mm_fn2n_testresp_cmd( uint32_t src_region, uint32_t dst_region, uint32_t trn_id,
				uint32_t lnid, struct node_info *ninf, uint32_t node_no) {
    struct mm_hyp_cmd mmcmd;
    uint32_t i=0, rm_node_id, rm_region_id;
    int32_t rc;

    rc = mm_hyp_cmd_clear(&mmcmd);

    mmcmd.cmd = MM_TEST_RESP;

    mmcmd.m.pglst_mng.src_group = RMT_REGION;
    mmcmd.m.pglst_mng.dst_group = LIST_GRP_TOTAL;
    mmcmd.m.pglst_mng.len = MM_TESTRESP_LEN;
    mmcmd.m.pglst_mng.region_id = dst_region;

    for (i=0; i < mmcmd.m.pglst_mng.len; i++) {
	mmcmd.m.pglst_mng.addr_blks[i].base = i*i;
	mmcmd.m.pglst_mng.addr_blks[i].length = i*(i-i/2);
    }

    rc = mmskt_send( &mmcmd, NULL, NULL, trn_id, lnid, ninf, node_no);
	    
    if (rc != sizeof(struct mm_pglst_rmt)) {
	printf("\tfunc %s, error: cannot send to dst_region=%i, rc=%i, sizeof(struct mm_pglst_rmt)=%lu\n", 
		     __func__, dst_region, rc, sizeof(struct mm_pglst_rmt) );
    }

    return rc; 
}

int32_t mm_fn2m_req_mcap(uint64_t amount, uint32_t this_region_id, uint32_t this_node_id, 
				struct node_info *ninf, uint32_t node_no) {
    struct mm_hyp_cmd mmcmd;
    int32_t rc, i;

    rc = mm_hyp_cmd_clear(&mmcmd);

    for (i=0; i < node_no; i++) {
	if (ninf[i].master == 1) {
	    mmcmd.m.pglst_mng.region_id = ninf[i].region;
	}
    }

    if (mmcmd.m.pglst_mng.region_id == 0) {
	#ifdef MM_FN2M_REQ_MCAP_DEBUG
	printf("func %s, error: no master present, can't send this command\n", __func__);
	#endif
	return ERR_FN2M_REQMCAP_NO_MASTER;
    }

    if (mmcmd.m.pglst_mng.region_id == this_region_id) {
	#ifdef MM_FN2M_REQ_MCAP_DEBUG
	printf("func %s, warning: this is master, error in usage\n", __func__);
	#endif
	return ERR_FN2M_REQMCAP_MASTER_ME;
    }

    mmcmd.cmd = MM_FN2M_REQ_MCAP;
    mmcmd.m.pglst_mng.len = amount;

    //rc = mmskt_send( &mmcmd, NULL, NULL, 0, this_node_id, ninf, node_no);

    rc = mm_hyp_cmd_clear(&mmcmd);
	
    
}

int32_t mm_plain_cmd(uint32_t cmd, uint32_t dst_region, uint64_t len, 
			uint32_t lnid, struct node_info *ninf, uint32_t node_no) {

    uint32_t rc=0;
    struct mm_hyp_cmd mmcmd;

    #ifdef MM_PLAIN_CMD_DEBUG
    printf("func %s, note: in here\n", __func__);
    #endif

    if (ninf[lnid].region == dst_region) {
	printf("func %s, note: dst_region=%i, ninf[%i].region=%i, can't send\n", 
			__func__, dst_region, lnid, ninf[lnid].region);
	return ERR_CANT_SEND_TO_SELF;
    }

    rc = mm_hyp_cmd_clear(&mmcmd);

    mmcmd.cmd = cmd;
    mmcmd.m.pglst_mng.len = len;
    mmcmd.m.pglst_mng.region_id = dst_region;

    #ifdef MM_PLAIN_CMD_DEBUG
    printf("func %s, note: at rc = mmskt_send( &mmcmd, NULL, NULL, 0, lnid, ninf, node_no);\n", __func__);
    #endif
    rc = mmskt_send( &mmcmd, NULL, NULL, 0, lnid, ninf, node_no);

    if (rc != sizeof(struct mm_rmtcmd) ) {
	printf("func %s, error: bytes sent rc=%i, sizeof(struct mm_rmtcmd)=%lu, not equal\n", 
			__func__, rc, sizeof(struct mm_rmtcmd));
	rc = ERR_MM_PLAIN_CMD_CANT_SEND;
    } else {
	rc = SUCC_MM_PLAIN_CMD_CANT_SEND;
    }   

    return rc;
}

int32_t mm_pglst_cmd(struct mm_hyp_cmd *mmcmd, uint32_t cmd, 
			uint32_t lnid, struct node_info *ninf, uint32_t node_no) {

    uint32_t rc=0;

    if (ninf[lnid].region == mmcmd->m.pglst_mng.region_id) {
	printf("func %s, note: mmcmd->m.pglst_mng.region_id=%i, ninf[%i].region=%i, can't send\n", 
			__func__, mmcmd->m.pglst_mng.region_id, lnid, ninf[lnid].region);
	return ERR_CANT_SEND_TO_SELF;
    }

    mmcmd->cmd = cmd;
    rc = mmskt_send( mmcmd, NULL, NULL, 0, lnid, ninf, node_no);

    if (rc != sizeof(struct mm_pglst_rmt) ) {
	printf("func %s, error: bytes sent rc=%i, sizeof(struct mm_pglst_rmt)=%lu, not equal\n", 
			__func__, rc, sizeof(struct mm_pglst_rmt));
	rc = ERR_MM_PGLST_CMD_CANT_SEND;
    } else {
	rc = SUCC_MM_PGLST_SEND;
    }   

    return rc;
}


int32_t mm_stats_cmd (struct tmop_cnt* opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t cmd, uint32_t src_region, 
			uint32_t dst_region, uint32_t lnid, struct node_info *ninf, uint32_t node_no) {
    int32_t rc=0;
    struct mm_hyp_cmd mmcmd;

    rc = mm_hyp_cmd_clear(&mmcmd);

    mmcmd.cmd = cmd;
    mmcmd.m.pglst_mng.region_id = dst_region;
    #ifdef TEST_SEQUENCE_4
    mmcmd.m.pglst_mng.len = MM_TEST_VALUE_1;
    #endif

    rc = mmskt_send( &mmcmd, opcnt, rmopcnt, 0, lnid, ninf, node_no);

    switch(cmd) {
	case MM_SND_STATS:
	    if (rc != sizeof(struct mm_stat_rmt)) {
		printf("\tfunc %s, error: cannot send to dst_region=%i, rc=%i, sizeof(struct mm_stat_rmt)=%lu\n", 
				__func__, dst_region, rc, sizeof(struct mm_stat_rmt) );
	    }
	break;
	case MM_REQ_STATS:
	    if (rc != sizeof(struct mm_rmtcmd)) {
		printf("\tfunc %s, error: cannot send to dst_region=%i, rc=%i, sizeof(struct mm_rmtcmd)=%lu\n", 
				__func__, dst_region, rc, sizeof(struct mm_rmtcmd) );
	    }
	break;
	default:
	    printf("func %s, error: cmd=%i, not registered\n", __func__, cmd);
	break;
    }
    return rc;
}


int32_t mm_cmd_rcv( struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                      uint64_t * entries, uint32_t this_region_id, uint32_t node_no, uint32_t lnid, struct node_info *ninf) {
    int32_t rc=0, errors=0, i=0, j=0, k=0, tind=-1, rind=-1, node_count=0, 
            dest_node_id=0, source_nid=0, rmopcnt_id=0, rmtcmd_ind = 0, rc_grph=0;
    int32_t *id_array = NULL;
    uint64_t pages_received = 0, pages_in_remote_dest = 0, lists_in_remote_dest = 0, pagesLeft=0;
    uint8_t cmd_type = 0;
    struct mm_rmtcmd rmtcmd; 
    struct mm_pglst_rmt pglst_cmdrmt;
    struct mm_stat_rmt stat_rmt;

    struct mm_hyp_cmd rep;

    #ifdef MM_CMD_RCV_DEBUG
    //printf("func %s, note: in this function\n", __func__);
    #endif
    rc = mm_hyp_cmd_clear(&rep);

    rc = mmskt_rcv(&cmd_type, &rmtcmd, &pglst_cmdrmt, &stat_rmt, lnid, ninf, node_no);
    #ifdef MM_CMD_RCV_DEBUG
    //printf("func %s, note: rc=%i\n", __func__, rc);
    #endif
    //if (rc <= 30) {
	//#ifdef MM_CMD_RCV_DEBUG
	//printf("func %s, note: rc=%i <= 30, about to return\n", __func__, rc);
	//#endif
	//return 1;
    //}

    if (rc == ERR_MMSKT_RCV_READHDRSKT) {
	#ifdef MM_CMD_RCV_DEBUG
	printf("func %s, error: ERR_MMSKT_RCV_READHDRSKT, cannot read header\n", __func__);
	#endif
	return rc;
    }

    if (rc == NEUT_NO_ACTIVITY) {
	#ifdef MM_CMD_RCV_DEBUG
	printf("func %s, note: no activity detected in sockets\n", __func__);
	#endif
	return 1;
    }

    if ( ninf[lnid].master == 1) {
        for (i=0; i < node_no; i++) {
            if (ninf[lnid].region == rmopcnt[i].region_id) {
                rmopcnt_id = i;
                break;
            } else {
                rmopcnt_id = 0;
            }
        }
    }

    switch(cmd_type) {
	case MM_CMD_PLAIN:
	    #ifdef MM_CMD_RCV_DEBUG
	    printf("func %s, note: command type received, MM_CMD_PLAIN\n", __func__);
	    #endif

	    switch(rmtcmd.cmd) {
		case MM_TEST_SEND:
		    rc = mm_fn2n_testresp_cmd( rmtcmd.dst_region, rmtcmd.src_region, 
						rmtcmd.trn_id, lnid, ninf, node_no);
		break;
		case MM_REQ_MCAP:

		    // This command: a node requests memory capacity
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_REQ_MCAP, from rmtcmd.src_region=%i\n", __func__, rmtcmd.src_region);
		    #endif

		    // 1. Fetch the required amount in the hypervisor
		    rep.m.pglst_mng.region_id = rmtcmd.src_region;
		    rc = mm_req_mcap(&rep, &opcnt[0], rmopcnt, nodecharts, entries, this_region_id, node_no, lnid, ninf, rmtcmd.len);
		
		    // 2. Send the list to the other mm if rep.m.pglst_mng.len
                    rmopcnt[rmopcnt_id].req_mcap_in = rmopcnt[rmopcnt_id].req_mcap_in + 1;
                    rmopcnt[rmopcnt_id].req_mcap_inpagreq = rmopcnt[rmopcnt_id].req_mcap_inpagreq + rmtcmd.len;
                    for (i = 0; i < MAX_RMTNODES; i++) {
                        if (rmtcmd.src_region == rmopcnt[rmopcnt_id].nodinfo[i].region_id) {
                            rmtcmd_ind = i;
                            rmopcnt[rmopcnt_id].nodinfo[i].req_mcap_in = rmopcnt[rmopcnt_id].nodinfo[i].req_mcap_in + 1;
                            rmopcnt[rmopcnt_id].nodinfo[i].req_mcap_inpagreq = rmopcnt[rmopcnt_id].nodinfo[i].req_mcap_inpagreq + rmtcmd.len;
                            break;
                        }
                    }

                    if (rep.m.pglst_mng.len > ((uint64_t)PG_BATCH_SIZE) ) {
                        #ifdef MM_CMD_RCV_DEBUG
                        printf("func %s, note: rep.m.pglst_mng.len=%lu, larger than PG_BATCH_SIZE=%i\n, wrong data received, the problem might be bigger",
                                                __func__, rep.m.pglst_mng.len, PG_BATCH_SIZE);
                        #endif
                        goto break_mm_req_mcap;
                    }

		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: addresses requested from HV:\n", __func__);
		    for (i=0; i < rep.m.pglst_mng.len; i++) {
			printf("\trep.m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n",
				    i, rep.m.pglst_mng.addr_blks[i].base, 
					rep.m.pglst_mng.addr_blks[i].length);
		    }
		    #endif

		    if (rep.m.pglst_mng.len > 0) {
			rep.cmd = MM_RCV_MCAP;
			#ifdef MM_CMD_RCV_DEBUG
			printf("func %s, note: about to send MM_RCV_MCAP to rep.m.pglst_mng.region_id=%i\n", 
					__func__, rep.m.pglst_mng.region_id);
			#endif
			rc = mmskt_send( &rep, NULL, NULL, rmtcmd.trn_id, lnid, ninf, node_no);
 
                        rmopcnt[rmopcnt_id].rcv_mcap_out = rmopcnt[rmopcnt_id].rcv_mcap_out + 1;
                        rmopcnt[rmopcnt_id].rcv_mcap_outpagsent = rmopcnt[rmopcnt_id].rcv_mcap_outpagsent + rep.m.pglst_mng.len;

                        rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_out = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_out + 1;
                        rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_outpagsent = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_outpagsent + rep.m.pglst_mng.len;

                        for (i = 0; i < rep.m.pglst_mng.len; i++) {
                            // NOTE: the destination of the memory pages has to be aware which node is the original giver.

                            if ( is_address_local(rep.m.pglst_mng.addr_blks[i].base, ninf, lnid) == 1 ) {
                                rmopcnt[rmopcnt_id].remote_pages_given = rmopcnt[rmopcnt_id].remote_pages_given + rep.m.pglst_mng.addr_blks[i].length;
                                rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].remote_pages_given = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].remote_pages_given + rep.m.pglst_mng.addr_blks[i].length;
                            } else {
                                int32_t areg;

                                rmopcnt[rmopcnt_id].remote_pages_taken = rmopcnt[rmopcnt_id].remote_pages_taken - rep.m.pglst_mng.addr_blks[i].length;

                                // we have to look for the original region of this set of addresses
                                areg = address_region(rep.m.pglst_mng.addr_blks[i].base, ninf, lnid);
                                for (j=0; j < node_no; j++) { 
                                    if (areg == rmopcnt[rmopcnt_id].nodinfo[j].region_id ) {
                                        rmopcnt[rmopcnt_id].nodinfo[j].remote_pages_taken = rmopcnt[rmopcnt_id].nodinfo[j].remote_pages_taken - rep.m.pglst_mng.addr_blks[i].length;
                                    }
                                }
                                  
                            }
                        }
		    }
		    #ifdef MM_CMD_RCV_DEBUG 
		    else {
			printf("func %s, note: cannot sent MM_RCV_MCAP to rep.m.pglst_mng.region_id=%i, rep.m.pglst_mng.len=%lu, not > 0", 
				    __func__, rep.m.pglst_mng.region_id, rep.m.pglst_mng.len);
		    }	
		    #endif
break_mm_req_mcap:
		break;
		case MM_FN2M_REQ_MCAP:
		    // This command: a node tells the master it needs memory capacity. The master chooses a node,
		    // that could be himself, and services the request. If the node is not himself, it will forward
		    // the request
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_FN2M_REQ_MCAP, from rmtcmd.src_region=%i\n",
				 __func__, rmtcmd.src_region);
		    #endif

		    // 1. Choose the domain from which to take the request from.
		    // Policy based value, but will be chosen randomly based on the node_no
		    //do {
		    //	dest_node_id = rand() % node_no;   // must be done based on policy
		    //} while ( (ninf[dest_node_id].region == rmtcmd.src_region) && (node_no > 1));

                    id_array = (int32_t *)malloc(node_no*sizeof(int32_t));
                    node_count = which_node2ask( opcnt, rmopcnt, id_array, lnid, ninf, node_no  );
                    printf("func %s, note: dest_node_id=%i, lnid=%i, opcnt[%i].region_id=%i, ninf[%i].region=%i", 
                              __func__, dest_node_id, lnid, dest_node_id, opcnt[dest_node_id].region_id, lnid, ninf[lnid].region);
		    // 2. Check if the chosen node is the master.
                    if (opcnt[id_array[0]].total_list == 0) {
                        pagesLeft = 0;
                    } else {
                        pagesLeft = rmtcmd.len;
                    }

                    for (k = 0; k < node_count && pagesLeft > 0; k++) {
                        dest_node_id = id_array[k];
                        printf("\tfunc %s, note: requesting pages from dest_node_id=%i, opcnt[%i].region_id=%i\n", 
                             __func__, dest_node_id, dest_node_id, opcnt[dest_node_id].region_id);
		        if ( opcnt[dest_node_id].region_id == ninf[lnid].region) {
			    #ifdef MM_CMD_RCV_DEBUG
			    printf("func %s, note: requesting memory from master, dest_node_id=%i, lnid=%i, equal\n",
				     __func__, dest_node_id, lnid);
			    #endif
			    // 2.1. If it is, Fetch the required amount in the hypervisor
			    rep.m.pglst_mng.region_id = rmtcmd.src_region;
                            if (pagesLeft >= opcnt[dest_node_id].total_list) {
		                rc = mm_req_mcap(&rep, &opcnt[0], rmopcnt, nodecharts, entries, this_region_id, node_no, lnid, ninf, opcnt[dest_node_id].total_list );
                                pagesLeft = pagesLeft - opcnt[dest_node_id].total_list;
                            } else {
		                rc = mm_req_mcap(&rep, &opcnt[0], rmopcnt, nodecharts, entries, this_region_id, node_no, lnid, ninf, pagesLeft );
                                pagesLeft = 0;
                            }

			    // 2.2. Send the list to the other mm
			    rep.cmd = MM_RCV_MCAP;
			    rc = mmskt_send( &rep, NULL, NULL, rmtcmd.trn_id, lnid, ninf, node_no);

                            rmopcnt[rmopcnt_id].rcv_mcap_out = rmopcnt[rmopcnt_id].rcv_mcap_out + 1;
                            rmopcnt[rmopcnt_id].rcv_mcap_outpagsent = rmopcnt[rmopcnt_id].rcv_mcap_outpagsent + rep.m.pglst_mng.len;

                            for (i = 0; i < MAX_RMTNODES; i++) {
                                if (rmtcmd.src_region == rmopcnt[rmopcnt_id].nodinfo[i].region_id) {
                                    rmtcmd_ind = i;
                                    break;
                                }
                            }

                            rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_out = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_out + 1;
                            rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_outpagsent = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].rcv_mcap_outpagsent + rep.m.pglst_mng.len;

                            for (i = 0; i < rep.m.pglst_mng.len; i++) {
                                if ( is_address_local(rep.m.pglst_mng.addr_blks[i].base, ninf, lnid) == 1 ) {
                                    rmopcnt[rmopcnt_id].remote_pages_given = rmopcnt[rmopcnt_id].remote_pages_given + rep.m.pglst_mng.addr_blks[i].length;
                                    rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].remote_pages_given = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].remote_pages_given + rep.m.pglst_mng.addr_blks[i].length;
                                } else {
                                    int32_t areg;

                                    rmopcnt[rmopcnt_id].remote_pages_taken = rmopcnt[rmopcnt_id].remote_pages_taken - rep.m.pglst_mng.addr_blks[i].length;

                                    // we have to look for the original region of this set of addresses
                                    areg = address_region(rep.m.pglst_mng.addr_blks[i].base, ninf, lnid);
                                    for (j =0; j < node_no; j++) {
                                        if (rmopcnt[rmopcnt_id].nodinfo[j].region_id == areg ) {
                                            rmopcnt[rmopcnt_id].nodinfo[j].remote_pages_taken = rmopcnt[rmopcnt_id].nodinfo[j].remote_pages_taken - rep.m.pglst_mng.addr_blks[i].length;
                                        }
                                    }
                                }
                            }

		        } else if ( dest_node_id != DEST_NODE_NONE ) {
                            if (ninf[dest_node_id].region != rmtcmd.src_region) { 
			        #ifdef MM_CMD_RCV_DEBUG
			        printf("func %s, note: requesting memory from other node, ninf[%i].region=%i, lnid=%i, equal\n",
			  	      __func__, dest_node_id, ninf[dest_node_id].region, lnid);
			        #endif
			        // 2.3. If it is not, forward the request to the desired node. Need third node to test it.
			        //rep.cmd = MM_FM2N_REQ_MCAP;
                                rep.cmd = MM_REQ_MCAP;
			        for (i=0; i < node_no; i++) {
			            if (ninf[i].region == rmtcmd.src_region) {
				        source_nid = i;
			            }
			        }
              
                                if (pagesLeft >= opcnt[dest_node_id].total_list) {
                                    rep.m.pglst_mng.len = opcnt[dest_node_id].total_list;
                                    pagesLeft = pagesLeft - opcnt[dest_node_id].total_list;
                                } else {
                                    rmtcmd.len = pagesLeft;
                                    pagesLeft = 0;
                                }
			        //rep.m.pglst_mng.len = rmtcmd.len;
			        // the final destination is node that sent req
			        rep.m.pglst_mng.region_id = opcnt[dest_node_id].region_id;
			

			        // 2.4. Forward the command to the node chosen to give it.
			        rc = mmskt_send( &rep, NULL, NULL, rmtcmd.trn_id, source_nid, ninf, node_no);
                            }
		        } 
                        #ifdef MM_CMD_RCV_DEBUG
                        else {
                            printf("func %s, note: dest_node_id = DEST_NODE_NONE, no node can service request\n",
                                 __func__ );
                        }
                        #endif

                        /*for (i=0; i < node_no; i++) {
                            if (opcnt[dest_node_id].region_id == rmopcnt[i].region_id) {
                                pages_in_remote_dest = ninf[lnid].num_pages + rmopcnt[i].remote_pages_taken - rmopcnt[i].remote_pages_given;
                                lists_in_remote_dest = opcnt[dest_node_id].total_list;
                                if ( (rmtcmd.len >= pages_in_remote_dest) && (rmtcmd.len >= lists_in_remote_dest) ) {
                                    node_count = 1;
                                }
                            }
                        }*/

                    }
                    free(id_array);
		break;
		case MM_FM2N_REQ_MCAP:
		    // This command: a node requests memory capacity
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_REQ_MCAP, from rmtcmd.src_region=%i\n", __func__, rmtcmd.src_region);
		    #endif

		    // 1. Fetch the required amount in the hypervisor
		    rep.m.pglst_mng.region_id = rmtcmd.src_region;
		    rc = mm_req_mcap(&rep, &opcnt[0], rmopcnt, nodecharts, entries, this_region_id, node_no, lnid, ninf, rmtcmd.len );

		    // 2. Send the list to the other mm
		    rep.cmd = MM_FN2N_REQ_MCAP;
		    rc = mmskt_send( &rep, NULL, NULL, rmtcmd.trn_id, lnid, ninf, node_no);
		break;	
		case MM_RECLAIM_MCAP:
 
		    // This command: a node requests memory capacity
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_RECLAIM_MCAP, from rmtcmd.src_region=%i\n", 
				__func__, rmtcmd.src_region);
		    #endif

		    // 1. Fetch the required amount in the hypervisor
		    rep.cmd = MM_RECLAIM_MCAP;
		    rep.m.pglst_mng.region_id = rmtcmd.src_region;
		    rc = mm_reclaim_mcap(&rep, &opcnt[0], rmopcnt, nodecharts, entries, this_region_id, node_no, lnid, ninf, rmtcmd.len, rmtcmd.src_region);

		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: addresses reclaimed from HV:\n", __func__);
		    for (i=0; i < rep.m.pglst_mng.len; i++) {
			printf("\trep.m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n",
				    i, rep.m.pglst_mng.addr_blks[i].base, 
					rep.m.pglst_mng.addr_blks[i].length);
		    }
		    #endif

		    // 2. Send the list to the other mm

                    rmopcnt->reclaim_mcap_in = rmopcnt->reclaim_mcap_in + 1;
                    rmopcnt->reclaim_mcap_inpagret = rmopcnt->reclaim_mcap_inpagret + rmtcmd.len;
                    for (i = 0; i < MAX_RMTNODES; i++) {
                        if (rmtcmd.src_region == rmopcnt->nodinfo[i].region_id) {
                            rmtcmd_ind = i;
                            rmopcnt->nodinfo[i].reclaim_mcap_in = rmopcnt->nodinfo[i].reclaim_mcap_in + 1;
                            rmopcnt->nodinfo[i].reclaim_mcap_inpagret = rmopcnt->nodinfo[i].reclaim_mcap_inpagret + rmtcmd.len;
                            break;
                        }
                    }

                    if (rep.m.pglst_mng.len > 0) { 
		        rep.cmd = MM_RECLACK_MCAP;
		        rc = mmskt_send( &rep, NULL, NULL, rmtcmd.trn_id, lnid, ninf, node_no);

                        rmopcnt[rmopcnt_id].reclack_mcap_out = rmopcnt[rmopcnt_id].reclack_mcap_out + 1;
                        rmopcnt[rmopcnt_id].reclack_mcap_outpagret = rmopcnt[rmopcnt_id].reclack_mcap_outpagret + rep.m.pglst_mng.len;

                        rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].reclack_mcap_out = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].reclack_mcap_out + 1;
                        rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].reclack_mcap_outpagret = rmopcnt[rmopcnt_id].nodinfo[rmtcmd_ind].reclack_mcap_outpagret + rep.m.pglst_mng.len;

                        for (i = 0; i < rep.m.pglst_mng.len; i++) {
                            rmopcnt[rmopcnt_id].remote_pages_taken = rmopcnt[rmopcnt_id].remote_pages_taken - rep.m.pglst_mng.addr_blks[i].length;    
                        }
                    }

		break;
		case MM_RELINQ_MCAP:
                    printf("func %s, note: if MM_RELINQ_MCAP is received as MM_CMD_PLAIN, we have a serious erro\n", 
                             __func__);
		break;
		case MM_REQ_STATS:
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: rcvd MM_REQ_STATS, about to send stats to master\n", 
				__func__);
		    #endif
		    // 1. Check that the node that send IS NOT a master
		    // If the source region is not a master, then we have a problem
		    for (i=0; i < node_no; i++) {
			if (ninf[i].master == 1) {
			    if (ninf[i].region != rmtcmd.src_region) {
				// This is a mistake, return
				printf("func %s, error: MM_REQ_STATS is master's privilege , wrong!\n", 
					__func__);
				return ERR_CMDRCV_REQSTAT_MASTER;
			    }
			    tind = i;
			}
		    }

		    #ifdef TEST_SEQUENCE_4
		    if (rmtcmd.len != MM_TEST_VALUE_1) {
			printf("func %s, error: rmtcmd.len=%lu, MM_TEST_VALUE_1=%i, not equal\n", 
					__func__, rmtcmd.len, MM_TEST_VALUE_1);
		    } else {
			printf("func %s, dap: rmtcmd.len=%lu, MM_TEST_VALUE_1=%i, ARE EQUAL\n", 
					__func__, rmtcmd.len, MM_TEST_VALUE_1);
		    }
		    #endif

		    // 2. Take the local opcnt, and send it back to the master
		    //opcnt[0].total_list = MM_TEST_VALUE_2;
		    //opcnt[0].local_list = MM_TEST_VALUE_3;
		    rc = mm_stats_cmd(opcnt, rmopcnt, MM_SND_STATS, ninf[lnid].region, 
					ninf[tind].region, lnid, ninf, node_no); 
		break;
		default:
		    printf("func %s, error: command not defined, rmtcmd.cmd=%i\n", 
				__func__, rmtcmd.cmd);
		break;
	    }

	break;
	case MM_CMD_PGLST:
	    #ifdef MM_CMD_RCV_DEBUG
	    printf("func %s, note: command type received, MM_CMD_PGLST\n", __func__);
	    #endif

	    switch(pglst_cmdrmt.cmd) {
		case MM_TEST_RESP:
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: pglst_cmdrmt.len=%lu\n", 
				__func__, pglst_cmdrmt.len);
		    #endif
		    for (i=0; i < pglst_cmdrmt.len; i++) {
			if (pglst_cmdrmt.addr_blks[i].base != (i*i) ) {
			    printf("func %s, note: %i*%i=%i, pglst_cmdrmt.addr_blks[%i].base=%lu, not equal\n", 
					__func__, i,i,(i*i), i, pglst_cmdrmt.addr_blks[i].base);
			    errors++;
			}

			if (pglst_cmdrmt.addr_blks[i].length != (i*(i-i/2)) ) {
			    printf("func %s, note: %i*(%i-%i/2)=%i, pglst_cmdrmt.addr_blks[%i].length=%lu, not equal\n", 
					__func__, i,i,i,(i*(i-i/2)), i, pglst_cmdrmt.addr_blks[i].length);
			    errors++;
			}
		    }

		    printf("func %s, note: test command received and sent successfully, errors=%i\n", 
				__func__, errors);
                    rc = 1;
		break;
		case MM_RCV_MCAP:

		    // The request has been responded
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_RCV_MCAP, from rmtcmd.src_region=%i\n", 
				__func__, pglst_cmdrmt.src_region);
		    #endif

		    // 1. Take the incoming data in pglst_cmdrmt and turn it into a mm_hyp_cmd
		    rep.m.pglst_mng.len = pglst_cmdrmt.len;
		    rep.m.pglst_mng.region_id = pglst_cmdrmt.src_region;
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: addresses received:\n", __func__);
		    #endif
                   
		    for (i=0; i < rep.m.pglst_mng.len; i++) {
			rep.m.pglst_mng.addr_blks[i].base = pglst_cmdrmt.addr_blks[i].base;
			rep.m.pglst_mng.addr_blks[i].length = pglst_cmdrmt.addr_blks[i].length;

			#ifdef MM_CMD_RCV_DEBUG
			printf("\trep.m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n",
				    i, rep.m.pglst_mng.addr_blks[i].base, 
					rep.m.pglst_mng.addr_blks[i].length);
			#endif
                        pages_received = pages_received + pglst_cmdrmt.addr_blks[i].length;
		    }

		    // 2. Hypercall to include it in total list
		    rc = mm_rcv_mcap(&rep);

		    if (rc != SUCC_RCV_MCAP) {
			printf("func %s, error: cannot mm_rcv_mcap\n", __func__);
		    }

                    // 3. Document which domain sent this message, and how many
                    // pages have been received.
                    rmopcnt[rmopcnt_id].rcv_mcap_in = rmopcnt[rmopcnt_id].rcv_mcap_in + 1;
                    rmopcnt[rmopcnt_id].rcv_mcap_inpagrcvd = rmopcnt[rmopcnt_id].rcv_mcap_inpagrcvd + pages_received;
                    for (i = 0; i < MAX_RMTNODES; i++) {
                        if (rmtcmd.src_region == rmopcnt[rmopcnt_id].nodinfo[i].region_id) {
                            rmtcmd_ind = i;
                            rmopcnt[rmopcnt_id].nodinfo[i].rcv_mcap_in = rmopcnt[rmopcnt_id].nodinfo[i].rcv_mcap_in + 1;

                            // change this. The amount of received pages depends on the actual region value.
                            //rmopcnt[rmopcnt_id].nodinfo[i].rcv_mcap_inpagrcvd = rmopcnt[rmopcnt_id].nodinfo[i].rcv_mcap_inpagrcvd + rmtcmd.len;
                        }
                    }   
 
                    //printf("func %s, note: received rep.m.pglst_mng.len=%lu\n", __func__, rep.m.pglst_mng.len);
                    for (i = 0; i < rep.m.pglst_mng.len; i++) {
                        //printf("func %s, note: in number %i\n", __func__, i);
                        if ( is_address_local(rep.m.pglst_mng.addr_blks[i].base, ninf, lnid) == 0 ) {
                            int32_t areg;

                            rmopcnt[rmopcnt_id].remote_pages_taken = rmopcnt[rmopcnt_id].remote_pages_taken + rep.m.pglst_mng.addr_blks[i].length;

                            areg = address_region(rep.m.pglst_mng.addr_blks[i].base, ninf, lnid);
                            for (j = 0; j < node_no; j++) {
                                if (areg == rmopcnt[rmopcnt_id].nodinfo[j].region_id) {
                                    rmopcnt[rmopcnt_id].nodinfo[j].remote_pages_taken = rmopcnt[rmopcnt_id].nodinfo[j].remote_pages_taken + rep.m.pglst_mng.addr_blks[i].length;
                                    rmopcnt[rmopcnt_id].nodinfo[j].rcv_mcap_inpagrcvd = rmopcnt[rmopcnt_id].nodinfo[j].rcv_mcap_inpagrcvd + rep.m.pglst_mng.addr_blks[i].length;
                                }
                            }

                            if (areg != pglst_cmdrmt.src_region) {
                                // Send notification to the source region (areg) that I have received a list of addresses belonging
                                // to areg from a different node (pglst_cmdrmt.src_region)
                                rep.cmd = MM_DISPERS_MCAP;
                                rep.m.pglst_mng.len = 1;
                                rep.m.pglst_mng.addr_blks[0].base = rep.m.pglst_mng.addr_blks[i].length;
		                rep.m.pglst_mng.region_id = areg;
                                rep.m.pglst_mng.dst_group = pglst_cmdrmt.src_region;   // from which node I got this pages from.
 
                                rc = mmskt_send( &rep, NULL, NULL, 0, lnid, ninf, node_no);
                            }

                            //printf("\tfunc %s, note: received base is not local\n", __func__);
                        } /* if the received addresses are local, then a serious mistake happened. */
                    }
                            //rmopcnt->remote_pages_taken = rmopcnt->remote_pages_taken + pages_received;
                     

		break;
		case MM_FN2N_REQ_MCAP:
		    // The request has been responded
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_FN2N_REQ_MCAP, from pglst_cmdrmt.src_region=%i\n", 
				__func__, pglst_cmdrmt.src_region);
		    #endif

		    // 1. Take the incoming data in pglst_cmdrmt and turn it into a mm_hyp_cmd
		    rep.m.pglst_mng.len = pglst_cmdrmt.len;
		    rep.m.pglst_mng.region_id = pglst_cmdrmt.src_region;
		    for (i=0; i < rep.m.pglst_mng.len; i++) {
			rep.m.pglst_mng.addr_blks[i].base = pglst_cmdrmt.addr_blks[i].base;
			rep.m.pglst_mng.addr_blks[i].length = pglst_cmdrmt.addr_blks[i].length;
		    }

		    // 2. Hypercall to include it in total list
		    rc = mm_rcv_mcap(&rep);

		    if (rc != SUCC_RCV_MCAP) {
			printf("func %s, error: cannot mm_rcv_mcap\n", __func__);
		    }
		break;
		case MM_RECLACK_MCAP:
		    // The reclaim request has been responded
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_RECLACK_MCAP, from pglst_cmdrmt.src_region=%i\n", 
				__func__, pglst_cmdrmt.src_region);
		    #endif
	
		    // 1. Take the incoming data in pglst_cmdrmt and turn it into a mm_hyp_cmd
		    rep.cmd = MM_RECLACK_MCAP;
		    rep.m.pglst_mng.len = pglst_cmdrmt.len;
		    rep.m.pglst_mng.region_id = pglst_cmdrmt.src_region;

		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: addresses reclaimed back from remote node:\n", __func__);
		    #endif
		    for (i=0; i < rep.m.pglst_mng.len; i++) {
			rep.m.pglst_mng.addr_blks[i].base = pglst_cmdrmt.addr_blks[i].base;
			rep.m.pglst_mng.addr_blks[i].length = pglst_cmdrmt.addr_blks[i].length;
			
			#ifdef MM_CMD_RCV_DEBUG
			printf("\trep.m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n",
				    i, rep.m.pglst_mng.addr_blks[i].base, 
					rep.m.pglst_mng.addr_blks[i].length);
			#endif
                        pages_received = pages_received + pglst_cmdrmt.addr_blks[i].length;
		    }
		    rep.m.pglst_mng.src_group = HASH_GRP_RMT;
		    rep.m.pglst_mng.dst_group = LIST_GRP_TOTAL;

		    // 2. Hypercall to include it in total list
		    rc = mm_reclack_mcap(&rep);

		    if (rc != SUCC_RECLACK_MCAP) {
			printf("func %s, error: cannot mm_rcv_mcap\n", __func__);
		    }

                    rmopcnt[rmopcnt_id].reclack_mcap_in = rmopcnt[rmopcnt_id].reclack_mcap_in + 1;
                    rmopcnt[rmopcnt_id].reclack_mcap_inpagret = rmopcnt[rmopcnt_id].reclack_mcap_inpagret + pages_received;
                    
                    rmopcnt[rmopcnt_id].remote_pages_given = rmopcnt[rmopcnt_id].remote_pages_given - pages_received;
                    
                    for (i = 0; i < MAX_RMTNODES; i++) {
                        if (rmtcmd.src_region == rmopcnt[rmopcnt_id].nodinfo[i].region_id) {
                            rmtcmd_ind = i;
                            rmopcnt[rmopcnt_id].nodinfo[i].reclack_mcap_in = rmopcnt[rmopcnt_id].nodinfo[i].reclack_mcap_in + 1;
                            rmopcnt[rmopcnt_id].nodinfo[i].reclack_mcap_inpagret = rmopcnt[rmopcnt_id].nodinfo[i].reclack_mcap_inpagret + pages_received;

                            rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given = rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given - pages_received;
                            break;
                        }
                    }

		break;
		case MM_RELACK_MCAP:

		    // The reclaim request has been responded
		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: received MM_RELACK_MCAP, from pglst_cmdrmt.src_region=%i\n", 
				__func__, pglst_cmdrmt.src_region);
		    #endif

		    rep.cmd = MM_RELACK_MCAP;
		    rep.m.pglst_mng.len = pglst_cmdrmt.len;
		    rep.m.pglst_mng.region_id = pglst_cmdrmt.src_region;

		    #ifdef MM_CMD_RCV_DEBUG
		    printf("func %s, note: addresses relacked from remote node:\n", __func__);
		    #endif
		    for (i=0; i < rep.m.pglst_mng.len; i++) {
			rep.m.pglst_mng.addr_blks[i].base = pglst_cmdrmt.addr_blks[i].base;
			rep.m.pglst_mng.addr_blks[i].length = pglst_cmdrmt.addr_blks[i].length;
			
			#ifdef MM_CMD_RCV_DEBUG
			printf("\trep.m.pglst_mng.addr_blks[%i], .base=%lu, .length=%lu\n",
				    i, rep.m.pglst_mng.addr_blks[i].base, 
					rep.m.pglst_mng.addr_blks[i].length);
			#endif
                        pages_received = pages_received + pglst_cmdrmt.addr_blks[i].length;
		    }
		    rep.m.pglst_mng.src_group = HASH_GRP_RMT;
		    rep.m.pglst_mng.dst_group = LIST_GRP_TOTAL;
		
		    // 2. Hypercall to include it in total list
		    rc = mm_relack_mcap(&rep);

		    if (rc != SUCC_RELACK_MCAP) {
			printf("func %s, error: cannot mm_relack_mcap\n", __func__);
		    }

                    rmopcnt[rmopcnt_id].relack_mcap_in = rmopcnt[rmopcnt_id].relack_mcap_in + 1;    
                    rmopcnt[rmopcnt_id].relack_mcap_inpagret = rmopcnt[rmopcnt_id].relack_mcap_inpagret + pages_received; 

                    rmopcnt[rmopcnt_id].remote_pages_given = rmopcnt[rmopcnt_id].remote_pages_given - pages_received;    
                    for (i = 0; i < MAX_RMTNODES; i++) {
                        if (rmtcmd.src_region == rmopcnt->nodinfo[i].region_id) {
                            rmopcnt[rmopcnt_id].nodinfo[i].relack_mcap_in = rmopcnt[rmopcnt_id].nodinfo[i].relack_mcap_in + 1;
                            rmopcnt[rmopcnt_id].nodinfo[i].relack_mcap_inpagret = rmopcnt[rmopcnt_id].nodinfo[i].relack_mcap_inpagret + pages_received ;

                            rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given = rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given - pages_received;
                            break;
                        }
                    }

		break;
                case MM_DISPERS_MCAP:
                    // Does not do any hypercall. This command tells this node that some or all of the memory it gave to
                    // a node Y, the node Y has given it away to a different node.
                    for (i=0; i < node_no; i++) {
                        if (rmopcnt[rmopcnt_id].nodinfo[i].region_id == pglst_cmdrmt.src_region) {
                            rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given = rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given + pglst_cmdrmt.len;
                        }

                        if (rmopcnt[rmopcnt_id].nodinfo[i].region_id == pglst_cmdrmt.dst_group) {
                            rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given = rmopcnt[rmopcnt_id].nodinfo[i].remote_pages_given - pglst_cmdrmt.addr_blks[0].base;
                        }
                    }

                break;
		default:
		    printf("func %s, error: command not defined, pglst_cmdrmt.cmd=%i\n", 
				__func__, pglst_cmdrmt.cmd);
		break;
	    }
	break;
	case MM_STATS:
	    #ifdef MM_CMD_RCV_DEBUG
	    printf("func %s, note: command type received, MM_STATS\n", __func__);
	    #endif

	    switch(stat_rmt.cmd) {
		case MM_SND_STATS:
		    /* A node sends its stats to the master node. */

		    // 1. Check that the node that send IS NOT a master
		    // If the source region is a master, 
		    for (i=0; i < node_no; i++) {
			if (ninf[i].master == 1) {
			    if (ninf[i].region == stat_rmt.src_region) {
				// This is a mistake, return
				printf("func %s, error: MM_SND_STATS sent by master, wrong!\n", __func__);
				return ERR_CMDRCV_SNDSTAT_MASTER;
			    }
			}
	
			if (opcnt[i].region_id == stat_rmt.src_region) {
			    tind = i;
                            //break;
			}

                        if (rmopcnt[i].region_id == stat_rmt.src_region) {
                            rind = i;
                        }
		    }

                    // No entry currently exists for the domain that just sent data. We need to create one.
                    if (tind == -1) {
                        printf("func %s, error: tind==-1, an entry should exist\n", __func__);
                        for (i=0; i < node_no; i++) {
                            if (opcnt[i].region_id == 0) {
                                tind = i;
                                break;
                            }
                        }
                    }

                    // No entry currently exists for the domain that just sent data. We need to create one.
                    // This is a mistake, though. There should be an entry.
                    if ( rind == -1) {
                        printf("func %s, error: rind==-1, an entry should exist\n", __func__);
                        for ( i=0; i < node_no; i++ ) {
                            if ( rmopcnt[i].region_id == 0 ) {
                                rind = i;
                                break;
                            }
                        }
                    }
                  
                    printf("func %s, note: before copying data, this_node_id=%i, opcnt[%i].total_list=%lu\n", __func__, lnid, lnid, opcnt[lnid].total_list ); 
		    // 2. Take the incoming rmt_opcnt, and copy it to the corresponding entry in opcnt
		    //memcpy(&opcnt[tind], &stat_rmt.rmt_opcnt, sizeof(struct tmop_cnt) );
                    opcnt[tind].curr_ts = stat_rmt.rmt_opcnt.curr_ts;
                    opcnt[tind].prev_ts = stat_rmt.rmt_opcnt.prev_ts;
                    opcnt[tind].free_pages = stat_rmt.rmt_opcnt.free_pages;
                    opcnt[tind].tmem_freeable_pages = stat_rmt.rmt_opcnt.tmem_freeable_pages;
                    opcnt[tind].domcnt = stat_rmt.rmt_opcnt.domcnt;
                    opcnt[tind].min_request_pages = stat_rmt.rmt_opcnt.min_request_pages;
                    opcnt[tind].region_id = stat_rmt.rmt_opcnt.region_id;
                    opcnt[tind].sys_mem = stat_rmt.rmt_opcnt.sys_mem;
                    opcnt[tind].tmempages = stat_rmt.rmt_opcnt.tmempages;

                    opcnt[tind].total_accesses = stat_rmt.rmt_opcnt.total_accesses;
                    opcnt[tind].total_list = stat_rmt.rmt_opcnt.total_list;
                    opcnt[tind].local_list = stat_rmt.rmt_opcnt.local_list;
                    opcnt[tind].remote_list = stat_rmt.rmt_opcnt.remote_list; 
                    opcnt[tind].given_list = stat_rmt.rmt_opcnt.given_list;
                    opcnt[tind].used_list = stat_rmt.rmt_opcnt.used_list;

                    opcnt[tind].total_puts = stat_rmt.rmt_opcnt.total_puts;
                    opcnt[tind].total_putssucc = stat_rmt.rmt_opcnt.total_putssucc;
                    opcnt[tind].total_gets = stat_rmt.rmt_opcnt.total_gets;
                    opcnt[tind].total_getssucc = stat_rmt.rmt_opcnt.total_getssucc;
                    opcnt[tind].total_flush = stat_rmt.rmt_opcnt.total_flush;
                    opcnt[tind].total_flushsucc = stat_rmt.rmt_opcnt.total_flushsucc;

                    opcnt[tind].local_puts = stat_rmt.rmt_opcnt.local_puts;
                    opcnt[tind].local_gets = stat_rmt.rmt_opcnt.local_gets;
                    opcnt[tind].local_flush = stat_rmt.rmt_opcnt.local_flush;
                    opcnt[tind].local_accesses = stat_rmt.rmt_opcnt.local_accesses;

                    opcnt[tind].rem_puts = stat_rmt.rmt_opcnt.rem_puts;
                    opcnt[tind].rem_gets = stat_rmt.rmt_opcnt.rem_gets;
                    opcnt[tind].rem_flush = stat_rmt.rmt_opcnt.rem_flush;
                    opcnt[tind].rem_accesses = stat_rmt.rmt_opcnt.rem_accesses;

                    opcnt[tind].total_tmem = stat_rmt.rmt_opcnt.total_tmem;
                    opcnt[tind].local_tmem = stat_rmt.rmt_opcnt.local_tmem;
                    opcnt[tind].remote_tmem = stat_rmt.rmt_opcnt.remote_tmem;
 
                    // 3. Take the incoming rmt_opcnt, and copy it to the corresponding entry in rmopcnt
                    rmopcnt[rind].region_id = stat_rmt.rmt_opcnt.region_id;
                    rmopcnt[rind].req_mcap_in = stat_rmt.rmt_opcnt.req_mcap_in;
                    rmopcnt[rind].req_mcap_inpagreq = stat_rmt.rmt_opcnt.req_mcap_inpagreq;
                    rmopcnt[rind].rcv_mcap_out = stat_rmt.rmt_opcnt.rcv_mcap_out;
                    rmopcnt[rind].rcv_mcap_outpagsent = stat_rmt.rmt_opcnt.rcv_mcap_outpagsent;
             
                    rmopcnt[rind].req_mcap_out = stat_rmt.rmt_opcnt.req_mcap_out;
                    rmopcnt[rind].req_mcap_outpagreq = stat_rmt.rmt_opcnt.req_mcap_outpagreq;
                    rmopcnt[rind].rcv_mcap_in = stat_rmt.rmt_opcnt.rcv_mcap_in;
                    rmopcnt[rind].rcv_mcap_inpagrcvd = stat_rmt.rmt_opcnt.rcv_mcap_in;

                    rmopcnt[rind].reclaim_mcap_in = stat_rmt.rmt_opcnt.reclaim_mcap_in;
                    rmopcnt[rind].reclaim_mcap_inpagret = stat_rmt.rmt_opcnt.reclaim_mcap_inpagret;
                    rmopcnt[rind].reclack_mcap_out = stat_rmt.rmt_opcnt.reclack_mcap_out;
                    rmopcnt[rind].reclack_mcap_outpagret = stat_rmt.rmt_opcnt.reclack_mcap_outpagret;
 
                    rmopcnt[rind].reclaim_mcap_out = stat_rmt.rmt_opcnt.reclaim_mcap_out;
                    rmopcnt[rind].reclaim_mcap_outpagreq = stat_rmt.rmt_opcnt.reclaim_mcap_outpagreq;
                    rmopcnt[rind].reclack_mcap_in = stat_rmt.rmt_opcnt.reclack_mcap_in;
                    rmopcnt[rind].reclaim_mcap_inpagret = stat_rmt.rmt_opcnt.reclaim_mcap_inpagret;

                    rmopcnt[rind].relack_mcap_in = stat_rmt.rmt_opcnt.relack_mcap_in;
                    rmopcnt[rind].relack_mcap_inpagret = stat_rmt.rmt_opcnt.relack_mcap_inpagret;
                    rmopcnt[rind].relack_mcap_out = stat_rmt.rmt_opcnt.relack_mcap_out;
                    rmopcnt[rind].relack_mcap_outpagret = stat_rmt.rmt_opcnt.relack_mcap_outpagret;

                    rmopcnt[rind].remote_pages_taken = stat_rmt.rmt_opcnt.remote_pages_taken;
                    rmopcnt[rind].remote_pages_given = stat_rmt.rmt_opcnt.remote_pages_given;
                    rmopcnt[rind].remote_pages_dispout = stat_rmt.rmt_opcnt.remote_pages_dispout;

                    for (i=0; i < MAX_RMTNODES; i++) {
                        rmopcnt[rind].nodinfo[i].region_id = stat_rmt.rmt_opcnt.nodinfo[i].region_id;

                        rmopcnt[rind].nodinfo[i].req_mcap_in = stat_rmt.rmt_opcnt.nodinfo[i].req_mcap_in;
                        rmopcnt[rind].nodinfo[i].req_mcap_inpagreq = stat_rmt.rmt_opcnt.nodinfo[i].req_mcap_inpagreq;
                        rmopcnt[rind].nodinfo[i].rcv_mcap_out = stat_rmt.rmt_opcnt.nodinfo[i].rcv_mcap_out;
                        rmopcnt[rind].nodinfo[i].rcv_mcap_outpagsent = stat_rmt.rmt_opcnt.nodinfo[i].rcv_mcap_outpagsent;

                        rmopcnt[rind].nodinfo[i].req_mcap_out = stat_rmt.rmt_opcnt.nodinfo[i].req_mcap_out;
                        rmopcnt[rind].nodinfo[i].req_mcap_outpagreq = stat_rmt.rmt_opcnt.nodinfo[i].req_mcap_outpagreq;
                        rmopcnt[rind].nodinfo[i].rcv_mcap_in = stat_rmt.rmt_opcnt.nodinfo[i].rcv_mcap_in;
                        rmopcnt[rind].nodinfo[i].rcv_mcap_inpagrcvd = stat_rmt.rmt_opcnt.nodinfo[i].rcv_mcap_inpagrcvd;

                        rmopcnt[rind].nodinfo[i].reclaim_mcap_in = stat_rmt.rmt_opcnt.nodinfo[i].reclaim_mcap_in;
                        rmopcnt[rind].nodinfo[i].reclaim_mcap_inpagret = stat_rmt.rmt_opcnt.nodinfo[i].reclaim_mcap_inpagret;
                        rmopcnt[rind].nodinfo[i].reclack_mcap_out = stat_rmt.rmt_opcnt.nodinfo[i].reclack_mcap_out;
                        rmopcnt[rind].nodinfo[i].reclack_mcap_outpagret = stat_rmt.rmt_opcnt.nodinfo[i].reclack_mcap_outpagret;

                        rmopcnt[rind].nodinfo[i].reclaim_mcap_out = stat_rmt.rmt_opcnt.nodinfo[i].reclaim_mcap_out;
                        rmopcnt[rind].nodinfo[i].reclaim_mcap_outpagreq = stat_rmt.rmt_opcnt.nodinfo[i].reclaim_mcap_outpagreq;
                        rmopcnt[rind].nodinfo[i].reclack_mcap_in = stat_rmt.rmt_opcnt.nodinfo[i].reclack_mcap_in;
                        rmopcnt[rind].nodinfo[i].reclack_mcap_inpagret = stat_rmt.rmt_opcnt.nodinfo[i].reclack_mcap_inpagret;
  
                        rmopcnt[rind].nodinfo[i].relack_mcap_in = stat_rmt.rmt_opcnt.nodinfo[i].relack_mcap_in;
                        rmopcnt[rind].nodinfo[i].relack_mcap_inpagret = stat_rmt.rmt_opcnt.nodinfo[i].relack_mcap_inpagret;
                        rmopcnt[rind].nodinfo[i].relack_mcap_out = stat_rmt.rmt_opcnt.nodinfo[i].relack_mcap_out; 
                        rmopcnt[rind].nodinfo[i].relack_mcap_outpagret = stat_rmt.rmt_opcnt.nodinfo[i].relack_mcap_outpagret;

                        rmopcnt[rind].nodinfo[i].remote_pages_taken = stat_rmt.rmt_opcnt.nodinfo[i].remote_pages_taken;
                        rmopcnt[rind].nodinfo[i].remote_pages_given = stat_rmt.rmt_opcnt.nodinfo[i].remote_pages_given;
                        rmopcnt[rind].nodinfo[i].remote_pages_dispin = stat_rmt.rmt_opcnt.nodinfo[i].remote_pages_dispin;
                        rmopcnt[rind].nodinfo[i].remote_pages_dispout = stat_rmt.rmt_opcnt.nodinfo[i].remote_pages_dispout;

                        rmopcnt[rind].nodinfo[i].reallatency2node = stat_rmt.rmt_opcnt.nodinfo[i].reallatency2node;
                        rmopcnt[rind].nodinfo[i].estilatency2node = stat_rmt.rmt_opcnt.nodinfo[i].estilatency2node;

                        #ifdef MM_CMD_RCV_SNDSTATS_DEBUG
                        printf("func %s, note: rind=%i,i=%i, rmopcnt[%i].nodinfo[%i], .remote_pages_taken=%lu, .remote_pages_given=%lu \n", 
                                     __func__, rind, i, rind, i,  rmopcnt[rind].nodinfo[i].remote_pages_taken, rmopcnt[rind].nodinfo[i].remote_pages_given);
                        #endif
                    }


                    // Call the function to build a new graph
                    rc_grph = build_graph(opcnt, rmopcnt, stat_rmt.rmt_opcnt.curr_ts, ninf, node_no, lnid);

                    #ifdef MM_CMD_RCV_SNDSTATS_DEBUG
                    printf("func %s, note: chec rmt stats: tind=%i, opcnt[%i].curr_ts=%lu, .prev_ts=%lu, .region_id=%i, .total_list=%lu, .local_list=%lu, .remote_list=%lu, .rem_puts=%lu\n", 
                                __func__, tind, tind, opcnt[tind].curr_ts, opcnt[tind].prev_ts, opcnt[tind].region_id, 
                                opcnt[tind].total_list, opcnt[tind].local_list, opcnt[tind].remote_list, opcnt[tind].rem_puts);

                    printf("func %s, note: this_node_id=%i, opcnt[%i].total_list=%lu\n", __func__, lnid, lnid, opcnt[lnid].total_list );

                    #endif

		    #ifdef TEST_SEQUENCE_4
		    if (opcnt[tind].total_list != MM_TEST_VALUE_0) {
			printf("func %s, error: in tx, opcnt[%i].total_list=%lu, MM_TEST_VALUE_0=%i\n", 
					__func__, tind, opcnt[tind].total_list, MM_TEST_VALUE_0);
			errors++;
		    }

		    if (opcnt[tind].local_list != (2*MM_TEST_VALUE_0) ) {
			printf("func %s, error: in tx, opcnt[%i].local_list=%lu, (2*MM_TEST_VALUE_0)=%i\n", 
					__func__, tind, opcnt[tind].local_list, (2*MM_TEST_VALUE_0));
			errors++;
		    }

		    printf("func %s, note: opcnt[%i], .total_list=%lu, .local_list=%lu, errors=%i \n", 
				__func__, tind, opcnt[tind].total_list, opcnt[tind].local_list, errors);
		    #endif  
		break;
		default:
		    printf("func %s, error: command not defined, stat_rmt.cmd=%i\n", 
				__func__, stat_rmt.cmd);
		break;
	    }
	break;
	default:
	    #ifdef MM_CMD_RCV_DEBUG
	    //printf("func %s, note: undefined command, cmd_type=%i\n", __func__, cmd_type);
	    #endif
	break;
    }


    return rc;
}

/* Receive stats coming from the HV->TKM */
int32_t stats_rcv(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, struct pernodechart *nodecharts, 
                     uint64_t *entries, uint32_t this_region_id, uint8_t inter,
                     uint32_t node_no, uint32_t this_node_id, struct node_info *ninf ) {
    int32_t rc, i;

    #ifdef MM_STATS_RCV_DEBUG
    printf("func %s, note: enter %s\n", __func__, __func__);
    #endif

    if (inter == 0) {
        rc = tmif_rcv_stats(opcnt);
        if (rc != TMIF_RCVD_EXPECTED ) {
	    #ifdef MM_STATS_RCV_DEBUG
	    printf("func %s, error: Problem receiving data from tmif_rcv, rc=%i\n", 
	    		__func__, rc);
	    #endif

            //if (ninf[this_node_id].master == 2) {
            //printf("func %s, note: stats_rcv demented\n", __func__);
            #ifdef MM_INACT_CONSTRAINT
            if ( time_size == nodecharts->time.size) {   // The window size
                inact_count = inact_count + 1;
                printf("\tfunc %s, note: inact_count=%u\n", __func__, inact_count);
                // Inactivity has been detected.
                if ( inact_count > INACTIVITY_COUNT ) {
                    printf("\tTrigger move_localOrRemote_to_toal hypercall");
                    rc = move_localOrRemote_to_total(PGSRET_NOACTIVITY);
                    inact_count = 0;
                }
            } else {
                inact_count = 0;
                time_size = nodecharts->time.size;
            }
            #endif
            //}
	    return rc;
        } else if (rc == TMIF_RCVD_EXPECTED) {
            //printf("func %s, note: modifying entries value\n", __func__);
	    *entries = *entries + 1;
        }
    } else if (inter == 1) {
        //printf("func %s, note: modifying entires value\n", __func__);
        *entries = *entries + 1;
    }
    rc = node_updt(opcnt, rmopcnt, nodecharts, this_region_id, node_no, this_node_id, ninf);
 
    opcnt->sys_mem = ninf[this_node_id].num_pages;
    #ifdef MM_STATS_RCV_DEBUG
    printf("\t%lu. opcnt, .free_pages=%lu, .total_list=%lu, .local_list=%lu, .remote_list=%lu, .given_list=%lu\n", 
		*entries,
		opcnt->free_pages, 
		opcnt->total_list,
		opcnt->local_list,
		opcnt->remote_list,
		opcnt->given_list );
    printf("\topcnt, .domcnt: %i, .total_puts=%lu, .total_gets=%lu\n\n", 
			opcnt->domcnt, 
			opcnt->total_puts,
			opcnt->total_gets);
    #endif

    // Inactivity detection.
    // In this case, once inactivity is detected, return pages from the local/remote lists 
    // to the total lists. This will enable the node to service requests for remote memory.
    /*if (nodecharts->time.size > 0) {
        if ( nodecharts->time.size == 1) {
            inact_count = inact_count + 1;
        } else {
            if ( nodecharts->nodestats.list_last->total_accesses == nodecharts->nodestats.list_last->prev->total_accesses) {
                inact_count = inact_count + 1;
            }
        }

        // Inactivity has been detected.
        if ( inact_count > INACTIVITY_COUNT ) {
            rc = move_localOrRemote_to_total(PGSRET_NOACTIVITY);
            inact_count = 0;
        }

    } else {
        inact_count = 0;
    }*/

    //printf("func %s, note: stats_rcv demented\n", __func__);
    #ifdef MM_INACT_CONSTRAINT
    if ( time_size == nodecharts->time.size) {
        inact_count = inact_count + 1;
        //printf("\tfunc %s, note: inact_count=%u\n", __func__, inact_count);
        // Inactivity has been detected.
        if ( inact_count > INACTIVITY_COUNT ) {
            rc = move_localOrRemote_to_total(PGSRET_NOACTIVITY);
            inact_count = 0;
        }
    } else {
        inact_count = 0;
        time_size = nodecharts->time.size;
    }
    #endif

    return rc;
}

/* Manage commands coming from other memory managers */
int mm_rmtcmd(struct tmop_cnt *opcnt) {
    printf("in %s", __func__);

    return 0;
}
