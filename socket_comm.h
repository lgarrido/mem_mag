#ifndef MM_INC_SOCKCOMM
#define MM_INC_SOCKCOMM

#include "mm_ctrl.h"
#include "node_info.h"

#define MAX_CLIENTS 30
#define MMSKT_SELECT_TIMEOUT_SEC 0
#define MMSKT_SELECT_TIMEOUT_USEC 10

#define SUCC_MMSKT_INIT  0


int client_socket_nid(int sd, unsigned int node_no);

int mmskt_init(struct node_info *ninf, uint32_t nid);
int mmskt_send(struct mm_hyp_cmd *mmcmd);
int mmskt_rcv(struct tmop_cnt *opcnt);
int mmskt_close();

int mmskt_wr_test();


#endif
