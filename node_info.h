#ifndef MM_INC_NODINF
#define MM_INC_NODINF

#include <arpa/inet.h> 

// Defining the rate of sampling
#define MM_SAMPLING_TIME   250000000
#define PAGE_SIZE	   4096
#define MAX_DOMAINS 	   4
#define MAX_RMTNODES	   5	// total nodes, master + remote nodes
#define BUF_SIZE 	   256
#define WINDOW_SIZE 		100
#define CHART_FILENAME_SIZE	100
#define GRAPH_FILENAME_SIZE     120

#define MM_NSTAT_FILE		"/home/silverman/Programs/memory_manager/mem_mag/mm_statsfile_reg"
#define MM_GRAPHFILE            "./flowgraphs/flowgraph"
#define MM_STATFILE_EXTENSION	".csv"
#define MM_GRAPHFILE_EXTENSION  ".gv"
#define MM_XEN_TIMESCALE	1000000000

#define MM_WRITESTATS_DEBUG
//#define MM_NODINF_DOMUPDT_DEBUG
//#define MM_NODINF_DOM_REG_DEBUG
//#define MM_BUILD_GRAPH_DEBUG

// Messages when trying to read configuraiton file
#define CFG_READ_SUCC 0
#define SUCC_DOMAIN_UPDATE 0

/* Debug Macros */
//#define COPY_NODESTATS_DEBUG
//#define COPY_DOMSTATS_DEBUG

/* Success & Error Macros */
#define ERR_CLEARING_PERNODECHART -1
#define E_CFG_OPEN  1
#define E_CFG_READ  2

#define SUCC_PERNODECHART_CLEAR		1
#define ERR_PERNODECHART_CLEAR		-1

#define SUCC_TMOPCNT_CLEAR	  1
#define ERR_TMOPCNT_CLEAR	 -1

#define SUCC_RMTAGGRCNT_CLEAR        1
#define ERR_RMTAGGRCNT_CLEAR        -1

#define SUCC_DOM_REG		 1
#define ERR_DOM_REG		-1

#define SUCC_NODE_UPDT		 1
#define ERR_NODE_UPDT		-1

#define SUCC_INSERT_ENTRYLIST	 1
#define ERR_INSERT_ENTRYLIST	-1

#define ERR_REGION_ID_INCONSISTENCY -3

#define SUCC_NODE_REGISTER		 1
#define ERR_NODE_REGISTER_CANTCLR	-1

#define GRAPH_CLEAR_SUCC                 1
#define GRAPH_CLEAR_ERR                 -1


#define MM_STATS_FILE_PRINT		1

struct node_info {
    uint32_t node_id;
    struct sockaddr_in naddr;
    uint32_t portno;
    unsigned short self;
    unsigned short master;
    uint32_t region;
 
    uint64_t addr_strt;
    uint64_t num_pages;
    uint64_t local_init_pages;
    int32_t rem_socket;
    int32_t connected;

    int sd;
    unsigned int bffst;
    char * rdbuf;
    uint8_t cmd_type;
};

struct domstat_file_status {
    uint32_t region_id;
    uint32_t dom_id;
    uint8_t stat_file_opened;
};

struct tmop_domcnt {
    /* General statistics */
    uint32_t region_id;
    int32_t dom_id;
    uint64_t total_access;
    uint32_t imm_alloc;

    /* 
    Policy statistics 
    only necessary to send back this, because the rest
    are already known by the MM. 
    */
    uint64_t mm_target;
    uint64_t mm_slack;
    uint64_t dn_target;
    uint64_t breached;
    uint64_t balloon_target;

    /* Local statistics */
    uint64_t nod_mem;
    uint64_t nod_tmem;
    //uint64_t nod_tmem_rsv;

    uint64_t nod_puts_tot;
    uint64_t nod_puts_succ;
    
    uint64_t nod_gets_tot;
    uint64_t nod_gets_succ;

    uint64_t nod_flush_tot;
    uint64_t nod_flush_succ;

    /* Local stats  */
    uint64_t total_local_access;
    uint64_t nod_puts_local_tot;
    uint64_t nod_puts_local_succ;

    uint64_t nod_gets_local_tot;
    uint64_t nod_gets_local_succ;

    uint64_t nod_flush_local_tot;
    uint64_t nod_flush_local_succ;

    /* Remote stats  */
    uint64_t total_rem_access;
    uint64_t nod_puts_rem_tot;
    uint64_t nod_puts_rem_succ;
    
    uint64_t nod_gets_rem_tot;
    uint64_t nod_gets_rem_succ;

    uint64_t nod_flush_rem_tot;
    uint64_t nod_flush_rem_succ;
};

struct tmop_cnt {
    uint64_t curr_ts;
    uint64_t prev_ts;
    uint64_t free_pages;
    uint64_t tmem_freeable_pages;
    uint8_t domcnt;
    uint64_t min_request_pages;
    uint32_t region_id;
    uint32_t sys_mem;
    uint32_t tmempages;

    uint64_t total_accesses;

    uint64_t total_list;
    uint64_t local_list;
    uint64_t remote_list;
    uint64_t given_list;
    uint64_t used_list;

    uint64_t total_puts;
    uint64_t total_putssucc;
    uint64_t total_gets;
    uint64_t total_getssucc;
    uint64_t total_flush;
    uint64_t total_flushsucc;

    uint64_t local_puts;
    uint64_t local_gets;
    uint64_t local_flush;
    uint64_t local_accesses;

    uint64_t rem_puts;
    uint64_t rem_gets;
    uint64_t rem_flush;
    uint64_t rem_accesses;

    uint64_t total_tmem;
    uint64_t local_tmem;
    uint64_t remote_tmem;

    struct tmop_domcnt dominfo[MAX_DOMAINS];
};

struct value_entry {
    uint64_t v;
    struct value_entry *prev;
    struct value_entry *next;
};

struct single_entry_list {
    long unsigned int size;
    struct value_entry *list_root;
    struct value_entry *list_last;
};

struct domchart_entry {
    uint64_t total_accesses;
    uint64_t nod_tmem;
    uint64_t nod_mem;
 
    uint64_t puts;
    uint64_t putssucc;
    float putrateavg;
    float putrateinst;
    float putfailrateinst;

    uint64_t gets;
    uint64_t getssucc;
    float getrateavg;
    float getrateinst;
    float getfailrateinst;

    uint64_t flush;
    uint64_t flushsucc;
    float flushrateavg;
    float flushrateinst;

    uint64_t mm_target;		// pages currently allocated, in total: the ones it using + the ones it can still use
    uint64_t mm_slack;
    uint64_t dn_target;
    uint8_t breached;
    uint64_t balloon_target;

    uint64_t total_local_access;
    uint64_t local_puts;
    uint64_t local_putssucc;
    float local_putrateavg;
    float local_putrateinst;
    float local_putfailrateinst;
 
    uint64_t local_gets;
    uint64_t local_getssucc;
    float local_getrateavg;
    float local_getrateinst;
    float local_getfailrateinst;

    uint64_t local_flush;
    uint64_t local_flushsucc;
    float local_flushrateavg;
    float local_flushrateinst;
    float local_flushfailrateinst;

    uint64_t total_rem_access;
    uint64_t rem_puts;
    uint64_t rem_putssucc;
    float rem_putrateavg;
    float rem_putrateinst;
    float rem_putfailrateinst;

    uint64_t rem_gets;
    uint64_t rem_getssucc;
    float rem_getrateavg;
    float rem_getrateinst;
    float rem_getfailrateinst;

    uint64_t rem_flush;
    uint64_t rem_flushsucc;
    float rem_flushrateavg;
    float rem_flushrateinst;
    float rem_flushfailrateinst;

    struct domchart_entry *prev;
    struct domchart_entry *next;
};

struct dom_entry_list {
    long unsigned int size;
    struct domchart_entry *list_root;
    struct domchart_entry *list_last;
};

struct perdomchart {
    uint32_t region_id;
    uint32_t dom_id;
    uint64_t nod_mem;

    //struct entry_list time;

    struct dom_entry_list domstats;
};

struct nodechart_entry {
    uint64_t total_accesses;

    uint64_t total_list;
    uint64_t local_list;
    uint64_t remote_list;
    uint64_t given_list;
    uint64_t used_list;
 
    uint64_t total_puts;
    uint64_t total_putssucc;
    float total_putrateavg;
    float total_putrateinst;

    uint64_t total_gets;
    uint64_t total_getssucc;
    float total_getrateavg;
    float total_getrateinst;

    uint64_t total_flush;
    uint64_t total_flushsucc;
    float total_flushrateavg;
    float total_flushrateinst;

    uint64_t local_puts;
    float local_putrateavg;
    float local_putrateinst;

    uint64_t local_gets;
    float local_getrateavg;
    float local_getrateinst;

    uint64_t local_flush;
    float local_flushrateavg;
    float local_flushrateinst;

    
    uint64_t remote_puts;
    float remote_putrateavg;
    float remote_putrateinst;

    uint64_t remote_gets;
    float remote_getrateavg;
    float remote_getrateinst;

    uint64_t remote_flush;
    float remote_flushrateavg;
    float remote_flushrateinst;

    uint64_t total_tmem;
    uint64_t local_tmem;
    uint64_t remote_tmem;

    struct nodechart_entry *prev;
    struct nodechart_entry *next;
};

struct node_entry_list {
    long unsigned int size;
    struct nodechart_entry *list_root;
    struct nodechart_entry *list_last;
};

struct pernodechart {
    uint32_t region_id;
    uint64_t sys_mem;
    uint64_t tmempages;

    uint32_t domcnt;
    uint64_t entry_count;
    uint64_t start_ts;

    struct single_entry_list time;

    struct node_entry_list nodestats;
    struct perdomchart domcharts[MAX_DOMAINS];
};

// Structure that holds number of pages received from remote nodes, 
// and from which node have they been received from

struct rmtaggr_nodcnt {
    uint32_t region_id;

    // MM_REQ_MCAP stats
    uint64_t req_mcap_in;             // amount of requests received coming from this remote node
    uint64_t req_mcap_inpagreq;       // amount of pages requested by this node
    uint64_t rcv_mcap_out;            // amount of responses sent out to this remote node
    uint64_t rcv_mcap_outpagsent;         // amount of pages actually sent out to this remote node
                                      // We expect req_mcap_in >= rcv_mcap_out, 
                                      // req_mcap_reqamt >= rcv_mcap_givamt.

    uint64_t req_mcap_out;            // amount of MM_REQ_MCAPs sent out to this remote node by local node. 
    uint64_t req_mcap_outpagreq;      // amount of pages requested to this node through MM_REQ_MCAPs
    uint64_t rcv_mcap_in;             // amount of MM_RCV_MCAPs received by this remote node to local node
    uint64_t rcv_mcap_inpagrcvd;      // amount of pages received from remote node through MM_RCV_MCAPs


    // MM_RECLAIM_MCAP stats
    uint64_t reclaim_mcap_in;         // amount of MM_RECLAIM_MCAPs received from this remote node by local node
    uint64_t reclaim_mcap_inpagret;   // amount of pages asked back by remote node through MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_out;        // amount of MM_RECLACK_MCAPs sent out to this remote node
    uint64_t reclack_mcap_outpagret;  // amount of pages returned back to the remote node through MM_RECLACK_MCAPs

    uint64_t reclaim_mcap_out;        // amount of MM_RECLAIM_MCAPs sent to this remote node by local node
    uint64_t reclaim_mcap_outpagreq;  // amount of pages sent to this remote through MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_in;         // amount of MM_RECLACK_MCAPs received from this remote node by local node
    uint64_t reclack_mcap_inpagret;   // amount of pages recovered from remote node through MM_RECLACK_MCAPs


    // MM_RELACK_STATS (relinquish)
    uint64_t relack_mcap_in;          // amount of MM_RELACK_MCAPs received by this remote node in the local node
    uint64_t relack_mcap_inpagret;    // amount of pages received back from this remote node
    uint64_t relack_mcap_out;         // amount of MM_RELACK_MCAPs sent to this remote node by the local node
    uint64_t relack_mcap_outpagret;   // amount of pages asked back to this remote node through MM_RELACK_MCAPs

    // statistics aggr
    uint64_t remote_pages_taken;     // remote pages taken from this remote node belonging to it.
    uint64_t remote_pages_given;     // local pages given to this remote node from local node.
    uint64_t remote_pages_dispin;    // remote pages belonging to non-local nodes given to this remote node
    uint64_t remote_pages_dispout;   // remote pages belonging to this remote node that the local node has
                                     // given away to other nodes

    // For the cost funciton
    uint64_t reallatency2node;           // Given in nanoseconds, the same way latency is specified.
    uint64_t estilatency2node;         
};

struct rmtaggr_cnt {
    uint32_t region_id;    

    // request stats
    uint64_t req_mcap_in;          // amount of MM_REQ_MCAP rcvd by this node coming from diff nodes
    uint64_t req_mcap_inpagreq;        // amount of pages requested by MM_REQ_MCAPS coming from diff nodes
    uint64_t rcv_mcap_out;          // amount of MM_RCV_MCAP sent out as response of MM_REQ_MCAP
    uint64_t rcv_mcap_outpagsent;       // amount of pages sent with MM_RCV_MCAPs.

    uint64_t req_mcap_out;          // amount of MM_REQ_MCAPs sent by this node
    uint64_t req_mcap_outpagreq;        // amount of pages requested through MM_REQ_MCAPs
    uint64_t rcv_mcap_in;          // amount of MM_RCV_MCAP responses received after sending MM_REQ_MCAPs
    uint64_t rcv_mcap_inpagrcvd;       // amount of pages actually received after issuing a MM_REQ_MCAPs

    // reclaim stats
    uint64_t reclaim_mcap_in;         // amount of MM_RECLAIM_MCAPs rcvd by this node from diff nodes
    uint64_t reclaim_mcap_inpagret;   // amount of pages asked back by MM_RECLAIM_MCAPs coming from diff nodes
    uint64_t reclack_mcap_out;        // amount of MM_RECLACK_MCAPs sent back as response to MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_outpagret;  // amount of pages returned back with MM_RECLACK_MCAPs

    uint64_t reclaim_mcap_out;        // amount of MM_RECLAIM_MCAPs sent by this node 
    uint64_t reclaim_mcap_outpagreq;  // amount of pages asked back by this node when sending MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_in;         // amount of responses registered after issuing MM_RECLAIM_MCAPs
    uint64_t reclack_mcap_inpagret;   // amount of pages received back when receiving MM_RECLACK_MCAPs

    // relinquish stats
    uint64_t relack_mcap_in;          // amount of MM_RELACK_MCAPs received by this node from diff nodes
    uint64_t relack_mcap_inpagret;    // amount of pages received back by nodes that have relinquished
    uint64_t relack_mcap_out;
    uint64_t relack_mcap_outpagret;
   
    // remote pages this node now possesses
    uint64_t remote_pages_taken;     // remote pages the node currently has. This value is incremented
                                     // or decremented.
    uint64_t remote_pages_given;     // remote pages belonging to this node that has given to other
                                     // nodes.
    uint64_t remote_pages_dispin;   // remote pages belonging to other nodes that this node has, but
                                     // it has give away to other nodes
    uint64_t remote_pages_dispout;   // remote pages belonging to other nodes that this node has, but
                                     // it has give away to other nodes

    struct rmtaggr_nodcnt nodinfo[MAX_RMTNODES]; 
};


/* The format of the configuration file is as follows

nodes=x
address=XXX.XXX.XXX.XXX, port_ts=XXXXX, port_mm=XXXXX, self=XX, addr_strt=0xXXXXXXXX, global_pages=X~X, region=XX
address=XXX.XXX.XXX.XXX, port_ts=XXXXX, port_mm=XXXXX, self=XX, addr_strt=0xXXXXXXXX, global_pages=X~X, region=XX
...
('nodes'-1 number of lines)
address=XXX.XXX.XXX.XXX, port_ts=XXXXX, port_mm=XXXXX, self=XX, addr_strt=0xXXXXXXXX, global_pages=X~X, region=XX
*/

int parnoconf(char * config_file_name, struct node_info **ninf, uint32_t *number_of_nodes);
int32_t node_updt(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, 
                    struct pernodechart * nodecharts, uint32_t this_region_id, uint32_t node_no, int32_t this_node_id, struct node_info *ninf );


uint32_t get_this_node_id(struct node_info *ninf, uint32_t number_of_nodes);
uint32_t get_this_region_id(struct node_info *ninf, uint32_t number_of_nodes);
uint64_t get_this_tmempages(struct node_info *ninf, uint32_t node_no);
uint64_t get_this_sys_mem(struct node_info *ninf, uint32_t node_no);

int32_t pernodechart_clear(struct pernodechart * nodecharts, uint32_t this_node_id);
int32_t tmop_cnt_clear(struct tmop_cnt * opcnt);
int32_t rmtaggr_cnt_clear(struct rmtaggr_cnt *rmopcnt);

int32_t display_chart (struct pernodechart * nodecharts );
int32_t write_stats_to_file(struct pernodechart *nodecharts);

int32_t setzero_domstats(struct domchart_entry *entry, uint64_t start_ts);
int32_t copy_domstats(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint32_t i, struct domchart_entry *entry, 
			uint64_t curr_ts, uint64_t prev_ts, uint64_t start_ts, 
			uint64_t prev_puts, uint64_t prev_gets, uint64_t prev_flush,
			uint64_t prev_puts_succ, uint64_t prev_gets_succ, 
                        /* New additions */ uint64_t prev_local_puts, uint64_t prev_local_gets, uint64_t prev_local_flush,
                        uint64_t prev_local_puts_succ, uint64_t prev_local_gets_succ,
                        uint64_t prev_rem_puts, uint64_t prev_rem_gets, uint64_t prev_rem_flush,
                        uint64_t prev_rem_puts_succ, uint64_t prev_rem_gets_succ );
int32_t copy_nodestats(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, 
                        uint32_t this_node_id, struct node_info *ninf, struct nodechart_entry *entry, 
			uint64_t curr_ts, uint64_t prev_ts, uint64_t start_ts, 
			uint64_t prev_total_puts, uint64_t prev_total_gets, uint64_t prev_total_flush,
			uint64_t prev_local_puts, uint64_t prev_local_gets, uint64_t prev_local_flush,
			uint64_t prev_remote_puts, uint64_t prev_remote_gets, uint64_t prev_remote_flush);

int32_t insert_element_to_entrylist( struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, 
				     struct pernodechart *nodecharts, 
				     uint32_t this_region_id, uint32_t node_no, uint32_t this_node_id, struct node_info *ninf);


int32_t node_register(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, 
                          struct node_info *ninf, uint32_t node_no, uint32_t this_node_id );

// to build the sharing graph
struct edge {
    int32_t label;       // node: use region IDs as labels
    uint64_t weight;      // the property of the edge
};

struct llnode {
    int32_t label;       // node: use region IDs as labels   
     struct edge myedges[MAX_RMTNODES];
};

int32_t build_graph(struct tmop_cnt *opcnt, struct rmtaggr_cnt *rmopcnt, uint64_t curr_ts,  struct node_info *ninf, uint32_t node_no, uint32_t this_node_id);
int32_t graph_clear(struct llnode *graph);


#endif
